<?php

namespace App\Http\Controllers;

use App\Models\Star;
use Illuminate\Http\Request;

class StarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate(
            [
                'star' => 'required|numeric',
                'id_service' => 'required|numeric',
                'id_teacher' => 'required|numeric',
                'id_service' => 'required|numeric',
                'id_std' => 'required|numeric',
            ],
            [
                'star.required' => 'اختر قيمة التقيم',
                'star.numeric' => 'اختر قيمة التقيم بشكل صحيح',
                'id_teacher.required' => 'اختر قيمة التقيم',
                'id_teacher.numeric' => 'اختر قيمة التقيم بشكل صحيح',
                'id_service.required' => 'اختر قيمة التقيم',
                'id_service.numeric' => 'اختر قيمة التقيم بشكل صحيح',
                'id_std.required' => 'اختر قيمة التقيم',
                'id_std.numeric' => 'اختر قيمة التقيم بشكل صحيح',
            ]

        );
        $Star = new Star();
        $Star->Values = $request->get('star');
        $Star->teacher_id = $request->get('id_teacher');
        $Star->service_id = $request->get('id_service');
        $Star->student_id = $request->get('id_std');
        $issave = $Star->save();
        if ($issave) {
            session()->flash('message', 'لقد تم تقيم المعلم .. شكراً لك '); // عبارة عن مسج لجلسة المستخدم تضيئ ثم تمسح من السشن
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Star  $star
     * @return \Illuminate\Http\Response
     */
    public function show(Star $star)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Star  $star
     * @return \Illuminate\Http\Response
     */
    public function edit(Star $star)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Star  $star
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Star $star)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Star  $star
     * @return \Illuminate\Http\Response
     */
    public function destroy(Star $star)
    {
        //
    }
}
