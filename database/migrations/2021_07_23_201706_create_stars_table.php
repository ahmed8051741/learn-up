<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stars', function (Blueprint $table) {
            $table->id();

            $table->foreignId('student_id')->nullable();
            $table->foreign('student_id')->on('students')->references('id');

            $table->foreignId('service_id');
            $table->foreign('service_id')->on('services')->references('id');

            $table->foreignId('teacher_id');
            $table->foreign('teacher_id')->on('teachers')->references('id');

            $table->string('Values');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stars');
    }
}
