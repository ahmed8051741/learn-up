<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notice extends Model
{
    use HasFactory, SoftDeletes;

    public function Rqserve()
    {
        return $this->belongsTo(Rqserve::class, 'Reserve_id', 'id');
    }

    public function Rqorder()
    {
        return $this->belongsTo(Rqorder::class, 'Rqorder_id', 'id');
    }
}
