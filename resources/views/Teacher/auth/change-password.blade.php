@extends('layout.parent')


@section('Title', 'الرئيسية')

@section('CSS')

    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

    <link href="{{ asset('assets/css/Star.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/profile.css') }}" rel="stylesheet">
    <style>
        * {
            font-family: 'cairo';
        }

        .visitor {
            color: white;
            font-size: 15px;

        }



        #header {
            visibility: hidden;
            margin-top: -50px;
        }

    </style>
@endsection



@section('dd')
    <h2 class="logo" style="font-family: Al-Jazeera-Arabic"><a href="{{ route('showHome') }}"
            style="font-family: Al-Jazeera-Arabic; color: aliceblue"> Learn <span
                style="font-family: Al-Jazeera-Arabic;font-weight: 760;color: #Fd4;"> up
            </span></a></h2>
@endsection
@section('con', 'display: none')


@section('content')


@section('Top')

    <section id="topbar">
        <div class="container d-flex justify-content-center justify-content-md-between" id="topb">
            <div class="contact-info d-flex align-items-center">
                <a href="mailto:ahmed8051741@gmail.com" class="visitor"> &nbsp; الدعم الفني عبر البريد الالكتروني </a>
            </div>
            <div class="d-flex flex-row align-items-center feed-text px-2" dir="ltr">
                &nbsp;&nbsp;
                <div class="d-flex flex-column flex-wrap ml-2"><span class="font-weight-bold">
                        <div class="dropdown">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" style="color: #ffffff;"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="rounded-circle" src="{{ asset('assets/img/my.jpeg') }}" width="42">
                                &nbsp;&nbsp;
                                {{ auth()->user()->name }} &nbsp;</a>

                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="{{ route('showprofile') }}">الملف الشخصي</a>
                                <hr style="margin-top:0.8px;margin-bottom:0.8px">
                                <a class="dropdown-item" href="#">اضافة خدمة جديد</a>
                                <a class="dropdown-item" href="{{ route('control') }}">لوحة القيادة</a>
                                <a class="dropdown-item" href="{{ route('logout_log') }}">تسجيل الخروج</a>
                            </div>
                        </div>
                    </span>

                </div>
            </div>
        </div>
    </section>
@endsection

<div class="container bootstrap snippets bootdey">
    <div class="row">
        <div class="profile-nav col-md-3">
            <div class="panel">
                <div class="user-heading round">
                    <a href="#">
                        <img src="{{ asset('assets/img/users/images/user.png') }}" alt="">
                    </a>
                    <h1> {{ auth()->user()->name }} </h1>
                    <p>{{ auth()->user()->email }} </p>
                </div>

                <ul class="nav">
                    <li><a href="{{ route('showprofile') }}"><i class="fa fa-user"> &nbsp;</i> الملف الشخصي</a></li>



                    @if (auth()->user()->id == auth('teacher')->id())
                        <li><a href="{{ route('show.teacher', auth()->user()->id) }}"><i class="fas fa-eye">
                                    &nbsp;</i>
                                عرض ملفك الشخصي كما يظهر للاخرين
                            </a></li>

                    @else
                        <li><a href="{{ route('show.std', auth()->user()->id) }}"><i class="fas fa-eye">
                                    &nbsp;</i>
                                عرض ملفك الشخصي كما يظهر للاخرين
                            </a></li>
                    @endif




                    <li><a href="{{ route('Edite_Profile') }}"><i class="fa fa-edit"> &nbsp;</i> تعديل الملف
                            الشخصي</a></li>

                    <li><a href="{{ route('image.index') }}"><i class="far fa-images"> &nbsp;</i>
                            تعديل صورة الملف
                            الشخصي</a></li>

                    <li class="active"><a href="{{ route('password_edit') }}"><i class="fas fa-lock"> &nbsp;</i>
                            تعديل كلمه المرور</a></li>
                    <li><a href="{{ route('control') }}"><i class="fa fa-calendar"> &nbsp;</i> العودة الى لوحة التحكم
                        </a></li>
                    <li><a href="{{ route('logout_log') }}"><i class="fas fa-sign-out-alt"> &nbsp;</i> تسجيل
                            الخروج</a></li>
                </ul>
            </div>
        </div>
        <div class="profile-info col-md-9">
            <div class="panel">
                <div class="bg-info clearfix">
                    <button type="button" class="btn btn-secondary float-right"> المعلومات الشخصية </button>
                </div>
            </div>
            <div class="panel">

                <div class="panel-body bio-graph-info">
                    <div style="margin: 5px; margin-right: 22px ; margin-top: 20px">
                        <h4> تعديل كلمه المرور الخاصة بك </h4>
                        <div align="center">
                            <hr width="96%">
                        </div>
                        <div class="row" style="margin: 5px;margin-top: 25px">

                            <div class="row">
                                <div class="col-md-12">
                                    <form>

                                        <div class="form-group row">
                                            <label for="current_password" class="col-4 col-form-label mb-2"> كلمه المرور
                                                الحالية </label>
                                            <div class="col-8">
                                                <input id="current_password" placeholder="ادخل كلمه المرور الحالية"
                                                    class="form-control here" type="password">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="new_password" class="col-4 col-form-label mb-2"> ادخل كلمه
                                                المرور الجديدة </label>
                                            <div class="col-8">
                                                <input id="new_password" placeholder="ادخل كلمه المرور الجديدة"
                                                    class="form-control here" type="password">
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="new_password_confirmation" class="col-4 col-form-label mb-2">
                                                اعد ادخال كلمه المرور خاصتك </label>
                                            <div class="col-8">
                                                <input id="new_password_confirmation"
                                                    placeholder="اعد ادخال كلمه المرور خاصتك " class="form-control here"
                                                    type="password">
                                            </div>
                                        </div>

                                        <br>
                                        <br>
                                        <br>
                                        <button type="button" onclick="updatePassword({{ auth()->user()->id }})"
                                            class="btn btn-primary" id="button" style="float: left; width: 100%"> تحديث
                                            كلمه المرور </button>
                                        <br>
                                        <br> <br>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div>

                </div>
            </div>
        </div>
    </div>

    <br>
    <br>
    <br>

</div>

@endsection


@section('JS')
<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-46156385-1', 'cssscript.com');
    ga('send', 'pageview');
</script>



@section('JS')

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>


<script>
    function updatePassword() {
        axios.put('/profile/profile_setting/Password/update_Password/', {
                current_password: document.getElementById('current_password').value,
                new_password: document.getElementById('new_password').value,
                new_password_confirmation: document.getElementById('new_password_confirmation').value
            })
            .then(function(response) {
                // handle success
                console.log(response);
                toastr.success(response.data.message);
                document.getElementById('create-form').reset(); // تصفير قيمة الفورم سبمت
                // window.location.href="/cms/admin/Update-password/"; // التحويل والانتقال
            })
            .catch(function(error) {
                // handle error
                console.log(error);
                toastr.error(error.response.data.message);
            })
            .then(function() {
                // always executed
            });
    }
</script>



@endsection

@endsection
