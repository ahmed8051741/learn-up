<?php

namespace App\Http\Controllers;

use App\Models\Notice;
use App\Models\Rqserve;
use App\Models\Service;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RqserveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Services = Service::all();
        return response()->view('order.show_req_serves', ['Services' => $Services]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator(
            $request->all(),
            [

                'teacher_id' => 'required|numeric',
                'services_id' => 'required|numeric',
                'teacher_sd' => '',
                'student_id' => '',
                'pay' => 'required|string',
                'date' => 'required|string',
                'time' => 'required|string',
                'account_type' => 'required|string',
            ],
            [

                'teacher_id.required' => 'حدث خلل امني .. الرجاء المحاولة مرة اخرى',
                'teacher_id.numeric' => 'حدث خلل امني .. الرجاء المحاولة مرة اخرى',
                'services_id.required' => 'حدث خلل امني .. الرجاء المحاولة مرة اخرى',
                'services_id.numeric' => 'حدث خلل امني .. الرجاء المحاولة مرة اخرى',
                'pay.required' => 'اختر طريقة الدفع ',
                'pay.string' => ' اختر طريقة الدفع بشكلاً صحيح',
                'daye.string' => 'يسمح فقط بنصوص في حقل اليوم',
                'date.required' => 'قم بادخال التاريخ الذي يناسبك ليتواصل معك مقدم الخدمه',
                'date.string' => ' قم بادخال التاريخ الذي يناسبك ليتواصل معك مقدم الخدمه بشكل صحيح',
                'time.required' => 'قم بادخال الساعة التي تناسبك ليتواصل معك مقدم الخدمه',
                'time.string' => ' قم بادخال الساعة التي تناسبك ليتواصل معك مقدم الخدمه بشكل صحيح',
                'account_type.required' => 'حدث خلل امني .. الرجاء المحاولة مرة اخرى',
                'account_type.string' => 'حدث خلل امني .. الرجاء المحاولة مرة اخرى',

            ]
        );

        if (!$validator->fails()) {
            $rqserve = new Rqserve();
            $rqserve->teacher_id = $request->get('teacher_id');
            $rqserve->teacher_sd = $request->get('teacher_sd');
            $rqserve->service_id = $request->get('services_id');
            $rqserve->student_id = $request->get('student_id');
            $rqserve->pay = $request->get('pay');
            $rqserve->date = $request->get('date');
            $rqserve->time = $request->get('time');
            $rqserve->account_type = $request->get('account_type');


            $isSaved = $rqserve->save();
            return response()->json([
                'message' => $isSaved ? "تم تأكيد طلبك بنجاح .. شكراً على تعاملك معنا" : "حدث خطاء غير معروف .. الرجاء التواصل مع الدعم الفني"
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            //VALIDATION FAILED
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Rqserve  $rqserve
     * @return \Illuminate\Http\Response
     */
    public function show(Rqserve $rqserve)
    {
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $Services = Service::with('Teacher')->get();
        return response()->view('order.req_serves_Stat', ['rqserve' => $rqserve, 'Services' => $Services, 'Notices' => $Notices, 'Reserves' => $Reserves]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Rqserve  $rqserve
     * @return \Illuminate\Http\Response
     */
    public function edit(Rqserve $rqserve)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Rqserve  $rqserve
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rqserve $rqserve)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Rqserve  $rqserve
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rqserve $rqserve)
    {
        $isDelete = $rqserve->delete();
        if ($isDelete) {
            return response()->json(
                ['title' => 'نجاح', 'text' => 'تم حذف المستخدم المشرف بنجاح', 'icon' => 'success'],
                Response::HTTP_OK
            );
        } else {
            return response()->json(
                ['title' => 'فشل', 'text' => 'فشل الحذف ', 'icon' => 'error'],
                Response::HTTP_BAD_REQUEST
            );
        }
    }



    //     $rqserve = Rqserve::find($rqserve);
    //     $isDelete = $rqserve->delete();
    //     if ($isDelete) {
    //         return response()->json(
    //             ['title' => 'نجاح', 'text' => 'نجحت العملية ', 'icon' => 'success'],
    //             Response::HTTP_OK
    //         );
    //     } else {
    //         return response()->json(
    //             ['title' => 'فشل', 'text' => 'فشل الحذف تواصل مع الدعم الفني', 'icon' => 'error'],
    //             Response::HTTP_BAD_REQUEST
    //         );
    //     }
    // }    }
}
