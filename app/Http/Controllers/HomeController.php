<?php

namespace App\Http\Controllers;

use App\Models\category;
use App\Models\City;
use App\Models\Notice;
use App\Models\Order;
use App\Models\Rqorder;
use App\Models\Rqserve;
use App\Models\Service;
use App\Models\Star;
use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{




    public function showindex()
    {
        $categoryx = NUll;
        $case  = NUll;
        $Cityx = NUll;
        $search = NUll;
        $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')->get();
        $categorise = category::all();
        $cities = City::all();
        $Teacher = Teacher::all();
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
        $stars = Star::all();
        return response()->view('index', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
    }






    public function showHome()
    {
        $categoryx = NUll;
        $case  = NUll;
        $Cityx = NUll;
        $search = NUll;
        $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')->get();
        $categorise = category::all();
        $cities = City::all();
        $Teacher = Teacher::all();
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
        $stars = Star::all();
        return response()->view('Teacher.Home', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
    }

    public function requests_Std()
    {
        $categoryx = NUll;
        $case  = NUll;
        $Cityx = NUll;
        $search = NUll;
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $orders = Order::with('student')->with('link')->orderBy('id', 'DESC')->get();
        $categorise = category::all();
        $cities = City::all();
        $Student = Student::all();
        return response()->view('students.Home', ['orders' => $orders, 'categorise' => $categorise, 'cities' => $cities, 'Student' => $Student, 'Notices' => $Notices, 'Reserves' => $Reserves, $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
    }

    public function showprofile()
    {
        $stars = Star::all();
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        return response()->view('Teacher.Profile', ['Notices' => $Notices, 'Reserves' => $Reserves, 'stars' => $stars]);
    }




    public function profile_settings()
    {
        $Notices = Notice::with('Rqserve')->with('Rqorder')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $Reses = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->get();
        $categorise = category::all();
        $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')->get();
        $orders = Order::with('suborder')->orderBy('id', 'DESC')->get();
        $Rqorders = Rqorder::with('order')->with('Studs')->orderBy('id', 'DESC')->get();

        return response()->view('Teacher.Control', ['services' => $services, 'Rqorders' => $Rqorders, 'orders' => $orders, 'categorise' => $categorise, 'Notices' => $Notices, 'Reserves' => $Reserves, 'Reses' => $Reses]);
    }


    public function profile_settings_std()
    {
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $Teacher = Teacher::all();
        $categorise = category::all();
        return response()->view('students.Control', ['categorise' => $categorise, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves]);
    }



    public function Edite_Profile()
    {
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $categorise = category::all();
        return response()->view('Teacher.Edite_Profile', ['categorise' => $categorise, 'Notices' => $Notices, 'Reserves' => $Reserves]);
    }


    // public function Show_Profile(Request $request)
    // {
    //     $Teacher = Teacher::where('id', '=', $request)->get();
    //     return response()->view('Teacher.Profile', ['Teacher' => $Teacher]);
    // }

    public function profile_update(Request $request)
    {
        $guard = auth('teacher')->check() ? 'teacher' : 'student';

        $validator = validator(
            $request->all(),
            [
                'name'     => 'required|string|min:3|max:30',
                'email'    => 'required|email',
                'smobile'   => 'required|numeric',
                'mobile'   => 'required|numeric',
                'select'   => 'required|string',
                'facebook' => 'nullable|string',
                'instagram' => 'nullable|string',
                'youtube'  => 'nullable|string',
                'describe' => 'required|string|min:10'
            ],
            [

                'name.required' => 'لا يمكن ترك الاسم فارغاً',
                'name.string' => 'لايمكنك ادخال غير النصوص في حقل الاسم',
                'name.min' => 'الاسم صغير جداً',
                'name.max' => 'الاسم طويل جداً',
                'email.required' => 'لا يمكن ترك البريد الالكتوني فارغاً',
                'email.string' => 'لايمكنك ادخال غير النصوص في حقل البريد الالكتوني',
                'mobile.required' => 'قم بادخال رقم الهاتف',
                'mobile.numeric' => ' قم بادخال رقم الهاتف بطريقة صحيحة',
                'smobile.numeric' => 'قم بادخال مقدمه الهاتف بطريقة',
                'smobile.required' => 'قم بادخال مقدمة الهاتف',                'select.required' => 'يجب ان تختار تخصصك',
                'select.string' => 'لايمكنك ادخال غير النصوص في حقل الاسم',
                'facebook.string' => 'لايمكنك ادخال غير النصوص في حقل الفيسبوك',
                'facebook.required' => ' لا يمكن ترك الفيسبوك فارغاً ان كنت ترغب في تركة فارغاً ضع اشارة ال # ',
                'instagram.string' => 'لايمكنك ادخال غير النصوص في حقل الانستجرام',
                'instagram.required' => ' لا يمكن ترك الانستجرام فارغاً ان كنت ترغب في تركة فارغاً ضع اشارة ال # ',
                'youtube.string' => 'لايمكنك ادخال غير النصوص في حقل اليوتيوب',
                'youtube.required' => ' لا يمكن ترك اليوتيوب فارغاً ان كنت ترغب في تركة فارغاً ضع اشارة ال # ',
                'describe.required' => ' يجب ان تضع وصفاً لنفسك لتعرف الناس عنك ',
                'describe.min' => ' هذا الوصف قصير جداً ',
                'describe.string' => 'لايمكنك ادخال غير النصوص في حقل الوصف',




            ]



        );

        if (!$validator->fails()) {
            $user = auth($guard)->user();
            $user->name = $request->get('name');
            $user->s_mobile = $request->get('smobile');
            $user->mobile = $request->get('mobile');
            $user->email = $request->get('email');
            $user->major = $request->get('select');
            $user->facebook = $request->get('facebook');
            $user->instagram = $request->get('instagram');
            $user->youtube = $request->get('youtube');
            $user->describe = $request->get('describe');
            $isSaved = $user->save();
            return response()->json(['message' => $isSaved ? 'تم تديث بيناتك بنجاح' : 'حصل خطاء ما حاول مجدداً او اتصل بالدعم الفني'], $isSaved ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function password_edit()
    {
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $categorise = category::all();
        return response()->view('Teacher.auth.change-password', ['categorise' => $categorise, 'Notices' => $Notices, 'Reserves' => $Reserves]);
    }


    public function show_Service(Request $request)
    {
    }


    public function update_Password(Request $request)
    {
        $guard = auth('teacher')->check() ? 'teacher' : 'student';
        $validator = validator(
            $request->all(),
            [
                'current_password' => "required|string|password:$guard",
                'new_password' => 'required|string|min:6|max:18|confirmed',
            ],
            [
                'current_password.required' => 'ادخل كلمه المرور الحالية',
                'current_password.string' => 'كلمه المرور الحالية غير صحيحة',
                'current_password.password' => 'كلمه المرور الحالية غير صحيحة',
                'new_password.required' => 'ادخل كلمه المرور الجديدة',
                'new_password.string' => 'كلمه المرور محصورة ما بين 18-6 احرف او رموز',
                'new_password.min' => 'كلمه المرور محصورة ما بين 18-6 احرف او رموز',
                'new_password.max' => 'كلمه المرور محصورة ما بين 18-6 احرف او رموز',
                'new_password.confirmed' => 'كلمات المرور غير متطابقة',


            ]


        );


        if (!$validator->fails()) {
            $user = auth($guard)->user();
            $user->password = Hash::make($request->get('new_password'));
            $isSaved = $user->save();
            return response()->json(['message' => $isSaved ? 'تم تحديث كلمه المرور بنجاح ' : 'حدث خطاء غير متوقع الرجاء المحاولة مرة اخرى'], $isSaved ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }


    public function show($student)
    {
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $student = Student::find($student);
        // $student = Student::where('id', '=', $student);
        // $data = Product::where('name', 'like', 'a%')->count();
        return response()->view('Show_profile_std', ['student' => $student, 'Notices' => $Notices, 'Reserves' => $Reserves]);
    }


    public function show_teacher($teacher)
    {
        $teacher = Teacher::find($teacher);
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        return response()->view('Show_profile', ['teacher' => $teacher, 'Notices' => $Notices, 'Reserves' => $Reserves]);
    }


    public function Reqserves($id)
    {
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $service = Service::with('teacher')->find($id);
        return response()->view('order.req_serves', ['service' => $service, 'Notices' => $Notices, 'Reserves' => $Reserves]);
    }

    public function show_Certain($id)
    {
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $Rqserve = Rqserve::with('subReserve')->with('Service')->with('Stud')->where('service_id', '=', $id)->get();
        // dd($Rqserve);
        return response()->view('order.show_req_serves', ['Rqserve' => $Rqserve, 'Notices' => $Notices, 'Reserves' => $Reserves]);
    }

    public function show_Certain_Admin($id)
    {
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $Rqserve = Rqserve::with('subReserve')->with('Service')->with('Stud')->where('service_id', '=', $id)->get();
        // dd($Rqserve);
        return response()->view('admins.teacher.Servicerq', ['Rqserve' => $Rqserve, 'Notices' => $Notices, 'Reserves' => $Reserves]);
    }



    public function show_CertainPro($id)
    {
        $read = Rqserve::find($id);
        $read->reade = '1';
        $read->save();
        return redirect()->route('show.Accept', $id);
    }

    public function show_CertainProstd($id)
    {
        $reade = Notice::find($id);
        $reade->read = '1';
        $reade->save();

        return redirect()->route('std_allserve');
    }

    public function Rqorder($id)
    {
        $orders = Order::with('student')->find($id);
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $Rqorder = Rqorder::with('order')->with('Teachers')->with('Studs')->where('order_id', '=', $id)->get();
        return response()->view('order.req_order', ['Order' => $orders, 'Notices' => $Notices, 'Reserves' => $Reserves, 'Rqorder' => $Rqorder]);
    }


    public function showstd_Certain($id)
    {
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $Rqorder = Rqorder::with('order')->with('Teachers')->with('Studs')->where('order_id', '=', $id)->get();

        return response()->view('order.show_req_order', ['Notices' => $Notices, 'Reserves' => $Reserves, 'Rqorder' => $Rqorder,]);
    }


    public function read_allstd($id)
    {
        DB::table('notices')
            ->where('req_send', $id)
            ->update(['read' => 1]);

        // DB::update("update notices set read = 1 where req_send = $id");
        return redirect()->back();
    }



    public function read_all($id)
    {
        DB::table('rqserves')
            ->where('teacher_id', $id)
            ->update(['reade' => 1]);

        // DB::update("update notices set read = 1 where req_send = $id");
        return redirect()->back();
    }


    public function Accept($id)
    {
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $Rqserve = Rqserve::with('subReserve')->with('Service')->with('Stud')->where('id', '=', $id)->get();
        return response()->view('order.req_serves_Stat', ['Rqserve' => $Rqserve, 'Notices' => $Notices, 'Reserves' => $Reserves]);

        // $Rqserve = Rqserve::find($id);
        // $Rqserve->Status = 1;
        // $isUpdated = $Rqserve->save();
        // $Rqserve->save();
        // if ($isUpdated) {
        //     session()->flash('message', 'تم قبول الطلب بنجاح مع العلم انه تم اشعار الطالب بالعملية');
        //     return redirect()->back();
        // }Notice.store
    }

    public function Accept_UPdate(Request $request, $id)
    {
        $validator = Validator(
            $request->all(),
            [
                'status' => 'required|numeric',
                'title' => '',
                'send' => 'required|string',
                'send_id' => 'required|string',
                'Rqserve_id'  => 'required|numeric',
                'Rqserve_id'  => 'required',
                'send_name'  => 'required',
                'req_send'  => 'required',
                'req_send_name'  => 'required',
                'serves'  => 'required',
                'req_send_type'  => 'required',
            ],
            [
                'status.required' => 'يجب اختيار حالة الطلب',
                'status.numeric' => 'حدث خلل امني الرجاء المحاولة لاحقاً',
                'send.string' => 'يسمح فقط بنصوص',
                'send.string' => 'حدث خلل امني الرجاء المحاولة لاحقاً',
                'send.required' => 'حدث خلل امني الرجاء المحاولة لاحقاً',
                'Rqserve_id.required' => 'حدث خلل امني الرجاء المحاولة لاحقاً',
                'send_name.required' => 'حدث خلل امني الرجاء المحاولة لاحقاً',
                'req_send.required' => 'حدث خلل امني الرجاء المحاولة لاحقاً',
                'req_send_name.required' => 'حدث خلل امني الرجاء المحاولة لاحقاً',
                'req_send_type.required' => 'حدث خلل امني الرجاء المحاولة لاحقاً',
                'serves.required' => 'حدث خلل امني الرجاء المحاولة لاحقاً',
                'send_id.required' => 'حدث خلل امني الرجاء المحاولة لاحقاً',

            ]
        );

        if (!$validator->fails()) {
            //TODO: CREATE NEW CATEGORY
            $Notice = new Notice();
            $Notice->Status = $request->get('status');;
            $Notice->title = $request->get('title');
            $Notice->send = $request->get('send');
            $Notice->send_id = $request->get('send_id');
            $Notice->Rqserve_id = $request->get('Rqserve_id');
            $Notice->send_name = $request->get('send_name');
            $Notice->req_send = $request->get('req_send');
            $Notice->serves = $request->get('serves');
            $Notice->req_send_name = $request->get('req_send_name');
            $Notice->req_send_type = $request->get('req_send_type');
            $Rqserve = Rqserve::find($id);
            $Rqserve->Status = $request->get('status');
            $isSaved = $Rqserve->save();
            $isSaved = $Notice->save();
            return response()->json([
                'message' => $isSaved ?  "تمت العملية بنجاح" : "فشلت العملية لسبب ما"
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            //VALIDATION FAILED
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function view_allstd($id)
    {
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();

        $Nots = Notice::with('Rqserve')->where('req_send', '=', $id)->orderBy('id', 'DESC')->get();

        return response()->view('students.notifications', ['Notices' => $Notices, 'Reserves' => $Reserves, 'Nots' => $Nots]);
    }

    public function view_all($id)
    {
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $Res = Rqserve::with('subReserve')->with('Service')->with('Stud')->where('teacher_id', '=', $id)->orderBy('id', 'DESC')->get();
        $Notics = Notice::with('Rqserve')->orderBy('id', 'DESC')->get();


        return response()->view('Teacher.notifications', ['Notices' => $Notices, 'Reserves' => $Reserves, 'Res' => $Res, 'Notics' =>  $Notics]);
    }



    public function rqserve_all()
    {
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $Reses = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->get();
        $categorise = category::all();
        $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')->get();

        return response()->view('Teacher.show_all_rqserves', ['services' => $services, 'categorise' => $categorise, 'Notices' => $Notices, 'Reserves' => $Reserves, 'Reses' => $Reses]);
    }

    public function rqserve_allstd()
    {
        $Notices = Notice::with('Rqserve')->with('Rqorder')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $Reses = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->get();
        $categorise = category::all();
        $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')->get();
        $orders = Order::with('suborder')->orderBy('id', 'DESC')->get();
        $Rqorders = Rqorder::with('order')->with('Studs')->get();

        return response()->view('students.show_all_rqserves', ['services' => $services, 'Rqorders' => $Rqorders, 'orders' => $orders, 'categorise' => $categorise, 'Notices' => $Notices, 'Reserves' => $Reserves, 'Reses' => $Reses]);
    }
    public function destroy($id)
    {
        $Rqserve = Rqserve::find($id);
        $isDelete = $Rqserve->delete();

        if ($isDelete) {
            session()->flash('message', 'تم حذف هذا الطلب بنجاح'); // عبارة عن مسج لجلسة المستخدم تضيئ ثم تمسح من السشن

            return redirect()->back();
        }
    }



    // public function show_teacher($teacher)
    // {
    //     $teacher = Teacher::find($teacher);
    //     $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
    //     $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
    //     return response()->view('Show_profile', ['teacher' => $teacher, 'Notices' => $Notices, 'Reserves' => $Reserves]);
    // }
    public function star($id)
    {

        $teacher = Teacher::find($id);

        $Notices = Notice::with('Rqserve')->with('Rqorder')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $Reses = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->get();
        $categorise = category::all();
        $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')->get();
        $orders = Order::with('suborder')->orderBy('id', 'DESC')->get();
        $Rqorders = Rqorder::with('order')->with('Studs')->get();
        $stars = Star::all();
        return response()->view('stare', ['stars' => $stars, 'teacher' => $teacher, 'services' => $services, 'Rqorders' => $Rqorders, 'orders' => $orders, 'categorise' => $categorise, 'Notices' => $Notices, 'Reserves' => $Reserves, 'Reses' => $Reses]);
    }

    public function std_allserve()
    {
        $teacher = Teacher::all();

        $Notices = Notice::with('Rqserve')->with('Rqorder')->orderBy('id', 'DESC')->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->get();
        $Reses = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->get();
        $categorise = category::all();
        $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')->get();
        $orders = Order::with('suborder')->orderBy('id', 'DESC')->get();
        $Rqorders = Rqorder::with('order')->with('Studs')->get();

        return response()->view('students.Show_all_serve', ['services' => $services, 'teachers' => $teacher, 'Rqorders' => $Rqorders, 'orders' => $orders, 'categorise' => $categorise, 'Notices' => $Notices, 'Reserves' => $Reserves, 'Reses' => $Reses]);
    }


    public function destroy_N($id)
    {
        $Notice = Notice::find($id);
        $isDelete = $Notice->delete();

        if ($isDelete) {
            session()->flash('message', 'تم حذف هذا الطلب بنجاح'); // عبارة عن مسج لجلسة المستخدم تضيئ ثم تمسح من السشن

            return redirect()->back();
        }
    }

    public function show_Certain_Edit($id)
    {
        $services = Service::withTrashed()->find($id);
        $categorise = category::all();
        $city = City::all();
        return response()->view('admins.teacher.edit_serves', ['service' => $services, 'categorise' => $categorise, 'city' => $city]);
    }
}
