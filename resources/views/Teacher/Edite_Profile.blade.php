@extends('layout.parent')


@section('Title', 'لوحة التحكم')

@section('CSS')

<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

<link href="{{ asset('assets/css/Star.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/profile.css') }}" rel="stylesheet">

<style>
    #header {
        visibility: hidden;
        margin-top: -50px;
    }

    * {
        font-family: 'cairo';
    }

    .visitor {
        color: white;
        font-size: 15px;

    }

</style>
@endsection


@section('dd')
<h2 class="logo" style="font-family: Al-Jazeera-Arabic"><a href="{{ route('showHome') }}"
        style="font-family: Al-Jazeera-Arabic; color: aliceblue"> Learn <span
            style="font-family: Al-Jazeera-Arabic;font-weight: 760;color: #Fd4;"> up
        </span></a></h2>
@endsection
@section('con', 'display: none')

@section('content')


@section('Top')

<section id="topbar">
<div class="container d-flex justify-content-center justify-content-md-between" id="topb">
    <div class="contact-info d-flex align-items-center">
        <a href="mailto:ahmed8051741@gmail.com" class="visitor"> &nbsp; الدعم الفني عبر البريد الالكتروني </a>
    </div>
    <div class="d-flex flex-row align-items-center feed-text px-2" dir="ltr">
        &nbsp;&nbsp;
        <div class="d-flex flex-column flex-wrap ml-2"><span class="font-weight-bold">
                <div class="dropdown">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" style="color: #ffffff;"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="rounded-circle" src="{{ asset('assets/img/my.jpeg') }}" width="42">
                        &nbsp;&nbsp;
                        {{ auth()->user()->name }} &nbsp;</a>

                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="{{ route('showprofile') }}">الملف الشخصي</a>
                        <hr style="margin-top:0.8px;margin-bottom:0.8px">
                        <a class="dropdown-item" href="{{ route('services.create') }}">اضافة خدمة جديد</a>
                        <a class="dropdown-item" href="{{ route('control') }}">لوحة القيادة</a>
                        <a class="dropdown-item" href="{{ route('logout_log') }}">تسجيل الخروج</a>
                    </div>
                </div>
            </span>

        </div>
    </div>
</div>
</section>
@endsection

<div class="container bootstrap snippets bootdey">
<div class="row">
<div class="profile-nav col-md-3">
    <div class="panel">
        <div class="user-heading round">
            <a href="#">
                <img src="{{ asset('storage/image_profile/' . auth()->user()->image) }}" alt="">
            </a>
            <h1> {{ auth()->user()->name }} </h1>
            <p>{{ auth()->user()->email }} </p>
        </div>

        <ul class="nav">
            <li><a href="{{ route('showprofile') }}"><i class="fa fa-user"> &nbsp;</i> الملف الشخصي</a></li>

            @if (auth()->user()->id == auth('teacher')->id())
                <li><a href="{{ route('show.teacher', auth()->user()->id) }}"><i class="fas fa-eye">
                            &nbsp;</i>
                        عرض ملفك الشخصي كما يظهر للاخرين
                    </a></li>

            @else
                <li><a href="{{ route('show.std', auth()->user()->id) }}"><i class="fas fa-eye">
                            &nbsp;</i>
                        عرض ملفك الشخصي كما يظهر للاخرين
                    </a></li>
            @endif




            <li class="active"><a href="{{ route('Edite_Profile') }}"><i class="fa fa-edit"> &nbsp;</i> تعديل
                    الملف
                    الشخصي</a></li>

            <li><a href="{{ route('image.index') }}"><i class="far fa-images"> &nbsp;</i>
                    تعديل صورة الملف
                    الشخصي</a></li>
            <li><a href="{{ route('password_edit') }}"><i class="fas fa-lock"> &nbsp;</i> تعديل كلمه
                    المرور</a></li>
            <li><a href="{{ route('control') }}"><i class="fa fa-calendar"> &nbsp;</i> العودة الى لوحة التحكم
                </a></li>
            <li><a href="{{ route('logout_log') }}"><i class="fas fa-sign-out-alt"> &nbsp;</i> تسجيل
                    الخروج</a></li>

        </ul>
    </div>
</div>
<div class="profile-info col-md-9">
    <div class="panel">
        <div class="bg-info clearfix">
            <button type="button" class="btn btn-secondary float-right"> المعلومات الشخصية </button>
        </div>
    </div>
    <div class="panel">

        <div class="panel-body bio-graph-info">
            <div style="margin: 5px; margin-right: 22px ; margin-top: 20px">
                <h4> تعديل بيناتك الشخصية </h4>
                <div align="center">
                    <hr width="96%">
                </div>
                <div class="row" style="margin: 5px;margin-top: 25px">

                    <div class="row">
                        <div class="col-md-12">
                            <form>

                                <p>
                                    ملاحظه هامه
                                    <span> يعد رقم الهاتف هو وسيلة الاتصال الرئيسية لدينا لا تنسا وضع رقم الهاتف
                                    </span>

                                </p>


                                <div class="form-group row">
                                    <label for="name" class="col-4 col-form-label mb-2"> الاسم </label>
                                    <div class="col-8">
                                        <input id="name" placeholder="اسمك ثلاثي" class="form-control"
                                            required="required" type="text"
                                            value="{{ auth()->user()->name }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-4 col-form-label mb-2">البريد
                                        الالكتروني</label>
                                    <div class="col-8">
                                        <input id="email" placeholder="بريدك الالكتروني"
                                            class="form-control here" type="text"
                                            value="{{ auth()->user()->email }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="smobile" class="col-4 col-form-label mb-2"> مقدمه رقم الهاتف
                                    </label>
                                    <div class="col-8">
                                        <select id="smobile" name="smobile" class="form-select">
                                            <option value="{{ auth()->user()->s_mobile }}">
                                                {{ auth()->user()->s_mobile }} </option>
                                            <option value="970"> فلسطين 970</option>
                                            <option value="972"> الأراضي الفلسطينة المحتله 972 </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="mobile" class="col-4 col-form-label mb-2">رقم الهاتف </label>
                                    <div class="col-8">
                                        <input id="mobile" placeholder="رقم الهاتف" class="form-control here"
                                            type="text" value="{{ auth()->user()->mobile }}">
                                    </div>
                                </div>




                                <div class="form-group row">
                                    <label for="select" class="col-4 col-form-label mb-2"> التخصص (مجال عملك -
                                        اذا كنت طالب فادخل هوايتك) </label>
                                    <div class="col-8">
                                        <select id="select" name="select" class="form-select">
                                            <option value="{{ auth()->user()->major }}">
                                                {{ auth()->user()->major }} </option>
                                            @foreach ($categorise as $categoris)
                                                <option value=" {{ $categoris->name }} ">
                                                    {{ $categoris->name }} </option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="facebook" class="col-4 col-form-label mb-2"> رابط حساب الفيس بوك
                                        الخاص بك </label>
                                    <div class="col-8">
                                        <input id="facebook"
                                            placeholder="https://www.facebook.com/user (أختياري)"
                                            class="form-control here" type="text"
                                            value="{{ auth()->user()->facebook }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="instagram" class="col-4 col-form-label mb-2"> رابط حساب
                                        الانستجرام الخاص بك </label>
                                    <div class="col-8">
                                        <input id="instagram"
                                            placeholder="https://www.instagram.com/user (أختياري)"
                                            class="form-control here" type="text"
                                            value="{{ auth()->user()->instagram }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="youtube" class="col-4 col-form-label mb-2"> رابط قناة اليوتيوب
                                        الخاص بك </label>
                                    <div class="col-8">
                                        <input id="youtube"
                                            placeholder="https://www.youtube.com/channel/user (أختياري)"
                                            class="form-control here" type="text"
                                            value="{{ auth()->user()->youtube }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="describe" class="col-4 col-form-label mb-2"> نبذة عنك </label>
                                    <div class="col-8">
                                        <textarea id="describe" cols="40" rows="4" class="form-control"
                                            placeholder=" ستظهر في ملفك الشخصي ">{{ auth()->user()->describe }}</textarea>
                                    </div>
                                </div>
                                <br>

                                <br>

                                <button type="button" onclick="update({{ auth()->user()->id }})"
                                    class="btn btn-primary" id="button" style="float: left; width: 100%"> تحديث
                                    البينات </button>
                                <br>
                                <br>
                                <br>
                            </form>
                        </div>
                    </div>


                </div>
            </div>
        </div>


        <div>

        </div>
    </div>
</div>
</div>

<br>
<br>
<br>

</div>

@endsection


@section('JS')
<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-46156385-1', 'cssscript.com');
    ga('send', 'pageview');
</script>



@section('JS')

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
</script>


<script>
    function update(id) {
        axios.put('/profile/profile_setting/' + id, {
                name: document.getElementById('name').value,
                email: document.getElementById('email').value,
                smobile: document.getElementById('smobile').value,
                mobile: document.getElementById('mobile').value,
                select: document.getElementById('select').value,
                facebook: document.getElementById('facebook').value,
                instagram: document.getElementById('instagram').value,
                youtube: document.getElementById('youtube').value,
                describe: document.getElementById('describe').value,
            })
            .then(function(response) {
                // handle success
                console.log(response);
                toastr.success(response.data.message);
                setTimeout(function() {
                    window.location.href = "/profile";
                }, 1500);
            })
            .catch(function(error) {
                // handle error
                console.log(error);
                toastr.error(error.response.data.message);
            })
            .then(function() {
                // always executed
            });
    }
</script>




@endsection
