@extends('admins.parent')
@section('title', 'تعديل بينات المشرف')
@section('bige-title', 'تعديل بينات المشرف')
@section('main-page', 'الرئيسية')
@section('sub-page', 'تعديل المشرف')
@section('profile', 'Active')

@section('content')
<!-- Main content -->
<section class="content">
<div class="container-fluid">
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title" style="float: right">تعديل بينات المشرف</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form id="create-form">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="name">الاسم</label>
                <input type="text" class="form-control" id="name" placeholder="ادخل الاسم"
                    value="{{ $admin->name }}">
            </div>

            <div class="form-group">
                <label for="name">البريد الالكتروني</label>
                <input type="text" class="form-control" id="email" placeholder="ادخل البريد الالكتروني"
                    value="{{ $admin->email }}">
            </div>

            {{-- @if ($admin->id != auth('admin')->id()) --}}


            <br>
            <div class="form-group">
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="active" @if ($admin->id == auth('admin')->id()) disabled @endif @if ($admin->Active) checked @endif>
                    <label class="custom-control-label" for="active">تفعيل الحساب</label>
                </div>
            </div>
            {{-- @endif --}}

        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            {{-- <button type="button" onclick="update({{$admin->id}}, '{{$admin->id !=auth('admin')->id()}}')" class="btn btn-primary" id="button">Update</button> --}}
            {{-- <button type="button" onclick="update({{$admin->id}},'{{$redirect ?? true}}')" class="btn btn-primary" id="button">Update</button> --}}
            <button type="button" onclick="Update()" class="btn btn-primary" id="button"> تحديث
                البينات</button>

        </div>
    </form>
</div>


</div>

</div>
<!-- /.row -->
</div><!-- /.container-fluid -->
</section>

@endsection

@section('JS')
<script>
    function Update() {
        axios.put('/admin/Update-profile/', {
                name: document.getElementById('name').value,
                email: document.getElementById('email').value,
                active: document.getElementById('active').checked
            })
            .then(function(response) {
                // handle success
                console.log(response);
                toastr.success(response.data.message);
                document.getElementById('create-form').reset(); // تصفير قيمة الفورم سبمت
                // window.location.href="/cms/admin/Update-password/"; // التحويل والانتقال
            })
            .catch(function(error) {
                // handle error
                console.log(error);
                toastr.error(error.response.data.message);
            })
            .then(function() {
                // always executed
            });
    }


    // function Update() {
    //     let formData = new FormData();
    //     formData.append('name', document.getElementById('name').value);
    //     formData.append('email', document.getElementById('email').value);
    //     formData.append('active', document.getElementById('active').checked ? 1 : 0);
    //     formData.append('images', document.getElementById('images').files[0]);
    //     axios.put('/admin/Update-profile/', formData)
    //         .then(function(response) {
    //             // handle success
    //             console.log(response);
    //             toastr.success(response.data.message);
    //             document.getElementById('create-form').reset();
    //         })
    //         .catch(function(error) {
    //             // handle error
    //             console.log(error);
    //             toastr.error(error.response.data.message);
    //         })
    //         .then(function() {
    //             // always executed
    //         });
    // }
</script>

@endsection
