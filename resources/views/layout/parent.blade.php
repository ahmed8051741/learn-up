 <!doctype html>
 <!-- =======================================================
* Template Name: Learn up - v->experimental .
* Created by : Ayat Abu Qasim - Ahmed Farhat - Osama Abu Tawahinah .
* Occasion : Our graduation project for the 2021 academic year .
* Type : CSS file .
  ======================================================== -->
 <html dir="rtl">

 <head>
     <meta charset="utf-8">
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width">
     <title> Learn up - @yield('Title')</title>

     <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
          integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
     </script>
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
     </script>
     <link rel="stylesheet" href="{{ asset('CMS/plugins/fontawesome-free/css/all.min.css') }}">

     <!-- Favicons - الايقونات  -->
     <link href="{{ asset('assets/img/favicon.png') }}" rel="icon">
     <link href="{{ asset('assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">
     <!-- Google Fonts- الخطوط -->
     <link rel="preconnect" href="https://fonts.gstatic.com">
     <!--css file ملفات ال Css-->
     <link href="{{ asset('assets/css/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
     <link href="{{ asset('assets/css/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
     <script src="https://kit.fontawesome.com/a81368914c.js"></script>
     <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
     <script src="{{ asset('CMS/plugins/jquery/jquery.min.js') }}"></script>
     <link href="{{ asset('assets/assets/vendor/aos/aos.css') }}" rel="stylesheet">


     <link rel="stylesheet" href="{{ asset('CMS/plugins/toastr/toastr.min.css') }}">

     <style>
         .dropdown-menu {

             text-align: right;
             right: 0px;
             min-width: 11rem;

         }

         .notification .badge {
             position: absolute;
             top: -10px;
             right: -10px;
             padding: 5px 5px;
             border-radius: 50%;
             background: red;
             color: white;
         }

         .notification {
             color: white;
             text-decoration: none;
             position: relative;
             display: inline-block;
             border-radius: 2px;
         }

         .notification:hover {
             color: #b8b8b8;
         }

         #new {
             width: 300px;
             transform: translate3d(-30px, 20px, 0px);
         }

         .dropdown-menu {

             border: 1px solid rgba(160, 160, 160, 0.144);
         }

         .dropdown-item {
             clear: inherit;

         }

         #ahmed {
             border-bottom: solid rgba(255, 255, 255, 0.76) 1px;

         }

         #ahmed2 {
             background-color:
                 rgb(228, 227, 227)
         }

         #ahmed2:hover {
             background-color:
                 rgba(235, 235, 235, 0.671)
         }

         #go {
             margin-top: 2px;
         }

     </style>
     @yield('CSS')

 </head>

 <body>

     @guest

         <section id="topbar">
             <div class="container d-flex justify-content-center justify-content-md-between" id="topb">
                 <div class="contact-info d-flex align-items-center">
                     <a href="mailto:ahmed8051741@gmail.com" class="visitor" id="go"> &nbsp; تواصل مع الدعم الفني
                     </a>

                 </div>
                 <a href="#" type="button" onclick="go()" href="" class="btn btn-success" id="go"> الاشتراك / تسجيل الدخول
                 </a>
             </div>
         </section>

     @endguest

     @auth

         <section id="topbar">
             <div class="container d-flex justify-content-center justify-content-md-between" id="topb">
                 <div class="contact-info d-flex align-items-center">


                     @yield('dd')
                 </div>
                 <div class="d-flex flex-row align-items-center feed-text px-2" dir="ltr">
                     &nbsp;&nbsp;
                     <div class="d-flex flex-column flex-wrap ml-2"><span class="font-weight-bold">
                             <div class="dropdown">
                                 <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                     style="color: #ffffff;" data-toggle="dropdown" aria-haspopup="true"
                                     aria-expanded="false">
                                     <img class="rounded-circle"
                                         src="{{ asset('storage/image_profile/' . auth()->user()->image) }}" width="44">
                                     &nbsp;&nbsp;
                                     {{ auth()->user()->name }} &nbsp;</a>

                                 <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                     <a class="dropdown-item" href="{{ route('showprofile') }}">الملف الشخصي</a>
                                     <hr style="margin-top:0.8px;margin-bottom:0.8px">

                                     @if (auth()->user()->id == auth('teacher')->id())

                                         <a class="dropdown-item" href="{{ route('services.create') }}">اضافة خدمة
                                             جديد</a>

                                     @else
                                         <a class="dropdown-item" href="{{ route('order.create') }}">اضافة طلب
                                             جديد</a>
                                     @endif
                                     <a class="dropdown-item" href="{{ route('boxing.create') }}">صندوق الاقتراحات</a>

                                     <a class="dropdown-item" href="{{ route('control') }}">لوحة القيادة</a>
                                     <a class="dropdown-item" href="{{ route('logout_log') }}">تسجيل الخروج</a>
                                 </div>
                                 &nbsp;


                                 <div class="notification dropdown">
                                     <a id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                         aria-expanded="false">
                                         <span class="badge">
                                             <?php $i = 0; ?>
                                             @foreach ($Reserves as $Reserve)
                                                 @if (auth()->user()->id == $Reserve->teacher_id)
                                                     @if (auth()->user()->account_type == 'teacher')
                                                         @if ($Reserve->reade != 1)
                                                             <?php
                                                             $i++;
                                                             ?>
                                                         @endif
                                                     @endif
                                                 @endif
                                             @endforeach

                                             @foreach ($Notices as $Notice)
                                                 @if (auth()->user()->id == $Notice->req_send)
                                                     @if (auth()->user()->account_type == $Notice->req_send_type)
                                                         @if ($Notice->read != 1)
                                                             <?php $i++; ?>
                                                         @endif
                                                     @endif
                                                 @endif
                                             @endforeach
                                             {{ $i }}

                                         </span>
                                         <i class="fas fa-bell"></i>

                                     </a>
                                     <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="new"
                                         style="padding-top:20px">
                                         @if (auth()->user()->account_type == 'teacher')
                                             <a href="{{ route('read_all', auth()->user()->id) }}"
                                                 style="padding: 8px;font-size: 12px;padding-bottom: 8px"> تحديد
                                                 الكل كمقروء </a>
                                         @endif

                                         @if (auth()->user()->account_type == 'student')
                                             <a href="{{ route('read_allstd', auth()->user()->id) }}"
                                                 style="padding: 8px;font-size: 12px;padding-bottom: 8px"> تحديد
                                                 الكل كمقروء </a>
                                         @endif
                                         @foreach ($Reserves as $Reserve)
                                             @if (auth()->user()->id == $Reserve->teacher_id)
                                                 @if (auth()->user()->account_type == 'teacher')
                                                     <a href="{{ route('show.CertainPro', $Reserve->id) }}"
                                                         class="dropdown-item" style="max-width: 100%;" @if ($Reserve->reade != 1) id="ahmed2" @endif>
                                                         <div class="media" id="ahmed">
                                                             <div class="media-body">
                                                                 <p class="text-sm" style="font-size: 13px;padding-top:5px"
                                                                     dir="rtl">

                                                                     @if ($Reserve->account_type == 'teacher')
                                                                         تقدم المعلم/ة
                                                                         {{ $Reserve->teacher->name }}
                                                                         <br>
                                                                         @if ($Rqserv->Service->title != null)
                                                                             {{ $Rqserv->Service->title }}
                                                                         @else
                                                                             لقد تم ايقاف هذه الخدمه
                                                                         @endif
                                                                     @endif
                                                                     @if ($Reserve->account_type == 'student')

                                                                         تقدم الطالب/ة {{ $Reserve->Stud->name }}
                                                                         <br>
                                                                         لخدمه {{ $Reserve->Service->title }} التي
                                                                         تقدمها

                                                                     @endif
                                                                 </p>

                                                                 <p style="margin-top: -13px ; float: left;font-size: 10px">
                                                                     <i class="far fa-clock mr-1"></i>
                                                                     {{ $Reserve->created_at->diffForHumans() }}
                                                                 </p>

                                                             </div>
                                                         </div>
                                                         <!-- Message End -->
                                                     </a>
                                                 @endif

                                             @endif

                                         @endforeach

                                         @foreach ($Notices as $Notice)
                                             @if (auth()->user()->id == $Notice->req_send)
                                                 @if (auth()->user()->account_type == $Notice->req_send_type)
                                                     <!-- Message Start -->
                                                     <a href="{{ route('show.CertainProstd', $Notice->id) }}"
                                                         class="dropdown-item" style="max-width: 100%;white-space:unset;"
                                                         @if ($Notice->read != 1) id="ahmed2" @endif>
                                                         <div class="media" id="ahmed">
                                                             <div class="media-body">
                                                                 <p class="text-sm" style="font-size: 13px;padding-top:5px"
                                                                     dir="rtl">
                                                                     @if ($Notice->send == 'teacher')

                                                                         @if ($Notice->title == null)

                                                                             @if ($Notice->Status == 1)
                                                                                 قام المعلم
                                                                                 {{ $Notice->send_name }}
                                                                                 : بقبول طلبك في خدمه
                                                                                 {{ $Notice->serves }}
                                                                             @endif
                                                                             @if ($Notice->Status == 2)
                                                                                 قام المعلم
                                                                                 {{ $Notice->send_name }}
                                                                                 : بتعليق طلبك في خدمه
                                                                                 {{ $Notice->serves }}
                                                                             @endif
                                                                             @if ($Notice->Status == 3)
                                                                                 قام المعلم
                                                                                 {{ $Notice->send_name }}
                                                                                 : برفض طلبك في خدمه
                                                                                 {{ $Notice->serves }}
                                                                             @endif
                                                                             @if ($Notice->Status == null)
                                                                                 قام المعلم
                                                                                 {{ $Notice->send_name }}
                                                                                 : بتقديم عرض على طلبك الذي بعنوان
                                                                                 {{ $Notice->serves }}
                                                                             @endif


                                                                         @else
                                                                             من خلال : {{ $Notice->send_name }}

                                                                             {{ $Notice->title }}
                                                                         @endif
                                                                     @endif


                                                                 </p>

                                                                 <p style="margin-top: -12px ; float: left;font-size: 10px">
                                                                     <i class="far fa-clock mr-1"></i>
                                                                     {{ $Notice->created_at->diffForHumans() }}
                                                                 </p>
                                                             </div>
                                                         </div>
                                                         <!-- Message End -->
                                                     </a>
                                                 @endif

                                             @endif

                                         @endforeach
                                         <hr>

                                         <div style="text-align: center">
                                             @if (auth()->user()->account_type == 'teacher')
                                                 <a href="{{ route('view_all', auth()->user()->id) }}"
                                                     style="font-size: 13px ;"> عرض جميع الاشعارات
                                                 </a>
                                             @endif

                                             @if (auth()->user()->account_type == 'student')
                                                 <a href="{{ route('view_allstd', auth()->user()->id) }}"
                                                     style="font-size: 13px ;"> عرض
                                                     جميع الاشعارات
                                                 </a>
                                             @endif
                                         </div>
                                     </div>



                                 </div>
                         </span>

                     </div>
                 </div>
             </div>
         </section>

     @endauth


     <header id="header" class="d-flex align-items-center" dir="ltr">

         <div class="container d-flex align-items-center justify-content-between">

             <h1 class="logo" style="font-family: Al-Jazeera-Arabic"><a href="#" style="font-family: Al-Jazeera-Arabic">
                     Learn <span style="font-family: Al-Jazeera-Arabic;font-weight: 760;"> up
                     </span></a></h1>

             <nav class="d-flex align-items-center navbar navbar-expand-lg navbar-light bg-light" id="navbar">
                 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                     aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="navbar-toggler-icon"></span>
                 </button>
                 <div class="navbar collapse navbar-collapse" id="navbarText">
                     <ul class="navbar-nav mr-auto" dir="rtl">
                         <li class="nav-item active nav-link">
                             @guest
                                 <a class="nav-link @yield('active')" href="{{ route('Home') }}">الرئيسية</a>
                             @endguest
                             @auth
                                 <a class="nav-link @yield('active')" href="{{ route('showindex') }}">الرئيسية</a>
                             @endauth
                         </li>
                         <li class="nav-item nav-link">
                             @guest
                                 <a class="nav-link @yield('w')" href="{{ route('Home') }}#team">من نحن</a>
                             @endguest
                             @auth
                                 <a class="nav-link @yield('w')" href="{{ route('showindex') }}#team">من نحن </a>
                             @endauth

                         </li>
                         <li class="nav-item nav-link">
                             @auth
                                 <a class="nav-link @yield('sarve')" href="{{ route('showHome') }}"> المعلمين وخدماتهم
                                 </a>
                             @endauth
                             @guest
                                 <a class="nav-link @yield('sarve')" href="{{ route('showHome_vs') }}"> المعلمين وخدماتهم
                                 </a>
                             @endguest
                         </li>
                         <li class="nav-item nav-link">
                             @auth

                                 <a class="nav-link @yield('req')" href="{{ route('requests_Std') }}"> الطلاب واحتيجاتهم
                                 </a>
                             @endauth
                             @guest
                                 <a class="nav-link @yield('req')" href="{{ route('requests_Std_vs') }}"> الطلاب
                                     واحتيجاتهم

                                 </a>
                             @endguest

                         </li>

                     </ul>

                 </div>
             </nav>
         </div>
     </header>


     @yield('content')


     <!-- ======= Footer ======= -->
     <div style="@yield('con')">

         <footer id="footer">
             <h1>
                 {{-- {{ session()->get('email') }}
                 {{ session()->get('email') }} --}}

             </h1>
             @yield('Footer')
             <div class="footer-top" id="footer-top">
                 <div class="container">
                     <div class="row">


                         <div class="col-lg-5 col-md-6 footer-links">
                             <h4> الصفحات </h4>
                             <ul>
                                 <li><i class="bx bx-chevron-right"></i> <a href="#">معلومات عنا</a></li>
                                 <li><i class="bx bx-chevron-right"></i> <a href="#">ابق آمنًا</a></li>
                                 <li><i class="bx bx-chevron-right"></i> <a href="#">مدونة</a></li>
                                 <li><i class="bx bx-chevron-right"></i> <a href="#">ارجع واكسب عملات معدنية </a></li>
                                 <li><i class="bx bx-chevron-right"></i> <a href="#">أسئلة وأجوبة</a></li>
                                 <li><i class="bx bx-chevron-right"></i> <a href="#">العملات المعدنية والتسعير</a></li>
                                 <li><i class="bx bx-chevron-right"></i> <a href="#">كيف يعمل - الطلاب</a></li>
                                 <li><i class="bx bx-chevron-right"></i> <a href="#">رواتب المعلمين</a></li>
                             </ul>
                         </div>

                         <div class="col-lg-4 col-md-6 footer-links">
                             <h4>للمعلمين</h4>
                             <ul>
                                 <li><i class="bx bx-chevron-right"></i> <a href="#">يتقاضون رواتبهم</a></li>
                                 <li><i class="bx bx-chevron-right"></i> <a href="#">عضوية مميزة</a></li>
                                 <li><i class="bx bx-chevron-right"></i> <a href="#">دليل التدريس عبر الإنترنت</a></li>
                                 <li><i class="bx bx-chevron-right"></i> <a href="#">كيف يعمل - المعلمين</a></li>
                                 <li><i class="bx bx-chevron-right"></i> <a href="#">كيف تحصل على وظائف</a></li>
                                 <li><i class="bx bx-chevron-right"></i> <a href="#">التقديم على الوظائف</a></li>
                                 <li><i class="bx bx-chevron-right"></i> <a href="#">تصنيفات المدرسين</a></li>
                                 <li><i class="bx bx-chevron-right"></i> <a href="#">شارك قصة</a></li>
                             </ul>
                         </div>

                         <div class="col-lg-3 col-md-6 footer-links">
                             <h4>تواصل معنا </h4>
                             <p>يمكنك التواصل معنا بشكل مباشر عبر صفحات التواصل الاجتماعي خاصتنا</p>
                             <div class="social-links mt-3">
                                 <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                                 <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                                 <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                                 <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                                 <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="container py-4">
                 <div class="copyright" style="@yield('st')">
                     تصميم وبرمجة : <a href=""> &nbsp; فريق المبدع </a>
                 </div>

                 <div class="credits" style="@yield('st')">
                     حقوق النشر محفوظة لدينا © تحيات فريق المبدع - كلية فلسطين التقنية
                 </div>
             </div>

         </footer>
         <div>

             <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
             <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

             <script src="{{ asset('assets/toastr/toastr.min.js') }}"></script>

             <script>
                 function go() {
                     Swal.fire({
                         title: 'كيف تود تسجيل الدخول او انشاء حساب',
                         showDenyButton: true,
                         showCancelButton: false,
                         confirmButtonColor: '#155b92',
                         denyButtonColor: '#0f5a69',
                         confirmButtonText: `حساب معلم`,
                         denyButtonText: `حساب طالب`,
                     }).then((result) => {
                         /* Read more about isConfirmed, isDenied below */
                         if (result.isConfirmed) {
                             window.location.href = "/teacher/login"; // التحويل والانتقال
                         } else if (result.isDenied) {
                             window.location.href = "/student/login"; // التحويل والانتقال
                         }
                     })
                 }
             </script>
             @yield('JS')

             <script src="{{ asset('assets/assets/vendor/aos/aos.js') }}"></script>
             <script src="{{ asset('assets/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
             <script src="{{ asset('assets/assets/vendor/glightbox/js/glightbox.min.js') }}"></script>
             <script src="{{ asset('assets/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
             <script src="{{ asset('assets/assets/vendor/php-email-form/validate.js') }}"></script>
             <script src="{{ asset('assets/assets/vendor/purecounter/purecounter.js') }}"></script>
             <script src="{{ asset('assets/assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
             <script src="{{ asset('assets/assets/js/main.js') }}"></script>
 </body>
