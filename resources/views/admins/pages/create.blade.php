@extends('admins.parent')

@section('title', 'Create Page')
@section('bige-title', 'Create Page')
@section('main-page', 'Home')
@section('sub-page', 'Page')
@section('CSS')
    <link rel="stylesheet" href="{{ asset('CMS/plugins/summernote/summernote-bs4.min.css') }}">

@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
        <form id="create-form">
            <div class="col-md-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Create a new page</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Name Page</label>
                            <input class="form-control" placeholder="Name Page:" id="name">
                            <input class="form-control" value="{{ auth('admin')->user()->name }}" hidden id="name_admin">
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="name">Add your page content</label>
                            <textarea id="compose_textarea" class="form-control" style="height: 300px">
                            <h1><u>Home page title</u></h1>
                            <h1 style="text-align: right; "> <u>عنوان الصفحة الرئيسي</u></h1>
                            <h4>Subheading</h4>
                          <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain
                            was born and I will give you a complete account of the system, and expound the actual teachings
                            of the great explorer of the truth, the master-builder of human happiness. No one rejects,
                            dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know
                            how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again
                            is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain,
                            but because occasionally circumstances occur in which toil and pain can procure him some great
                            pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise,
                            except to obtain some advantage from it? But who has any right to find fault with a man who
                            chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that
                            produces no resultant pleasure? On the other hand, we denounce with righteous indignation and
                            dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so
                            blinded by desire, that they cannot foresee</p>
                          <ul>
                            <li>List item one</li>
                            <li>List item two</li>
                            <li>List item three</li>
                            <li>List item four</li>
                          </ul>

                        </textarea>
                        </div>
                        <!-- /.card-body -->
                        <div class="form-group">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="active">
                                <label class="custom-control-label" for="active">Active</label>
                            </div>
                        </div>
                        <div class="card-footer col-md-12">
                            <div class="float-right col-md-12">
                                <button type="button" onclick="store()" class="btn btn-primary col-md-12"><i
                                        class="fas fa-pencil-alt"></i> Save and Post</button>
                            </div>

                        </div>

                        <!-- /.card-footer -->
                    </div>
                    <!-- /.card -->
                </div>
        </form>

    </section>





@endsection

@section('JS')
    <script src="{{ asset('CMS/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('CMS/dist/js/demo.js') }}"></script>


    <script>
        $(function() {
            //Add text editor
            $('#compose_textarea').summernote()
        })

    </script>

    <script>
        function store() {
            axios.post('/admin/pages', {
                    name: document.getElementById('name').value,
                    name_admin: document.getElementById('name_admin').value,
                    name_admin: document.getElementById('name_admin').value,
                    active: document.getElementById('active').checked,
                    compose_textarea: document.getElementById('compose_textarea').value,

                })
                .then(function(response) {
                    // handle success
                    console.log(response);
                    toastr.success(response.data.message);
                    // document.getElementById('create-form').reset(); // تصفير قيمة الفورم سبمت
                    window.location.href = "/admin/pages"; // التحويل والانتقال
                })
                .catch(function(error) {
                    // handle error
                    console.log(error);
                    toastr.error(error.response.data.message);
                })
                .then(function() {
                    // always executed
                });
        }

    </script>
@endsection
