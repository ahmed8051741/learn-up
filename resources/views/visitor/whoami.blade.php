@extends('layout.parent')


@section('Title', 'من نحن')


@section('CSS')
    <link href="{{ asset('assets/css/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">


    <style>
        .bg-white {
            background-color: #ffffff !important;
        }


        .time {
            font-size: 9px !important
        }

        .socials i {
            margin-right: 14px;
            font-size: 17px;
            color: #d2c8c8;
            cursor: pointer
        }

        .feed-image img {
            width: 100%;
            height: auto
        }

        body {
            /* background-color: #eee */
            background-image: url(../assets/img/2.jpg);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: cover;
        }

    </style>

@endsection

@section('dd')
    <a href="mailto:ahmed8051741@gmail.com" class="visitor"> &nbsp; الدعم الفني عبر البريد الالكتروني
    </a>
@endsection
@section('active', 'active')

@section('content')




    <div class="container mt-4 mb-5 ">
        <div class="d-flex justify-content-center row">
            <div class="col-md-9">
                <div class="feed p-2">
                    <div class="bg-white mt-2">
                        <div class="d-flex flex-row justify-content-between align-items-center p-2 bg-white border">
                            <div class="feed-text px-2" style="border-right: 5px solid #002A56 ;border-radius: 8px;">
                                <h2 class="text-black-60 mt-2"> من نحن...؟ </h2>
                            </div>

                        </div>
                        <div class="p-4 px-4">
                            <span style="line-height:2.4">
                                <b>فريق المبدع </b>
                                <p> نحن طلاب كلية فلسطين التقنية قسم الحاسوب تخصص تصميم وتطوير مواقع الويب </p>
                                <p>صممنا موقع Lern up : هو موقع تعليمي عن بعد تم تطبيقه عام 2021 حيث يمكن للطالب الدخول
                                    والبحث عن مدرس
                                    معين او مدرس يدرس منهاج (مادة) معين/ة بالطريقة التي تمكنه من الاستفادة والفهم منها ومن
                                    مميزاته يمكن
                                    للطالب دخوله في اي وقت كان نظرا للظروف مثل انقطاع الكهرباء – وتوفر الكثير من المدرسين
                                    للمنهاج الواحد
                                    ومن ناحية أخرى يمكن للمعلم انشاء دورة تعليمية تستهدف فئة معينة من الطلاب .</p>
                            </span>
                        </div>
                        <div class="d-flex justify-content-end socials p-2 py-3">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- <div class="container mt-4 mb-5 ">
                                                                                                                                                                                      <div class="d-flex justify-content-center row">
                                                                                                                                                                                          <div class="col-md-9">
                                                                                                                                                                                              <div class="feed p-2">

                                                                                                                                                                                                <div class="card-group">
                                                                                                                                                                                                  <div class="card">
                                                                                                                                                                                                    <img src="..." class="card-img-top" alt="...">
                                                                                                                                                                                                    <div class="card-body">
                                                                                                                                                                                                      <h5 class="card-title">Card title</h5>
                                                                                                                                                                                                      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                    <div class="card-footer">
                                                                                                                                                                                                      <small class="text-muted">Last updated 3 mins ago</small>
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                  </div>
                                                                                                                                                                                                  <div class="card">
                                                                                                                                                                                                    <img src="..." class="card-img-top" alt="...">
                                                                                                                                                                                                    <div class="card-body">
                                                                                                                                                                                                      <h5 class="card-title">Card title</h5>
                                                                                                                                                                                                      <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                    <div class="card-footer">
                                                                                                                                                                                                      <small class="text-muted">Last updated 3 mins ago</small>
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                  </div>
                                                                                                                                                                                                  <div class="card">
                                                                                                                                                                                                    <img src="..." class="card-img-top" alt="...">
                                                                                                                                                                                                    <div class="card-body">
                                                                                                                                                                                                      <h5 class="card-title">Card title</h5>
                                                                                                                                                                                                      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                    <div class="card-footer">
                                                                                                                                                                                                      <small class="text-muted">Last updated 3 mins ago</small>
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                  </div>
                                                                                                                                                                                                </div>

                                                                                                                                                                                    </div>
                                                                                                                                                                                    </div>
                                                                                                                                                                                    </div>
                                                                                                                                                                                    </div> -->



    <div class="container mt-4 mb-5 ">
        <div class="d-flex justify-content-center row">
            <div class="col-md-9">
                <div class="feed p-2">

                    <div class="row row-cols-1 row-cols-md-3" style="text-align: center">
                        <div class="col mb-4">
                            <div class="card h-100">
                                <img src="{{ asset('assets/img/ayat.png') }}" class="card-img-top">
                                <div class="card-body">
                                    <h5 class="card-title">أيات ابو قاسم</h5>
                                    <p class="card-text">تخصص تصميم وتطوير مواقع الويب </p>
                                    <p class="card-text">خريجة كلية فلسطين التقنية دير البلح </p>
                                    <p class="card-text">2020/2021 </p>
                                </div>
                            </div>
                        </div>
                        <div class="col mb-4">
                            <div class="card h-100">
                                <img src="{{ asset('assets/img/my.jpeg') }}" class="card-img-top">
                                <div class="card-body">
                                    <h5 class="card-title">أحمد فرحات </h5>
                                    <p class="card-text">تخصص تصميم وتطوير مواقع الويب </p>
                                    <p class="card-text">خريج كلية فلسطين التقنية دير البلح </p>
                                    <p class="card-text">2020/2021 </p>
                                </div>
                            </div>
                        </div>
                        <div class="col mb-4">
                            <div class="card h-100">
                                <img src="{{ asset('assets/img/o.jpg') }}" class="card-img-top">
                                <div class="card-body">
                                    <h5 class="card-title">أسامة ابو طواحينة</h5>
                                    <p class="card-text">تخصص تصميم وتطوير مواقع الويب </p>
                                    <p class="card-text">خريج كلية فلسطين التقنية دير البلح </p>
                                    <p class="card-text">2020/2021 </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="container mt-4 mb-5 ">
        <div class="d-flex justify-content-center row">
            <div class="col-md-9">
                <div class="feed p-2">

                    <div class="card mb-3" style="max-width: 100%;">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <br>
                                <img src="{{ asset('assets/img/9.jpg') }}" class="card-img" width="100%">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">أهداف موقعنا </h5>
                                    <ol>
                                        <b>
                                            <li> تمكن الطلاب من اختيار المدرس الذي يراه مناسب</li>
                                        </b>
                                        <b>
                                            <li> توفير الوقت والجهد (حيث سيقوم المعلم بإجاد طلابه من خلالنا – وأيضاً يصل
                                                الطالب الى المعلم
                                                بسهولة)</li>
                                        </b>
                                        <b>
                                            <li> الرفع من مستوى انتشار العلم وسهولة تداوله في القطاع خاصة وفلسطين بشكل عام
                                            </li>
                                        </b>
                                        <b>
                                            <li> الوصل بين المعلمين والطلاب في كافة انحاء ومحفظات فلسطين </li>
                                        </b>
                                    </ol>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>


@endsection



@section('JS')

@endsection
