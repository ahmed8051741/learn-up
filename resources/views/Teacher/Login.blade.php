 <!doctype html>
 <!-- =======================================================
 * Template Name: Learn up - v->experimental .
 * Created by : Ayat Abu Qasim - Ahmed Farhat - Osama Abu Tawahinah .
 * Occasion : Our graduation project for the 2021 academic year .
 * Type : CSS file .
 ======================================================== -->
 <html dir="rtl">

 <head>
 <meta charset="utf-8">
 <!-- Bootstrap CSS -->
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
 integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
 <link rel="stylesheet" href="{{ asset('CMS/plugins/toastr/toastr.min.css') }}">
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/loginCss/style.css') }}">
 <script src="https://kit.fontawesome.com/a81368914c.js"></script>
 <link rel="preconnect" href="https://fonts.gstatic.com">
 <title> Learn up - تسجيل الدخول </title>

 </head>

 <body>
 <img class="wave" src="{{ asset('assets/svg/wave.png') }}" alt="imges">
 <div class="container">
 <div class="img">
 <img src="{{ asset('assets/svg/undraw_education_f8ru.svg') }}" alt="svg">
 </div>
 <div class="login-content">
 <form action="index.html">
 <img src="{{ asset('assets/svg/profile.svg') }}" alt="">
 <h2 class="title"> تسجيل الدخول</h2>
 <br>
 <div class="input-div one">
 <div class="i">
 <i class="fas fa-user"></i>
 </div>
 <div class="div">
 <h5>البريد الالكتروني</h5>
 <input type="email" class="input" id="email">
 </div>
 </div>
 <div class="input-div pass">
 <div class="i">
 <i class="fas fa-lock"></i>
 </div>
 <div class="div">
 <h5>كلمه المرور </h5>
 <input type="password" class="input" id="password">
 </div>
 </div>

 <!-- Default switch -->
 <div class="custom-control custom-switch">
 <input type="checkbox" class="custom-control-input" id="customSwitches">
 <label class="custom-control-label" for="customSwitches"> تذكر تسجيل الدخول </label>
 </div>

 <button type="button" onclick="login('{{ $guard }}')" class="btn" id="bt"
 style="font-family: cairo"> تسجيل الدخول </button>
 <button type="button" onclick="go()" class="btn" id="bt" style="font-family: cairo"> التسجيل في الموقع
 </button>

 </form>
 </div>
 </div>
 <!-- =======================================================
 * Template Name: Learn up - v->experimental .
 * Created by : Ayat Abu Qasim - Ahmed Farhat - Osama Abu Tawahinah .
 * Occasion : Our graduation project for the 2021 academic year .
 * Type : CSS file .
 ======================================================== -->
 <script type="text/javascript" src="{{ asset('assets/jquery/jquery.min.js') }}"></script>
 <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
 <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
 <script src="{{ asset('CMS/plugins/toastr/toastr.min.js') }}"></script>
 <script type="text/javascript" src="{{ asset('assets/LoginCss/main.js') }}"></script>


 <script>
     function login(guard) {
         axios.post('/login', {
                 email: document.getElementById('email').value,
                 password: document.getElementById('password').value,
                 customSwitches: document.getElementById('customSwitches').checked,
                 guard: guard
             })
             .then(function(response) {
                 // handle success
                 console.log(response);
                 toastr.success(response.data.message);
                 window.location.href = "/Home";
             })
             .catch(function(error) {
                 // handle error
                 console.log(error);
                 toastr.error(error.response.data.message);
             })
             .then(function() {
                 // always executed
             });
     }
 </script>

 <script>
     function go() {
         Swal.fire({
             title: 'اختـر نوع الحساب الذي تود انشاؤه',
             showDenyButton: true,
             showCancelButton: false,
             confirmButtonColor: '#155b92',
             denyButtonColor: '#0f5a69',
             confirmButtonText: `حساب معلم`,
             denyButtonText: `حساب طالب`,
         }).then((result) => {
             /* Read more about isConfirmed, isDenied below */
             if (result.isConfirmed) {
                 window.location.href = "/register/teacher/create"; // التحويل والانتقال
             } else if (result.isDenied) {
                 window.location.href = "/register/student/create"; // التحويل والانتقال
             }
         })
     }
 </script>



 <style>
 .toast toast-error {
 direction: rtl;
 font-size: 10px
 }

 </style>
 </body>

 </html>
