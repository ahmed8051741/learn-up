<?php

namespace App\Http\Controllers;

use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $teacher = Teacher::where('id', '!=', auth('teacher')->id())->get();
        return response()->view('Teacher.Register', ['teachers' => $teacher]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->view('Teacher.Register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator(
            $request->all(),
            [
                'name' => 'required|string|min:8',
                'email' => 'required|string|min:3|max:90|email',
                'password' => 'required|string|min:6|max:16|confirmed',

            ],
            [
                'name.required' => 'يجب عليك ادخال اسمك ثلاثي',
                'name.min' => 'يجب عليك ادخال اسمك ثلاثي',
                'email.required' => 'يجب عليك تعبئة حقل البريد الالكتروني',
                'email.min' => 'تأكد من صيغة البريد الالكتروني',
                'email.max' => 'تأكد من صيغة البريد الالكتروني',
                'email.email' => 'الرجاء ادخال بريد الكتروني صالح',
                'password.required' => 'عذاً عليك تعين كلمة مرور تتكون من 6-18 حرفاً ',
                'password.min' => 'قم بوضع كلمة مرور ما بين 6-16 رقم وحرف ورمز',
                'password.max' => 'قم بوضع كلمة مرور ما بين 6-16 رقم وحرف ورمز',
                'password.confirmed' => 'كلمتان المرور غير متطابقتان',
            ]
        );
        if (!$validator->fails()) {

            $Teacher = new Teacher();
            $Teacher->name = $request->get('name');
            $Teacher->email = $request->get('email');
            $Teacher->password = Hash::make($request->get('password'));
            $isSaved = $Teacher->save();

            return response()->json([
                'message' => $isSaved ? "تم انشاء حسابك بنجاح ...حظاً سعيداً" : "عذاً تعذر انشاء الحساب لسبب مجهول"
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            //VALIDATION FAILED
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        return response()->view('Show_profile', ['teacher' => $teacher]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        return response()->view('admins.teacher.edit', ['teacher' => $teacher]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Teacher $teacher)
    {
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:3|max:30',
            'email' => 'required|email',
            'active' => 'required|boolean',
        ]);
        if (!$validator->fails()) {
            $teacher->name = $request->get('name');
            $teacher->email = $request->get('email');
            $teacher->active = $request->get('active');
            $isSaved = $teacher->save();
            return response()->json([
                'message' => $isSaved ? "تمت التحديث بنجاح" : "فشل التحديث"
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json(['message' => $validator->getMessageBag()->first()], Response::HTTP_BAD_REQUEST);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teacher $teacher)
    {
        $isDelete = $teacher->delete();
        if ($isDelete) {
            return response()->json(
                ['title' => 'Success', 'text' => 'تم ازاله المستخدم بنجاح', 'icon' => 'success'],
                Response::HTTP_OK
            );
        } else {
            return response()->json(
                ['title' => 'Failed', 'text' => 'حدث خطاء اثناء تنفيذ الاجراء', 'icon' => 'error'],
                Response::HTTP_BAD_REQUEST
            );
        }
    }
}
