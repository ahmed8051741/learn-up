@extends('layout.parent')


@section('Title', 'الملف الشخصي')

@section('CSS')

    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

    <link href="{{ asset('assets/css/Star.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/profile.css') }}" rel="stylesheet">


@endsection

@section('dd')
    <a href="mailto:ahmed8051741@gmail.com" class="visitor"> &nbsp; الدعم الفني عبر البريد الالكتروني
    </a>
@endsection


@section('content')


@section('Top')
    <section id="topbar">
        <div class="container d-flex justify-content-center justify-content-md-between" id="topb">
            <div class="contact-info d-flex align-items-center">
                <a href="mailto:ahmed8051741@gmail.com" class="visitor"> &nbsp; الدعم الفني عبر البريد الالكتروني </a>
            </div>
            <div class="d-flex flex-row align-items-center feed-text px-2" dir="ltr">
                &nbsp;&nbsp;
                <div class="d-flex flex-column flex-wrap ml-2"><span class="font-weight-bold">
                        <div class="dropdown">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" style="color: #ffffff;"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="rounded-circle" src="assets/img/my.jpeg" width="42"> &nbsp;&nbsp;
                                {{ auth()->user()->name }}

                                &nbsp;
                            </a>

                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="{{ route('showprofile') }}">الملف الشخصي</a>
                                <hr style="margin-top:0.8px;margin-bottom:0.8px">
                                <a class="dropdown-item" href="{{ route('services.create') }}">اضافة خدمة جديد</a>
                                <a class="dropdown-item" href="{{ route('control') }}">لوحة القيادة</a>
                                <a class="dropdown-item" href="{{ route('logout_log') }}">تسجيل الخروج</a>
                            </div>
                        </div>
                    </span>

                </div>
            </div>
        </div>
    </section>
@endsection

<div class="container bootstrap snippets bootdey">
    <div class="row">
        <div class="profile-nav col-md-3">
            <div class="panel">
                <div class="user-heading round">
                    <a href="#">
                        <img src="{{ asset('storage/image_profile/' . $student->image) }}" alt="">
                    </a>
                    <h1> {{ $student->name }}</h1>
                    <p> التخصص :

                        @if ($student->major == null)
                            لم يتم اختيار التخصص بعد
                        @else
                            {{ $student->major }}
                        @endif

                    </p>
                    <p> هاتف :
                        @if ($student->mobile == null)
                            لا يوجد رقم هاتف

                        @else
                            {{ $student->mobile }}

                        @endif

                </div>

                <ul class="nav">
                    <li class="active"><a href=""><i class="fa fa-user"> &nbsp;</i>
                            الملف
                            الشخصي</a></li>
                    @if ($student->email != null)
                        <li><a href="mailto:{{ $student->email }}" target="_blank"><i class="far fa-envelope">
                                    &nbsp;</i>
                                تواصل من
                                خلال البريد الالكتروني </a></li>
                    @endif
                    @if ($student->facebook != null)

                        <li><a href="{{ $student->facebook }}" target="_blank"><i class="fab fa-facebook-square">
                                    &nbsp;</i> تواصل من
                                خلال فيسبوك
                                بوك</a></li>
                    @endif
                    @if ($student->mobile != null)

                        <li><a href="https://wa.me/{{ $student->s_mobile }}{{ $student->mobile }}"
                                target="_blank"><i class="fab fa-whatsapp" style="  font-size: 14px"> &nbsp;</i>
                                تواصل من خلال واتس
                                اب </a>
                        </li>
                    @endif
                    @if ($student->instagram != null)

                        <li><a href="https://wa.me/{{ $student->instagram }}" target="_blank"><i
                                    class="fab fa-instagram" style="  font-size: 14px"> &nbsp;</i>
                                تواصل من خلال انستجرام </a>

                        </li>
                    @endif
                    @if ($student->youtube != null)

                        <li><a href="{{ $student->youtube }}" target="_blank"><i class="fab fa-youtube">
                                    &nbsp;</i>الذهاب
                                الى قناه
                                اليوتيوب
                            </a></li>
                    @endif




                </ul>
            </div>
        </div>
        <div class="profile-info col-md-9">
            <div class="panel">
                <div class="bg-info clearfix">
                    <button type="button" class="btn btn-secondary float-right"> الملف الشخصي </button>
                </div>
            </div>
            <div class="panel">

                <div class="panel-body bio-graph-info">
                    <div style="margin: 5px; margin-right: 16px;margin-top:20px; ">
                        <h4> نبذة تعريفية عن {{ $student->name }} </h4>
                        <div align="center">
                            <hr width="96%">
                        </div>


                        <div class="row" style="margin: 5px;margin-top: 25px">
                            <div align="center">
                                <p style="text-align: right ; width: 80%"><span>
                                        @if ($student->describe == null)
                                            لم يتم اضافة اي وصف
                                        @else
                                            {{ $student->describe }}
                                        @endif
                                    </span>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>

                <div>

                    <br>
                    <br>
                    <br>

                </div>
                <br>
            </div>
        </div>
    </div>

    <br>
    <br>
    <br>

</div>



@endsection


@section('JS')
<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-46156385-1', 'cssscript.com');
    ga('send', 'pageview');
</script>
@endsection
