@extends('admins.parent')

@section('title', 'انشاء مشرف')
@section('bige-title', 'انشاء مشرف')
@section('main-page', 'الرئيسية')
@section('sub-page', 'المشرفين')
@section('himen', 'menu-open')

@section('content')
<!-- Main content -->
<section class="content">
<div class="container-fluid">
<div class="row" dir="rtl">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title" style="float: right"> انشاء مشرف </h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form id="create-form" dir="rtl">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="name">الأسم</label>
                <input type="text" class="form-control" id="name" placeholder="ادخل اسم المشرف الجديد">
            </div>
            <div class="form-group">
                <label for="name">البريد الالكتروني</label>
                <input type="text" class="form-control" id="email" placeholder="ادخل البريد الالكتروني">
            </div>



            <div class="form-group">
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="active">
                    <label class="custom-control-label" for="active">تفعيل الحساب</label>
                </div>
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="button" onclick="store()" class="btn btn-primary" id="button">ادخال
                وحفظ</button>
        </div>
    </form>
</div>


</div>

</div>
<!-- /.row -->
</div><!-- /.container-fluid -->
</section>

@endsection

@section('JS')
<script>
    function store() {
        axios.post('/admin/admins/', {
                name: document.getElementById('name').value,
                email: document.getElementById('email').value,
                active: document.getElementById('active').checked

            })
            .then(function(response) {
                // handle success
                console.log(response);
                toastr.success(response.data.message);
                // document.getElementById('create-form').reset(); // تصفير قيمة الفورم سبمت
                setTimeout(function() {
                    window.location.href = "/admin/admins/"; // التحويل والانتقال
                }, 1500);
            })
            .catch(function(error) {
                // handle error
                console.log(error);
                toastr.error(error.response.data.message);
            })
            .then(function() {
                // always executed
            });
    }
</script>
@endsection
