@extends('layout.parent')


@section('Title','الخدمات')


@section('CSS')

 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/boxicons/css/boxicons.min.css') }}" rel="stylesheet">

<style>
	body {
    background-color: #eee
}
	.custom-select {
    display: inline-block;
    width: 100%;
    height: calc(2.4375rem + 2px);
    padding: .375rem 1.75rem .375rem .75rem;
    line-height: 1.5;
    color: #495057;
    vertical-align: middle;
background: #fff url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 4 5'%3E%3Cpath fill='%23343a40' d='M2 0L0 2h4zm0 5L0 3h4z'/%3E%3C/svg%3E") no-repeat left.75rem center;
		background-size: 8px 10px;
    border: 1px solid rgba(0,0,0,.26);
    border-radius: .125rem;
    appearance: none;
}
.btn-success {
    color: #fff;
    background-color: #198754;
    border-color: #198754;
}
.btn-success:hover {
    color: #fff;
    background-color: #157347;
    border-color: #146c43;
}
.time {
    font-size: 9px !important
}

.socials i {
    margin-right: 14px;
    font-size: 17px;
    color: #d2c8c8;
    cursor: pointer
}

.feed-image img {
    width: 100%;
    height: auto
}

.bg-gradient-directional-primary {

    background-image: linear-gradient(
45deg
,#008385,#00E7EB);

    background-repeat: no-repeat;
	color: white
}

.bg-gradient-directional-danger {
    background-image: linear-gradient(
45deg
,#FF425C,#FFA8B4);
    background-repeat: no-repeat;
	color: white
}

	.bg-gradient-directional-success {

    background-image: linear-gradient(
45deg
,#11A578,#32EAB2);
    background-repeat: no-repeat;
		color: white
}
 .bg-gradient-directional-warning {

    background-image: linear-gradient(
45deg
,#FF864A,#FFCAB0);
    background-repeat: no-repeat;
	 color: white

}

	.list-group-item.active {
    z-index: 2;
    color: #fff;
    background-color: #009688;
    border-color: #009688;
}
	.list-group-item:first-child {
    border-top-left-radius: 0;
    border-top-right-radius: 0;
}
	.list-group-item {
    position: relative;
    display: block;
 }
	.list-group-item {
    position: relative;
    display: block;
    padding: .75rem 1.25rem;
    margin-bottom: 0;
    background-color: inherit;
    border: 0 solid rgba(0,0,0,.125);
}

.list-group-item-action {
    /* width: 100%; */
    /* color: #495057; */
    text-align: inherit;
}
a {
    color: #106eea;
    text-decoration: none;
}
	.card-body {
    flex: 1 1 auto;
    padding: 1.25rem;
}

	div {
    display: block;
}
	.card {
    border: 0;
    box-shadow: 0 2px 2px 0 rgb(0 0 0 / 14%), 0 3px 1px -2px rgb(0 0 0 / 20%), 0 1px 5px 0 rgb(0 0 0 / 12%);
		    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;

		border-radius: .125rem;
}
	.cardForm {
		    font-size: .875rem;
    font-weight: 400;
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    /* word-wrap: break-word; */
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.12);
    border-radius: .125rem;
}

	.row {
    display: flex;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}
@media (min-width: 768px)
.col-md-12 {
    flex: 0 0 100%;
    max-width: 100%;
}
	h1, h2, h3, h4, h5, h6 {
    font-family: "Al-Jazeera-Arabic";
}

 .h4, h4 {
    font-size: 1.5rem;
}

	.form-group {
		margin-bottom: 5px;
		margin-top: 5px
	}

	.h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
    margin-bottom: .5rem;
    font-family: inherit;
    font-weight: 400;
    line-height: 1.2;
    color: inherit;
}
 h1, h2, h3, h4, h5, h6 {
    margin-top: 0;
    margin-bottom: .5rem;
}

}
 	.form-group {
    margin-bottom: 1rem;
	h4 {
    display: block;
    margin-block-start: 1.33em;
    margin-block-end: 1.33em;
    margin-inline-start: 0px;
    margin-inline-end: 0px;

}

	hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid rgba(0,0,0,.1);
		box-sizing: content-box;
    height: 0;
    overflow: visible;

}


	</style>

@endsection



@section('content')

<div class="container mt-4 mb-5">
    <div class="d-flex justify-content-center row">

		<div class="row">

	<div class="col-xl-3 col-sm-6 col-12">
      <div class="card bg-gradient-directional-primary">
        <div class="card-content">
          <div class="card-body">
            <div class="media d-flex">
              <div class="media-body white text-left" style=" width: 60%" >
                <h3>423</h3>
                <span> الخدمات المضافة </span>
              </div>
              <div class="align-self-center col-xl-3 col-sm-6 col-12 " style="padding-right:10%;width: 40%" >
				<i class="bx bx-credit-card " style="font-size:50px;color:white; "></i>
				</div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="col-xl-3 col-sm-6 col-12">
      <div class="card bg-gradient-directional-warning">
        <div class="card-content">
          <div class="card-body">
            <div class="media d-flex">
                 <div class="media-body white text-left" style=" width: 60%" >
                <h3>423</h3>
                <span> الطلبات الواردة </span>
              </div>
              <div class="align-self-center col-xl-3 col-sm-6 col-12 " style="padding-right:10%;width: 40%" >
				<i class="bx bxs-category" style="font-size:50px;color:white; "></i>
 				</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-sm-6 col-12">
      <div class="card bg-gradient-directional-success">
        <div class="card-content">
          <div class="card-body">
            <div class="media d-flex">
              <div class="media-body white text-left" style=" width: 60%" >
                <h3>423</h3>
                <span> طلبات مقبوله  </span>
              </div>
              <div class="align-self-center col-xl-3 col-sm-6 col-12 " style="padding-right:10%;width: 40%" >
				<i class="bx bx-message-square-check" style="font-size:50px;color:white; "></i>
				</div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-3 col-sm-6 col-12">
      <div class="card bg-gradient-directional-danger">
        <div class="card-content">
          <div class="card-body">
            <div class="media d-flex">
              <div class="media-body white text-left" style=" width: 60%" >
                <h3>423</h3>
                <span> طلبات مرفوضة </span>
              </div>
              <div class="align-self-center col-xl-3 col-sm-6 col-12 " style="padding-right:10%;width: 40%" >
				<i class="bx bx-message-square-x" style="font-size:50px;color:white; "></i>
				</div>
            </div>
          </div>
        </div>
      </div>
    </div>

			<div align="center">
						<hr style="color:royalblue;margin:4px; width: 80%; height: 3px;" >

			</div>
     					<div class="col-xl-3 col-sm-6 col-12" style="margin-top:4px;margin-bottom: 4px">

						<button type="button" class="btn btn-primary" style="width: 100%">اضافة خدمة</button>
						</div>

<div class="col-xl-3 col-sm-6 col-12" style="margin-top:4px;margin-bottom: 4px">
						<button type="button" class="btn btn-secondary"style="width: 100%">عرض جميع الطلبات</button>

						</div>
 <div class="col-xl-3 col-sm-6 col-12" style="margin-top:4px;margin-bottom: 4px">
						<button type="button" class="btn btn-success" style="width: 100%">عرض الطلبات المقبولة</button>
						</div>
<div class="col-xl-3 col-sm-6 col-12" style="margin-top:4px;margin-bottom: 4px">
						<button type="button" class="btn btn-danger" style="width: 100%"  >عرض الطلبات المرفوضة</button>
						</div>
   </div>
	</div>
</div>








<div class="container">
	<div class="row">
		<div class="col-md-3 ">
		     <div class="list-group ">
                <a href="{{route('profile_settings')}}" class="list-group-item list-group-item-action active"> اعدادات الملف الشخصي</a>
                <a href="{{route('password_edit')}}" class="list-group-item list-group-item-action"> تغير كلمه المرور </a>
                <a href="#" class="list-group-item list-group-item-action"> ادارة الخدمات </a>
            </div>
		</div>
		<div class="col-md-9">
		    <div class="card cardForm">
		        <div class="card-body">
		            <div class="row">
		                <div class="col-md-12">
		                    <h4> البينات الخاصة بك </h4>
		                    <hr>
		                </div>
		            </div>
		            <div class="row">
		                <div class="col-md-12">
		                    <form>
                              <div class="form-group row">
                                <label for="name" class="col-4 col-form-label"> الاسم </label>
                                <div class="col-8">
                                  <input id="name"  placeholder="اسمك ثلاثي" class="form-control here" required="required" type="text" value="{{auth()->user()->name}}">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="email" class="col-4 col-form-label" >البريد الالكتروني</label>
                                <div class="col-8">
                                  <input id="email" placeholder="بريدك الالكتروني" class="form-control here" type="text" value="{{auth()->user()->email}}">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="mobile" class="col-4 col-form-label">رقم الهاتف </label>
                                <div class="col-8">
                                  <input id="mobile" placeholder="رقم الهاتف" class="form-control here" type="text" value="{{auth()->user()->mobile}}">
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="select" class="col-4 col-form-label"> التخصص (مجال عملك - اذا كنت طالب فادخل هوايتك) </label>
                                <div class="col-8">
                                  <select id="select" name="select" class="custom-select">
                               <option value="{{auth()->user()->major}}"> {{auth()->user()->major}} </option>
                                @foreach ($categorise as $categoris)
                                <option value=" {{$categoris->name}} "> {{$categoris->name}} </option>
                                @endforeach

                            </select>
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="facebook" class="col-4 col-form-label"> رابط  حساب الفيس بوك الخاص بك </label>
                                <div class="col-8">
                                  <input id="facebook" placeholder="ادخل اشارة السلم ان لم تكن تملك " class="form-control here"  type="text"  value="{{auth()->user()->facebook}}">
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="instagram" class="col-4 col-form-label">  رابط حساب الانستجرام الخاص بك </label>
                                <div class="col-8">
                                  <input id="instagram"  placeholder="ادخل اشارة السلم ان لم تكن تملك "class="form-control here" type="text"  value="{{auth()->user()->instagram}}">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="youtube" class="col-4 col-form-label">  رابط قناة اليوتيوب الخاص بك </label>
                                <div class="col-8">
                                  <input id="youtube"  placeholder="ادخل اشارة السلم ان لم تكن تملك " class="form-control here" type="text" value="{{auth()->user()->youtube}}">
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="describe" class="col-4 col-form-label"> نبذة عنك </label>
                                <div class="col-8">
                                  <textarea id="describe"  cols="40" rows="4" class="form-control" placeholder=" ستظهر في ملفك الشخصي " >{{auth()->user()->describe}}</textarea>
                                </div>
                              </div>
                              <div class="card-footer">
                                <button  type="button" onclick="update({{auth()->user()->id}})" class="btn btn-primary" id="button" style="float: left"> تحديث البينات  </button>
                            </div>
                            </form>
		                </div>
		            </div>

		        </div>
		    </div>
		</div>
	</div>
</div>
	<br>
	<br>
	<br>

@endsection



@section('JS')

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

  <script>
    function update(id){
        axios.put('/profile/profile_setting/'+id,{
            name: document.getElementById('name').value,
            email: document.getElementById('email').value,
            mobile: document.getElementById('mobile').value,
            select: document.getElementById('select').value,
            facebook: document.getElementById('facebook').value,
            instagram: document.getElementById('instagram').value,
            youtube: document.getElementById('youtube').value,
            describe: document.getElementById('describe').value,
         })
        .then(function (response) {
            // handle success
            console.log(response);
            toastr.success(response.data.message);
            window.location.href="/admin/categories";
        })
        .catch(function (error) {
            // handle error
            console.log(error);
            toastr.error(error.response.data.message);
        })
        .then(function () {
            // always executed
        });
    }
</script>



@endsection
