@extends('layout.parent')


@section('Title', 'جميع الطلبات الواردة')

@section('CSS')

    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="{{ asset('assets/css/Star.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/profile.css') }}" rel="stylesheet">

    <style>
        element.style {}

        .alert .close,
        .alert .mailbox-attachment-close {
            color: #000;
            opacity: .2;
        }

        .alert-dismissible .close,
        .alert-dismissible .mailbox-attachment-close {
            position: absolute;
            top: 0;
            right: 0;
            padding: .75rem 1.25rem;
            color: inherit;
        }

        [type=button]:not(:disabled),
        [type=reset]:not(:disabled),
        [type=submit]:not(:disabled),
        button:not(:disabled) {
            cursor: pointer;
        }

        [type=button]:not(:disabled),
        [type=reset]:not(:disabled),
        [type=submit]:not(:disabled),
        button:not(:disabled) {
            cursor: pointer;
        }

        button.close,
        button.mailbox-attachment-close {
            padding: 0;
            background-color: transparent;
            border: 0;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }

        .close,
        .mailbox-attachment-close {
            float: right;
            font-size: 1.5rem;
            font-weight: 700;
            line-height: 1;
            color: #000;
            text-shadow: 0 1px 0 #fff;
            opacity: .5;
        }

        .badge-success {
            color: #fff;
            background-color: #28a745;
        }

        .badge-info {
            color: #fff;
            background-color: #1c3c94;
        }

        .badge {
            display: inline-block;
            padding: .25em .4em;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25rem;
        }

        .badge-warning {
            color: #fff;
            background-color: #ffc400de;
        }

        .badge-danger {
            color: #fff;
            background-color: #ff0000de;
        }

        .badge-info {
            color: #fff;
            background-color: #1c3c94;
        }

        .visitor {
            color: white;
            font-size: 15px;

        }

        * {
            font-family: 'cairo';
        }

        .visitor {
            color: white;
            font-size: 15px;


        }

        .row {
            display: flex;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }

        @media (min-width: 768px) {
            .col-md-12 {
                flex: 0 0 100%;
                max-width: 100%;
            }

            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                font-family: "Al-Jazeera-Arabic";
            }

            .h4,
            h4 {
                font-size: 1.5rem;
            }

            .form-group {
                margin-bottom: 5px;
                margin-top: 5px
            }

            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                margin-top: 0;
                margin-bottom: .5rem;
            }

        }

        .card {
            margin-top: 12px
        }

        .form-group {
            margin-bottom: 1rem;
        }

        h4 {
            display: block;
            margin-block-start: 1.33em;
            margin-block-end: 1.33em;
            margin-inline-start: 0px;
            margin-inline-end: 0px;

        }


        /* .btn-group>.btn-group:not(:last-child)>.btn, .btn-group>.btn:not(:last-child):not(.dropdown-toggle) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    border-top-right-radius: 4px;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    border-bottom-right-radius: 4px;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                }

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                .btn-group>.btn-group:not(:first-child)>.btn, .btn-group>.btn:nth-child(n+3), .btn-group>:not(.btn-check)+.btn {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         border-top-left-radius:4px;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         border-bottom-left-radius:4px;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                } */
        .dropdown-menu {
            text-align: right;
            right: 0px;
            min-width: 11rem;
        }

        .pro1 {
            border-top-right-radius: 0px;
            border-bottom-right-radius: 0px;
            border-top-left-radius: 0px;
            border-bottom-left-radius: 0px;
        }

        .pro {
            border-top-right-radius: 0px;
            border-bottom-right-radius: 0px;


        }

        h4 {
            margin-block-start: 0.33em;
            margin-block-end: 0.33em;
            margin-inline-start: 0px;
            margin-inline-end: 0px;
        }

        .table>:not(caption)>*>* {
            padding: .5rem .5rem;
            padding-top: 20px;
        }

        .badge-success {
            color: #fff;
            background-color: #28a745;
        }

        .badge-info {
            color: #fff;
            background-color: #1c3c94;
        }

        .badge {
            display: inline-block;
            padding: .25em .4em;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25rem;
        }

        .btn-success {
            color: #fff;
            background-color: #28a745;
            border: #28a745;
        }

        .btn-success:hover {
            color: #fff;
            background-color: #1f8b38;
            border: #28a745;
        }

        .badge-warning {
            color: #fff;
            background-color: #ffc400de;
        }

        .badge-danger {
            color: #fff;
            background-color: #ff0000de;
        }

        .badge-info {
            color: #fff;
            background-color: #1c3c94;
        }
    #header {
        visibility: hidden;
        margin-top: -50px;
    }
    </style>
@endsection


@section('dd')
<h2 class="logo" style="font-family: Al-Jazeera-Arabic"><a href="{{ route('showHome') }}"
        style="font-family: Al-Jazeera-Arabic; color: aliceblue"> Learn <span
            style="font-family: Al-Jazeera-Arabic;font-weight: 760;color: #Fd4;"> up
        </span></a></h2>
@endsection
@section('con', 'display: none')

@section('content')


@section('Top')

    <section id="topbar">
        <div class="container d-flex justify-content-center justify-content-md-between" id="topb">
            <div class="contact-info d-flex align-items-center">
                <a href="mailto:ahmed8051741@gmail.com" class="visitor"> &nbsp; الدعم الفني عبر البريد الالكتروني </a>
            </div>
            <div class="d-flex flex-row align-items-center feed-text px-2" dir="ltr">
                &nbsp;&nbsp;
                <div class="d-flex flex-column flex-wrap ml-2"><span class="font-weight-bold">
                        <div class="dropdown">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" style="color: #ffffff;"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="rounded-circle" src="{{ asset('assets/img/my.jpeg') }}" width="42">
                                &nbsp;&nbsp;
                                {{ auth('teacher')->user()->name }} &nbsp;</a>

                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="{{ route('showprofile') }}">الملف الشخصي</a>
                                <hr style="margin-top:0.8px;margin-bottom:0.8px">
                                <a class="dropdown-item" href="{{ route('services.create') }}">اضافة خدمة جديد</a>
                                <a class="dropdown-item" href="{{ route('control') }}">لوحة القيادة</a>
                                <a class="dropdown-item" href="{{ route('logout_log') }}">تسجيل الخروج</a>
                            </div>
                        </div>
                    </span>

                </div>
            </div>
        </div>
    </section>
@endsection

<div class="container bootstrap snippets bootdey">
    <div class="row">
        <div class="profile-nav col-md-3">
            <div class="panel">
                <div class="user-heading round">
                    <a href="#">
                        <img src="{{ asset('storage/image_profile/' . auth()->user()->image) }}" alt="">
                    </a>
                    <h1> {{ auth()->user()->name }} </h1>
                    <p>{{ auth()->user()->email }} </p>
                </div>

                <ul class="nav">
                    <li><a href="{{ route('control') }}"><i class="fas fa-tachometer-alt">
                                &nbsp;</i>
                            دفقة القيادة </a></li>
                    <li><a href="{{ route('showprofile') }}"><i class="fa fa-user"> &nbsp;</i> الملف الشخصي </a>
                    </li>

                    @if (auth()->user()->id == auth('teacher')->id())
                        <li><a href="{{ route('services.index') }}"><i class="fab fa-servicestack"> &nbsp;</i>
                                الخدمات
                            </a></li>

                        <li class="active"><a href="{{ route('rqserve_all') }}"><i class="fas fa-retweet"> &nbsp;</i>
                                الطلبات الواردة
                            </a></li>
                    @else
                        <li><a href="{{ route('order.index') }}"><i class="fab fa-servicestack"> &nbsp;</i>
                                الطلبات
                            </a></li>


                        <li><a href="{{ route('rqserve_allstd') }}"><i class="fas fa-retweet"> &nbsp;</i>
                                العروض المقدمه
                            </a></li>
                    @endif
                    <li><a href="{{ route('password_edit') }}"><i class="fas fa-lock"> &nbsp;</i> تعديل كلمه
                            المرور</a></li>



                    <li><a href="{{ route('logout_log') }}"><i class="fas fa-sign-out-alt"> &nbsp;</i> تسجيل
                            الخروج</a></li>
                </ul>
            </div>
        </div>


        <div class="profile-info col-md-9" style="margin-bottom: 60px">
            <div class="panel">
                <div class="bg-info clearfix">
                    <button type="button" class="btn btn-secondary float-right"> الخدمات واجراءاتها </button>
                </div>
            </div>






            <div class="card">



                <div class="card-header" style="padding-top: 13px;padding-bottom: 12px;padding-right:18px;width: 100%;">
                    <div class="col-4" style="float: left; "> <a href="{{ route('services.create') }}">
                            <button type="button" class="btn btn-outline-primary" style="width: 100%"> <i
                                    class="far fa-plus-square">&nbsp;</i> اضف خدمة جديدة </button>
                        </a>
                    </div>

                    <div class="col-8">
                        <h4>
                            جميع الطلبات الواردة لديك

                        </h4>
                    </div>


                </div>

                <!-- /.card-header -->
                <div class="card-body" dir="rtl">
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"
                                style="background-color: none">×</button>
                            <h5><i class="icon fas fa-ban"></i>خطاء !</h5>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </div>
                    @endif

                    @if (session()->has('message'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-check"></i> ! تنبية </h5>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                </div>
                <table id="example1" class="table table-bordered table-striped main-content" style="text-align: center">

                    <thead dir="rtl" class="thead_dark">
                        <tr>
                            <th>#</th>
                            <th> اسم المتقدم للخدمه </th>
                            <th> اليوم المناسب </th>
                            <th> الساعة التقريبية المناسبة </th>
                            <th> وقت الطلب </th>
                            <th> الخدمه </th>
                            <th> رقم الهاتف </th>
                            <th> الحالة </th>
                            <th> اجراءات اخرى </th>

                        </tr>
                    </thead>
                    <tbody dir="rtl">

                        <?php $i = 0; ?>
                        @foreach ($Reserves as $Rqserv)

                            @if ($Rqserv->subReserve->id == auth('teacher')->user()->id)
                                <tr>
                                    <td>
                                        <?php $i = $i + 1; ?>

                                        {{ $i }}

                                    </td>

                                    <td>
                                        @if ($Rqserv->account_type == 'teacher')
                                            {{ $Rqserv->subReserve->name }}
                                        @elseif ($Rqserv->account_type == 'student')
                                            {{ $Rqserv->Stud->name }}
                                        @endif


                                    </td>

                                    <td> {{ $Rqserv->date }}</td>
                                    <td>{{ $Rqserv->time }}</td>

                                    <td>{{ $Rqserv->created_at->diffForHumans() }}</td>
                                    <td>{{ $Rqserv->Service->title }}</td>

                                    <td>
                                        @if ($Rqserv->account_type == 'teacher')
                                            {{ $Rqserv->subReserve->mobile }}
                                            @if ($Rqserv->subReserve->mobile == null)
                                                لا يوجد رقم هاتف
                                            @endif

                                        @elseif ($Rqserv->account_type == 'student')
                                            {{ $Rqserv->Stud->mobile }}
                                            @if ($Rqserv->Stud->mobile == null)
                                                لا يوجد رقم هاتف
                                            @endif

                                        @endif

                                    </td>
                                    <td>
                                        @if ($Rqserv->Status == 0)

                                            <span class="badge badge-info" style="font-size: 13px"> لم يتم اتخاذ اي
                                                اجراء </span>
                                        @elseif ($Rqserv->Status ==1)
                                            <span class="badge badge-success" style="font-size: 13px"> تم القبول
                                            </span>
                                        @elseif ($Rqserv->Status ==2)
                                            <span class="badge badge-warning" style="font-size: 13px"> تم التعليق
                                            </span>
                                        @elseif ($Rqserv->Status ==3)
                                            <span class="badge badge-danger" style="font-size: 13px"> تم الرفض
                                            </span>

                                        @endif

                                    </td>

                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('show.Accept', $Rqserv->id) }} "
                                                class="btn btn-primary pro1" style="border-radius: 5px">
                                                <i class="fas fa-edit" style="font-size: 22px; "></i>
                                            </a>

                                            @if ($Rqserv->Status == 3)
                                                &nbsp;
                                                <form action="{{ url('/serves/destroy/' . $Rqserv->id) }}"
                                                    method="post">
                                                    @csrf
                                                    @method('Delete')
                                                    <button onclick='return myFunction()' type="submit" class="btn btn-danger"><i
                                                            class="far fa-trash-alt"
                                                            style="font-size: 22px; "></i></button>
                                                </form>
                                            @endif
                                        </div>

                                    </td>
                                </tr>
                            @endif

                        @endforeach
                    </tbody>
                    <hr>
                </table>

            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
</div>

<!-- /.container-fluid -->
</section>


@endsection


@section('JS')



<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-46156385-1', 'cssscript.com');
    ga('send', 'pageview');
</script>



<!-- jQuery -->
<script src="{{ asset('CMS/plugins/jquery/jquery.min.js') }}"></script>


<script src="{{ asset('CMS/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('CMS/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('CMS/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>

<script src="{{ asset('CMS/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('CMS/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('CMS/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('CMS/plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('CMS/plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('CMS/plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('CMS/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('CMS/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('CMS/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('dist/js/demo.js') }}"></script>
<!-- Page specific script -->
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": true,
            "autoWidth": false,
            "buttons": ["copy", "excel", "pdf", "print"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });
    });
</script>





<script>
    //عملية الحذف
    function confirmDestroy(id, referince) {
        //    console.log("ID:"+id)    // ->?   (id) تستعمل لفحص الكي اذا تم تمريرة او لا
        Swal.fire({
            title: 'هل انت متأكد من عملية الحذف؟',
            text: "لن تتمكن من التراجع عن هذا!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'الغاء',
            confirmButtonText: 'نعم ، احذفها!',
        }).then((result) => {
            if (result.isConfirmed) {
                destroy(id, referince); // referince لاستقبال الذس من الرابط
            }
        })
    }

    function destroy(id, referince) {
        // Make a request for a user with a given ID
        axios.delete('/serves/destroy/' + id)
            .then(function(response) {
                // handle success
                console.log(response);
                referince.closest('tr')
                    .remove(); //referince في فوق في الرابط او الاشرف وليس شرك ان يسمى بي  this الذي يتبع الى كلمه  referince لحذف الصف دون الحاجة الى تحديث الصفحة بلاستناد على المتغير
                ShowMessage(response.data); // لاستقبال الجايسون من الكنترولر ببيناته رسالة النجاح والايقونة
            })
            .catch(function(error) {
                // handle error
                console.log(error); // اسقبال رسائل وبينات الخطاء
                ShowMessage(error.response.data);
            })
            .then(function() {
                // دائماً منفذة لو بدي انفذ داله معينة في كل الحالات
            });

    }

    function ShowMessage(data) {
        Swal.fire({
            icon: data.icon, // طباعة الايقونة والاعنوان بناء على البينات المستقبلة من الكنترولر
            title: data.title,
            text: data.text,
            showConfirmButton: false,
            timer: 1500
        })
    }
</script>

<script src="{{ asset('CMS/plugins/toastr/toastr.min.js') }}"></script>

<script>
    function myFunction() {
        return confirm("هل انت متأكد من عمليه الحذف هذه ..!");
    }
</script>
@endsection
