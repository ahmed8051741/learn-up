<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;


class AuthController extends Controller
{
    //

    public function showLogin(Request $request, $guard)
    {
        return response()->view('admins.login', ['guard' => $guard]);
    }




    public function login(Request $request)
    {
        $validator = Validator($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:3|max:30',
            'remember' => 'required|boolean',
            'guard' => 'required|string|in:admin'
        ], [
            'guard.in' => 'الرجاء التأكد من عنوان البريد الالكتروني',
            'email.required' => 'ادخل البريد الالكتروني',
            'email.email' => 'ادخل البريد الالكتروني بشكل صحيح',
            'password.required' => 'ادخل كلمه المرور ',
            'password.string' => 'ادخل كلمه المرور بشكل صحيح ',
            'password.min' => 'ادخل كلمه المرور بشكل صحيح ',
            'password.max' => 'ادخل كلمه المرور بشكل صحيح ',
            'remember.required' => 'هل تود ان يتذكرك المتصفح',
        ]);

        if (!$validator->fails()) {
            $credentials = [
                'email' => $request->get('email'),
                'password' => $request->get('password'),
            ];
            $email = $request->get('email');
            $password = $request->get('password');
            if (Auth::guard($request->get('guard'))->attempt(['email' => $email, 'password' => $password, 'active' => 1], $request->get('remember'))) {
                return response()->json([
                    'message' => 'تم تسجيل الدخول بنجاح'
                ], Response::HTTP_OK);
            } elseif (Auth::guard($request->get('guard'))->attempt(['email' => $email, 'password' => $password, 'active' => 0])) {
                Auth::guard('admin')->logout();
                return response()->json([
                    'message' => 'لقد تم ايقاف حسابك من قبل مدير النظام'
                ], Response::HTTP_BAD_REQUEST);
            } else {
                return response()->json([
                    'message' => 'فشل تسجيل الدخول , معلوماتك خاطئة '
                ], Response::HTTP_BAD_REQUEST);
            }
        } else {
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }



    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        $request->session()->invalidate();
        return redirect()->route('login', 'pro/admin');
    }

    public function editPassword()
    {
        return response()->view('admins.auth.change-password');
    }
    public function updatePassword(Request $request)
    {
        $guard = auth('admin')->check() ? 'admin' : 'user';
        $validator = validator($request->all(), [
            'current_password' => "required|string|password:admin",
            'new_password' => 'required|string|min:3|max:16|confirmed',
        ]);

        if (!$validator->fails()) {
            $user = auth($guard)->user();
            $user->password = Hash::make($request->get('new_password'));
            $isSaved = $user->save();
            return response()->json(['message' => $isSaved ? 'Password changed Successfully ' : 'Failde to changed Password '], $isSaved ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }


    public function edit_profile()
    {
        $view = auth('admin')->check() ? 'admins.admin.edit' : 'admins.users.edit';
        $guard = auth('admin')->check() ? 'admin' : 'user';
        return response()->view($view, [$guard => auth($guard)->user(), 'redirect' => false]);
    }

    public function edit_profile_image()
    {
        $view = auth('admin')->check() ? 'admins.admin.edit_image' : '';
        $guard = auth('admin')->check() ? 'admin' : 'user';
        return response()->view($view, [$guard => auth($guard)->user(), 'redirect' => false]);
    }

    public function updat_profile_image(Request $request)
    {
        $guard = auth('admin')->check() ? 'admin' : 'user';
        $validator = Validator(
            $request->all(),
            [
                'image' => 'required|image',
            ],
            [
                'profle_image.required' => 'اضف صورة من فضلك',
                'profle_image.image' => 'يسمح فقط بالصور',
            ]
        );

        if (!$validator->fails()) {
            $image = $request->file('image');
            $imageName = time() . '_profle_image.' . $image->getClientOriginalExtension();
            $image->storeAs('image_profile', $imageName, ['disk' => 'public']);
            $user = auth($guard)->user();
            $user->image = $imageName;
            $isSaved = $user->save();
            return response()->json([
                'message' => $isSaved ? "تم تحديث صورتك بنجاح" : "حدث خطاء غير معروف .. الرجاء التواصل مع الدعم الفني"
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            //VALIDATION FAILED
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function updat_profile(Request $request)
    {
        $guard = auth('admin')->check() ? 'admin' : 'user';
        $validator = validator($request->all(), [
            'name' => 'required|string|min:3|max:30',
            'email' => 'required|email',
            'active' => 'required|boolean',
            // 'images' => 'image|max:2048|mimes:png,jpg',
        ]);

        if (!$validator->fails()) {
            $user = auth($guard)->user();
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->active = $request->get('active');
            $isSaved = $user->save();
            return response()->json(['message' => $isSaved ? 'تم تحديث الملف الشخصي بنجاح  ' : 'حدث خطاء غير متوقع الرجاء المحاولة مرة اخرى'], $isSaved ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }



    public function updat_image(Request $request)
    {
        $guard = auth('teacher')->check() ? 'teacher' : 'student';
        $validator = Validator(
            $request->all(),
            [
                'images' => 'required|image',

            ],
            [
                'profle_image.required' => 'اضف صورة من فضلك',
                'profle_image.image' => 'يسمح فقط بالصور',

            ]
        );

        if (!$validator->fails()) {

            $image = $request->file('images');
            $imageName = time() . '_profle_image.' . $image->getClientOriginalExtension();
            $image->storeAs('image_profile', $imageName, ['disk' => 'public']);
            $user = auth($guard)->user();
            $user->image = $imageName;
            $isSaved = $user->save();
            return response()->json([
                'message' => $isSaved ? "تم تحديث صورتك بنجاح" : "حدث خطاء غير معروف .. الرجاء التواصل مع الدعم الفني"
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            //VALIDATION FAILED
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function ahmed_serve($id)
    {
        $serves = Service::find($id)->get();
        return response()->view('admins.teacher.index_Service', ['servess' => $serves]);
    }
}
