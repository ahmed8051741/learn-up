<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\user;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //


    public function Show_Dashboard()
    {
        $TeacherCounts = Teacher::count();
        $StudentiesCount = Student::count();
        $adminsCounts = Admin::count();
        $Teachers = Teacher::orderBy('updated_at', 'DESC')->take(7)->get();
        $stds = Student::orderBy('updated_at', 'DESC')->take(7)->get();

        return response()->view('admins.dashboard', [
            'TeacherCounts' => $TeacherCounts,
            'StudentiesCount' => $StudentiesCount,
            'adminsCounts' => $adminsCounts,
            'Teachers' => $Teachers,
            'stds' => $stds,
        ]);
    }



    public function student()
    {
        $students = Student::all();

        return response()->view('admins.std.index', [
            'students' => $students,
        ]);
    }


    public function teacher()
    {
        $teachers = Teacher::all();
        $search = "";

        return response()->view('admins.teacher.index', [
            'teachers' => $teachers,
            'search' => $search,
        ]);
    }
}
