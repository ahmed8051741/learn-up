<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('price');
            $table->string('describe', 10000);
            $table->string('image')->nullable();
            $table->string('status')->default('1');
            $table->string('select_CAT');

            $table->foreignId('category_id');
            $table->foreign('category_id')->on('categories')->references('id');

            $table->foreignId('teacher_id');
            $table->foreign('teacher_id')->on('teachers')->references('id');

            $table->foreignId('city_id');
            $table->foreign('city_id')->on('cities')->references('id');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
