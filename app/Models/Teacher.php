<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Teacher extends Authenticatable
{
    use HasFactory, SoftDeletes;

    public function subTeacher()
    {
        // return $this->hasMany(Service::class);
        return $this->hasMany(Service::class, 'teacher_id', 'id');
    }

    public function getStatusKeywordAttribute()
    {
        return $this->Active ? "فعال" : "معطل";
    }

    public function images()
    {
        return $this->hasMany(Image::class, 'teacher_id', 'id');
    }

    public function Reserve()
    {
        return $this->hasMany(Reserve::class, 'teacher_id', 'id');
    }

    public function subteacher_sd()
    {
        return $this->hasMany(Reserve::class, 'teacher_sd', 'id');
    }

    public function subTeachers()
    {
        // return $this->hasMany(Service::class);
        return $this->hasMany(Rqorder::class, 'teacher_id', 'id');
    }
}
