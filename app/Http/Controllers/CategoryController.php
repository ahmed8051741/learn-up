<?php

namespace App\Http\Controllers;

use App\Models\category;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categorise = category::all();
        return response()->view('admins.categories.index', ['categorise' => $categorise]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return response()->view('admins.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:3|max:30',
        ]);

        if (!$validator->fails()) {
            //TODO: CREATE NEW CATEGORY
            $category = new Category();
            $category->name = $request->get('name');
            $isSaved = $category->save();
            return response()->json([
                'message' => $isSaved ? "تمت الاضافة بنجاح" : "فشلت الاضافة لسبب ما"
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            //VALIDATION FAILED
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(category $category)
    {
        //
        return response()->view('admins.categories.edit', ['category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:3|max:30',
        ]);

        if (!$validator->fails()) {
            $category->name = $request->get('name');
            $isUpdated = $category->save();
            return response()->json([
                'message' => $isUpdated ? "نجح تحديث التنصيف" : "فشل تحديث التصنيف"
            ], $isUpdated ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(category $category)
    {
        //
        $isDelete = $category->delete();
        if ($isDelete) {
            return response()->json(
                ['title' => 'Success', 'text' => 'تم حذف التصنيف ', 'icon' => 'success'],
                Response::HTTP_OK
            );
        } else {
            return response()->json(
                ['title' => 'Failed', 'text' => 'فشلت عملية الحذف', 'icon' => 'error'],
                Response::HTTP_BAD_REQUEST
            );
        }
    }
}
