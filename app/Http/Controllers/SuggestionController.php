<?php

namespace App\Http\Controllers;

use App\Models\category;
use App\Models\City;
use App\Models\Notice;
use App\Models\Rqserve;
use App\Models\Service;
use App\Models\Star;
use App\Models\Suggestion;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SuggestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $boxing = Suggestion::all();
        return response()->view('admins.boxing', ['boxing' => $boxing]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')->get();
        $categorise = category::all();
        $cities = City::all();
        $Teacher = Teacher::all();
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $stars = Star::all();
        return response()->view(
            'boxing',
            ['services' => $services, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator(
            $request->all(),
            [
                'describe' => 'required|string',
            ],
            [
                'describe.required' => 'لا يمكنك ارسال نموذج فارغ',
                'describe.string' => 'يسمح فقط بالنصوص',
            ]
        );

        if (!$validator->fails()) {
            //TODO: CREATE NEW CATEGORY
            $category = new Suggestion();
            $category->suggestion = $request->get('describe');
            $isSaved = $category->save();
            return response()->json([
                'message' => $isSaved ? "شكراً على مساهمتك في تطورنا ... نسعد بكم" : "فشلت الاضافة لسبب ما"
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            //VALIDATION FAILED
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Suggestion  $suggestion
     * @return \Illuminate\Http\Response
     */
    public function show(Suggestion $suggestion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Suggestion  $suggestion
     * @return \Illuminate\Http\Response
     */
    public function edit(Suggestion $suggestion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Suggestion  $suggestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Suggestion $suggestion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Suggestion  $suggestion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Suggestion $suggestion)
    {
        //
    }
}
