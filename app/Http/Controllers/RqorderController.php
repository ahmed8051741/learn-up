<?php

namespace App\Http\Controllers;

use App\Models\Notice;
use App\Models\Rqserve;
use App\Models\Service;
use App\Models\Rqorder;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RqorderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator(
            $request->all(),
            [

                'teacher_id' => 'required|numeric',
                'order_id' => 'required|numeric',
                'student_id' => 'required|numeric',
                'pay' => 'required|string',
                'time' => 'required|string',
                'date' => 'required|string',
                'account_type' => 'required|string',
                'sename' => 'required|string',
                'rq_name' => 'required|string',
                'order' => 'required|string',
            ],
            [
                'order.required' => 'حدث خلل امني الرجاء المحاولة فيما بعد jg',
                'order.string' => 'حدث خلل امني الرجاء المحاولة فيما بعدjg',

                'rq_name.required' => 'حدث خلل امني الرجاء المحاولة فيما بعدs',
                'rq_name.string' => 'حدث خلل امني الرجاء المحاولة فيما بعدs',

                'sename.required' => 'حدث خلل امني الرجاء المحاولة فيما بعدdd',
                'sename.string' => 'حدث خلل امني الرجاء المحاولة فيما بعدd',
                'student_id.required' => 'حدث خلل امني .. الرجاء المحاولة مرة اخرى',
                'student_id.numeric' => 'حدث خلل امني .. الرجاء المحاولة مرة اخرى',
                'teacher_id.required' => 'حدث خلل امني .. الرجاء المحاولة مرة اخرى',
                'teacher_id.numeric' => 'حدث خلل امني .. الرجاء المحاولة مرة اخرى',
                'order_id.required' => 'حدث خلل امني .. الرجاء المحاولة مرة اخرى',
                'order_id.numeric' => 'حدث خلل امني .. الرجاء المحاولة مرة اخرى',
                'pay.required' => 'اختر طريقة الدفع ',
                'pay.string' => ' اختر طريقة الدفع بشكلاً صحيح',
                'daye.string' => 'يسمح فقط بنصوص في حقل اليوم',

                'time.required' => 'قم بادخال الساعة التي تناسبك ليتواصل معك مقدم الخدمه',
                'time.string' => ' قم بادخال الساعة التي تناسبك ليتواصل معك مقدم الخدمه بشكل صحيح',

                'date.required' => 'قم بادخال التاريخ الذي يناسبك ليتواصل معك مقدم الخدمه',
                'date.string' => ' قم بادخال التاريخ الذي يناسبك ليتواصل معك مقدم الخدمه بشكل صحيح',

                'account_type.required' => 'حدث خلل امني .. الرجاء المحاولة مرة اخرى',
                'account_type.string' => 'حدث خلل امني .. الرجاء المحاولة مرة اخرى',

            ]
        );

        if (!$validator->fails()) {
            $rqorder = new Rqorder();
            $rqorder->teacher_id = $request->get('teacher_id');
            $rqorder->order_id = $request->get('order_id');
            $rqorder->student_id = $request->get('student_id');
            $rqorder->pay = $request->get('pay');
            $rqorder->date = $request->get('date');
            $rqorder->time = $request->get('time');
            $rqorder->account_type = $request->get('account_type');

            $Notice = new Notice();
            $Notice->send = "teacher";
            $Notice->req_send_type = "student";
            $Notice->send_id = $request->get('teacher_id');
            $Notice->send_name = $request->get('sename');
            $Notice->req_send = $request->get('student_id');
            $Notice->req_send_name = $request->get('rq_name');
            $Notice->serves = $request->get('order');
            $Notice->Rqorder_id = $request->get('order_id');

            $isSaved = $Notice->save();
            $isSaved = $rqorder->save();
            return response()->json([
                'message' => $isSaved ? "تم تأكيد طلبك بنجاح .. شكراً على تعاملك معنا" : "حدث خطاء غير معروف .. الرجاء التواصل مع الدعم الفني"
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            //VALIDATION FAILED
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Rqorder  $rqorder
     * @return \Illuminate\Http\Response
     */
    public function show(Rqorder $rqorder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Rqorder  $rqorder
     * @return \Illuminate\Http\Response
     */
    public function edit(Rqorder $rqorder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Rqorder  $rqorder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rqorder $rqorder)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Rqorder  $rqorder
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rqorder $rqorder)
    {
        //
    }
}
