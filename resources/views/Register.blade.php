<!doctype html>
  <!-- =======================================================
* Template Name: Learn up - v->experimental .
* Created by : Ayat Abu Qasim - Ahmed Farhat - Osama Abu Tawahinah .
* Occasion : Our graduation project for the 2021 academic year .
* Type : CSS file .
  ======================================================== -->
 <html dir="rtl">
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/loginCss/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/toastr/toastr.min.css') }}">
<script src="https://kit.fontawesome.com/a81368914c.js"></script>
<link rel="preconnect" href="https://fonts.gstatic.com">
<title> Learn up - انشاء حساب طالب جديد </title>
</head>

<body>
	<img class="wave" src="{{ asset('assets/svg/wave.png') }}" alt="imges">
	<div class="container">
		<div class="img">
			<img src="{{ asset('assets/svg/undraw_personalization_triu.svg') }}" alt="svg">
		</div>
		<div class="login-content">
            <form id="create-form">
                @csrf
                <h2 class="title"> انشاء حساب جديد </h2>
           		<div class="input-div one">
           		   <div class="i">
					   <i class="far fa-id-card"></i>
             		   </div>
           		   <div class="div">
           		   		<h5>الاسم الخاص بك ثلاثي</h5>
           		   		<input type="text" class="input" id="name" name="name">
           		   </div>
           		</div>

			   <div class="input-div one">
           		   <div class="i">
           		   		<i class="fas fa-user"></i>
           		   </div>
           		   <div class="div">
           		   		<h5>البريد الالكتروني</h5>
           		   		<input type="text" id="email" class="input" name="email">
           		   </div>
           		</div>
           		<div class="input-div pass">
           		   <div class="i">
           		    	<i class="fas fa-lock"></i>
           		   </div>
           		   <div class="div">
           		    	<h5>كلمه المرور </h5>
           		    	<input type="password" id="password" class="input"  name="password">
            	   </div>
            	</div>
			     <div class="input-div pass">
           		   <div class="i">
           		    	<i class="fas fa-key"></i>
           		   </div>
           		   <div class="div">
           		    	<h5>اعادة كتابة كلمه المرور </h5>
           		    	<input type="password" id="password_confirmation" class="input">
            	   </div>
            	</div>

                <a href="#" style="font-size: 13px">  عند تسجيلك هذا يعني انك توافق على سياسة الخصوصية خاصتنا  </a>
                {{-- <button type="button" onclick="store()" class="btn btn-primary" id="button">Store</button> --}}

                <button type="button" onclick="store()" class="btn"  id="button"> الموافقة والمتابعة </button>

                 <a onclick="go()" style="text-align: center"> لدي حساب بالفعل - العودلة لتسجيل الدخول </a>
            </form>
        </div>
    </div>
  <!-- =======================================================
* Template Name: Learn up - v->experimental .
* Created by : Ayat Abu Qasim - Ahmed Farhat - Osama Abu Tawahinah .
* Occasion : Our graduation project for the 2021 academic year .
* Type : CSS file .
  ======================================================== -->
  <script type="text/javascript" src="{{ asset('assets/jquery/jquery.min.js') }}"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
  <script type="text/javascript" src="{{ asset('assets/toastr/toastr.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('assets/LoginCss/main.js') }}"></script>


  <script>

    function store(){
        axios.post('/register/student/',{
            name: document.getElementById('name').value,
            email: document.getElementById('email').value,
            password: document.getElementById('password').value,
            password_confirmation: document.getElementById('password_confirmation').value
        })
        .then(function (response) {
            // handle success
            console.log(response);
            toastr.success(response.data.message);
            //  window.location.href="/teacher/login/";
            window.setTimeout(window.location.href = "/student/login/",9000);
        })
        .catch(function (error) {
            // handle error
            console.log(error);
            toastr.error(error.response.data.message);
        })
        .then(function () {
            // always executed
        });
    }
</script>
<script>

    function go(){
       Swal.fire({
       title: 'كيف تود تسجيل الدخول او انشاء حساب',
       showDenyButton: true,
       showCancelButton: false,
       confirmButtonColor: '#155b92',
       denyButtonColor: '#0f5a69',
       confirmButtonText: `حساب معلم`,
       denyButtonText: `حساب طالب`,
     }).then((result) => {
       /* Read more about isConfirmed, isDenied below */
       if (result.isConfirmed) {
            window.setTimeout(window.location.href = "/teacher/login",9000);
        } else if (result.isDenied) {
                        window.setTimeout(window.location.href = "/student/login",9000);

        }
     })
    }
     </script>
</body>
</html>
