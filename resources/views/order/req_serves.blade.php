@extends('layout.parent')


@section('Title', 'اكمال الطلب')

@section('CSS')

    <link href="{{ asset('assets/css/Star.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/profile.css') }}" rel="stylesheet">
    <style>
        .custom-file {
            position: relative;
            display: inline-block;
            width: 100%;
            height: calc(2.25rem + 2px);
            margin-bottom: 0;
        }

        .custom-file-input {
            position: relative;
            z-index: 2;
            width: 100%;
            height: calc(2.25rem + 2px);
            margin: 0;
            opacity: 0;
        }

        .custom-file-label {
            position: absolute;
            font-family: 'cairo';
            top: 0;
            left: 0;
            right: 0;
            z-index: 1;
            height: calc(2.25rem + 2px);
            padding: .375rem .75rem;
            font-weight: 400;
            line-height: 1.5;
            color: #495057;
            background-color: rgb(235, 235, 235);
            border: 1px solid #ced4da;
            border-radius: .25rem;
        }

    </style>

@endsection



@section('dd')
    <a href="mailto:ahmed8051741@gmail.com" class="visitor"> &nbsp; الدعم الفني عبر البريد الالكتروني
    </a>
@endsection

@section('content')


@section('Top')
    <section id="topbar">
        <div class="container d-flex justify-content-center justify-content-md-between" id="topb">
            <div class="contact-info d-flex align-items-center">
                <a href="mailto:ahmed8051741@gmail.com" class="visitor"> &nbsp; الدعم الفني عبر البريد الالكتروني </a>
            </div>
            <div class="d-flex flex-row align-items-center feed-text px-2" dir="ltr">
                &nbsp;&nbsp;
                <div class="d-flex flex-column flex-wrap ml-2"><span class="font-weight-bold">
                        <div class="dropdown">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" style="color: #ffffff;"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="rounded-circle" src="{{ asset('assets/img/my.jpeg') }}" width="44">
                                &nbsp;&nbsp;
                                {{ auth()->user()->name }} &nbsp;</a>

                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="{{ route('showprofile') }}">الملف الشخصي</a>
                                <hr style="margin-top:0.8px;margin-bottom:0.8px">

                                @if (auth()->user()->id == auth('teacher')->id())

                                    <a class="dropdown-item" href="{{ route('services.create') }}">اضافة خدمة جديد</a>

                                @else
                                    <a class="dropdown-item" href="{{ route('order.create') }}">اضافة طلب جديد</a>
                                @endif

                                <a class="dropdown-item" href="{{ route('control') }}">لوحة القيادة</a>
                                <a class="dropdown-item" href="{{ route('logout_log') }}">تسجيل الخروج</a>
                            </div>
                        </div>
                    </span>

                </div>
            </div>
        </div>
    </section>
@endsection

<div class="container bootstrap snippets bootdey">
    <div class="row">
        <div class="profile-nav col-md-3">
            <div class="panel">
                <div class="user-heading round">
                    <a href="#">
                        <img src="{{ asset('storage/image_profile/' . $service->Teacher->image) }}" alt="">
                    </a>
                    <h1> {{ $service->Teacher->name }}</h1>
                    <p> التخصص :

                        @if ($service->Teacher->major == null)
                            لم يتم اختيار التخصص بعد
                        @else
                            {{ $service->Teacher->major }}
                        @endif

                    </p>
                    <p> هاتف :
                        @if ($service->Teacher->mobile == null)
                            لا يوجد رقم هاتف

                        @else
                            {{ $service->Teacher->mobile }}

                        @endif

                </div>


                <ul class="nav">
                    <li class=""><a href="{{ route('show.teacher', $service->Teacher->id) }}"><i class="fa fa-user">
                                &nbsp;</i>
                            الملف
                            الشخصي</a></li>
                    <li><a href="mailto:{{ $service->Teacher->email }}" target="_blank"><i class="far fa-envelope">
                                &nbsp;</i>
                            تواصل من
                            خلال البريد الالكتروني </a></li>
                    <li><a href="{{ $service->Teacher->facebook }}" target="_blank"><i
                                class="fab fa-facebook-square">
                                &nbsp;</i> تواصل من
                            خلال فيسبوك
                            بوك</a></li>
                    <li><a href="https://wa.me/{{ $service->Teacher->mobile }}" target="_blank"><i
                                class="fab fa-whatsapp" style="  font-size: 14px"> &nbsp;</i>
                            تواصل من خلال واتس
                            اب </a>
                    </li>
                    <li><a href="https://wa.me/{{ $service->Teacher->instagram }}" target="_blank"><i
                                class="fab fa-instagram" style="  font-size: 14px"> &nbsp;</i>
                            تواصل من خلال انستجرام </a>
                    </li>
                    <li><a href="{{ $service->Teacher->youtube }}" target="_blank"><i class="fab fa-youtube">
                                &nbsp;</i>الذهاب
                            الى قناه
                            اليوتيوب
                        </a></li>



                </ul>
            </div>
        </div>


        <div class="profile-info col-md-9" style="margin-bottom: 60px">
            <div class="panel">
                <div class="bg-info clearfix">
                    <button type="button" class="btn btn-secondary float-right"> اتمام الطلب </button>
                </div>
            </div>


            <div class="card">
                <div class="card-header" style="padding-top: 13px;padding-bottom: 12px;padding-right:18px;width: 100%;">


                    <h4> لاتمام طلب الخدمه يجب عليك اكمال البينات في الاسفل </h3>


                </div>


                <!-- /.card-header -->
                <div class="card-body">

                    <form>



                        <div class="form-group row">
                            <label class="col-4 col-form-label mb-2"> عنوان الخدمة التي تريدها</label>
                            <div class="col-8">
                                <input class="form-control" required="required" type="text"
                                    value="{{ $service->title }}" disabled>
                            </div>
                        </div>
                        <div style="display: none">
                            <div class="form-group row">
                                <label class="col-4 col-form-label mb-2"> </label>
                                <div class="col-8">
                                    <input id="services_id" class="form-control" required="required" type="hidden"
                                        value="{{ $service->id }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-4 col-form-label mb-2"> اسم المتقدم لطلب الخدمة </label>
                                <div class="col-8">
                                    <input class="form-control" type="hidden" value="{{ $service->teacher->id }}"
                                        disabled id="teacher_id">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-8">
                                    <input class="form-control" type="hidden"
                                        value="{{ auth()->user()->account_type }}" disabled id="account_type">
                                </div>
                            </div>

                            {{-- ----------------------------------------------------------------------------------------------------------------- --}}
                            @if (auth()->user()->id == auth('student')->id())
                                {{-- المتقدم للخدمه طالب --}}
                                <div class="col-8">
                                    <input class="form-control" type="hidden" value="{{ auth()->user()->id }}"
                                        disabled id="student_id">
                                </div>

                                <div class="col-8">
                                    <input class="form-control" type="hidden" value="" disabled id="teacher_sd">
                                </div>

                            @elseif ( auth()->user()->id == auth('teacher')->id())
                                {{-- المتقدم للخدمه طالب --}}
                                <div class="col-8">
                                    <input class="form-control" type="hidden" value="" disabled id="student_id">
                                </div>

                                <div class="col-8">
                                    <input class="form-control" type="hidden" value="{{ auth()->user()->id }}"
                                        disabled id="teacher_sd">
                                </div>
                            @endif
                            {{-- ----------------------------------------------------------------------------------------------------------------- --}}


                        </div>
                        <div class="form-group row">
                            <label class="col-4 col-form-label mb-2"> اسم المتقدم لطلب الخدمة </label>
                            <div class="col-8">
                                <input class="form-control" type="text" value="{{ auth()->user()->name }}" disabled>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="pay" class="col-4 col-form-label mb-2"> طريقة الدفع </label>
                            <div class="col-8">
                                <select id="pay" name="select" class="form-select">
                                    <option> الدفع نقداً عند المقابلة </option>
                                </select>
                            </div>
                        </div>

                        {{-- <div class="form-group row">
                            <label for="daye" class="col-4 col-form-label mb-2"> اليوم
                                 </label>
                            <div class="col-8">
                                <input  class="form-control"
                                      type="text" placeholder="ادخل اليوم الذي يناسبك للتواصل معك" id="daye">
                            </div>
                        </div> --}}


                        <div class="form-group row">

                            <label for="time" class="col-4 col-form-label mb-2"> ادخل الساعة التي تناسبك للتواصل معك
                            </label>
                            <div class="col-8">
                                <input class="form-control" type="time"
                                    placeholder="ادخل الساعة الذي يناسبك للتواصل معك" id="time"
                                    style="Al-Jazeera-Arabic">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="date" class="col-4 col-form-label mb-2"> التاريخ
                            </label>
                            <div class="col-8">
                                <input class="form-control" type="date" style="Al-Jazeera-Arabic" id="date">
                            </div>
                        </div>
                        <br>

                        <button type="button" onclick="store()" class="btn btn-primary" id="button"
                            style="float: left; width: 100%"> تأكيد الطلب </button>
                        <br>
                        <br>
                        <br>
                    </form>
                </div>

                <!-- /.card-body -->
            </div>

        </div>
    </div>
</div>


@endsection


@section('JS')

<script>
    function store() {
        Swal.fire({

            title: 'تحذير !',
            text: 'لن تتمكن من التراجع هل انت متأكد من طلبك لهذه الخدمه ..!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'الغاء',
            confirmButtonText: 'نعم ، اكمل من فضلك',
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                axios.post('/serves/Certain/', {
                        services_id: document.getElementById('services_id').value,
                        teacher_id: document.getElementById('teacher_id').value,
                        pay: document.getElementById('pay').value,
                        date: document.getElementById('date').value,
                        time: document.getElementById('time').value,
                        teacher_sd: document.getElementById('teacher_sd').value,
                        student_id: document.getElementById('student_id').value,
                        account_type: document.getElementById('account_type').value,
                    })
                    .then(function(response) {
                        // handle success
                        console.log(response);
                        toastr.success(response.data.message);
                        // document.getElementById('create-form').reset(); // تصفير قيمة الفورم سبمت
                        window.location.href = "/Home/"; // التحويل والانتقال
                    })
                    .catch(function(error) {
                        // handle error
                        console.log(error);
                        toastr.error(error.response.data.message);
                    })
                    .then(function() {
                        // always executed
                    });
            }
        })
    }
</script>


@endsection
