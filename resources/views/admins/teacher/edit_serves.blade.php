@extends('admins.parent')
@section('title', ' تعديل بينات الخدمه ')
@section('bige-title', 'تعديل بينات الخدمه ')
@section('main-page', 'الرئيسية')
@section('sub-page', ' تعديل بينات الخدمه ')
@section('user', 'menu-open')

@section('content')
<!-- Main content -->
<section class="content">
<div class="container-fluid">
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title" style="float: right">تعديل بينات طالب </h3>
    </div>
    <!-- /.card-header -->

    <!-- /.card-header -->
    <div class="card-body">

        <form>



            <div class="form-group row">
                <label for="name" class="col-4 col-form-label mb-2"> عنوان الخدمة </label>
                <div class="col-8">
                    <input id="name" placeholder="عنوان للخدمة التي ستقدمها" class="form-control"
                        required="required" type="text" value="{{ $service->title }}">
                </div>
            </div>

            <div class="form-group row">
                <label for="price" class="col-4 col-form-label mb-2"> سعر الخدمه التي تقدمها بالشيكل
                </label>
                <div class="col-8">
                    <input id="price" placeholder="ادخل السعر هنا بالشيكل" class="form-control"
                        required="required" type="text" value="{{ $service->price }}">
                </div>
            </div>

            <div class=" form-group row">
                <label for="select" class="col-4 col-form-label mb-2"> تصنيف الخدمه </label>
                <div class="col-8">
                    <select id="select" name="select" class="form-select">
                        <option value=" {{ $service->category->id }} ">
                            {{ $service->category->name }}
                        </option>
                        @foreach ($categorise as $categoris)
                            <option value=" {{ $categoris->id }} "> {{ $categoris->name }}
                            </option>
                        @endforeach

                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="city" class="col-4 col-form-label mb-2"> المنطقة التي تتيح فيها الخدمة
                </label>
                <div class="col-8">
                    <select id="city" name="city" class="form-select">
                        <option value=" {{ $service->City->id }}  ">{{ $service->City->name }}
                        </option>
                        @foreach ($city as $cities)
                            <option value=" {{ $cities->id }} "> {{ $cities->name }} </option>
                        @endforeach

                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="select_CAT" class="col-4 col-form-label mb-2"> الفئة المستهدفة </label>
                <div class="col-8">
                    <select id="select_CAT" name="select" class="form-select">
                        <option value=" {{ $service->select_CAT }} "> {{ $service->select_CAT }}
                        </option>
                        <option value="المرحلة الاعدادية"> المرحلة الاعدادية </option>
                        <option value="الثانوية العامه"> الثانوية العامه </option>
                        <option value="المرحلة الثانوية بدون توجيهي"> المرحلة الثانوية بدون توجيهي
                        </option>
                        <option value="الثانوية العامه (توجيهي)"> الثانوية العامه (توجيهي) </option>
                        <option value="المرحلة الاساسية و الاعدادية"> المرحلة الاساسية و الاعدادية
                        </option>
                        <option value="المرحلة الاساسية و الاعدادية والثانوية"> المرحلة الاساسية و
                            الاعدادية
                            والثانوية</option>
                        <option value="المرحلة الاعدادية والثانوية"> المرحلة الاعدادية والثانوية
                        </option>
                        <option value="المرحلة الجامعية"> المرحلة الجامعية </option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="describe" class="col-4 col-form-label mb-2"> </label>
                <div class="col-8">
                    <input id="teacher_id" type="text" value="{{ auth()->user()->id }}"
                        style="visibility: hidden" disabled>
                </div>
            </div>

            <div class="form-group row">
                <label for="describe" class="col-4 col-form-label mb-2"> تفاصيل الخدمه </label>
                <div class="col-8">
                    <textarea id="describe" cols="40" rows="4"
                        class="form-control"> {{ $service->describe }}</textarea>
                </div>
            </div>
            <br>
            <br>

            <div class="custom-file">
                <input type="file" class="custom-file-input" id="category_image" accept="image/*,.">
                <label class=" custom-file-label" for="category_image">
                    {{ $service->image }}
                </label>
            </div>

            <br>
            <br>
            <br>

            <button type="button" onclick="update({{ $service->id }})" class="btn btn-primary"
                id="button" style="float: left; width: 100%"> تعديل الخدمه </button>
            <br>
            <br>
            <br>
        </form>
    </div>
</div>


</div>

</div>
<!-- /.row -->
</div><!-- /.container-fluid -->
</section>

@endsection

@section('JS')


<script>
    function update(id) {
        let formData = new FormData();
        formData.append('_method', 'PUT');
        formData.append('name', document.getElementById('name').value);
        formData.append('price', document.getElementById('price').value);
        formData.append('select', document.getElementById('select').value);
        formData.append('city', document.getElementById('city').value);
        formData.append('teacher_id', document.getElementById('teacher_id').value);
        formData.append('describe', document.getElementById('describe').value);
        formData.append('select_CAT', document.getElementById('select_CAT').value);
        formData.append('category_image', document.getElementById('category_image').files[0]);

        axios.post('/profile/control/services/' + id, formData)
            .then(function(response) {
                // handle success
                console.log(response);
                toastr.success(response.data.message);
                // document.getElementById('create-form').reset();
                setTimeout(function() {
                    window.location.href = "/profile/control/services";
                }, 1500);

            })
            .catch(function(error) {
                // handle error
                console.log(error);
                toastr.error(error.response.data.message);
            })
            .then(function() {
                // always executed
            });
    }
</script>






{{-- <script>
    function update(id){
        axios.put('/cms/admin/categories/'+id,{
            name: document.getElementById('name').value,
            active: document.getElementById('active').checked
        })
        .then(function (response) {
            // handle success
            console.log(response);
            toastr.success(response.data.message);
            // document.getElementById('create-form').reset();
            window.location.href = "/cms/admin/categories";
        })
        .catch(function (error) {
            // handle error
            console.log(error);
            toastr.error(error.response.data.message);
        })
        .then(function () {
            // always executed
        });
    }
</script> --}}
@endsection
