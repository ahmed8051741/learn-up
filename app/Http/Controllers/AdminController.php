<?php

namespace App\Http\Controllers;

use App\Mail\WelcomeEmail;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $admins = Admin::where('id', '!=', auth('admin')->id())->get();
        return response()->view('admins.admin.index', ['admins' => $admins]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return response()->view('admins.admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator(
            $request->all(),
            [
                'name' => 'required|string|min:3|max:30',
                'email' => 'required|email',
                'active' => 'required|boolean',
            ],
            [
                'name.required' => 'لا يمكن ترك حقل الاسم فارغاً',
                'name.string' => 'يجب ان يكون نصاً فقط',
                'name.min' => 'هذا الاسم قصير جداً',
                'name.max' => 'هذا الاسم طويلاً جداً',
                'email.required' => 'لا يمكن ترك حقل البريد الالكتروني فارغاً',
                'email.email' => 'صيغة البريد الالكرتوني خاطئه',
                'active.required' => 'حدد حالة الحساب',
            ]
        );
        if (!$validator->fails()) {
            $admin = new Admin();
            $admin->name = $request->get('name');
            $admin->email = $request->get('email');
            $admin->password = Hash::make('password');
            $admin->active = $request->get('active');
            $isSaved = $admin->save();

            //  Mail::to($admin->email)->send(new WelcomeEmail($admin));

            return response()->json([
                'message' => $isSaved ? "تم العملية بنجاح" : "حدث خطاء غير معروف"
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json(['message' => $validator->getMessageBag()->first()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        //
        return response()->view('admins.admin.edit', ['admin' => $admin]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        //
        $validator = Validator(
            $request->all(),
            [
                'name' => 'required|string|min:3|max:30',
                'email' => 'required|email',
                'active' => 'required|boolean',
            ],
            [
                'name.required' => 'لا يمكن ترك حقل الاسم فارغاً',
                'name.string' => 'يجب ان يكون نصاً فقط',
                'name.min' => 'هذا الاسم قصير جداً',
                'name.max' => 'هذا الاسم طويلاً جداً',
                'email.required' => 'لا يمكن ترك حقل البريد الالكتروني فارغاً',
                'email.email' => 'صيغة البريد الالكرتوني خاطئه',
                'active.required' => 'حدد حالة الحساب',
            ]

        );
        if (!$validator->fails()) {
            $admin->name = $request->get('name');
            $admin->email = $request->get('email');
            $admin->password = Hash::make('password');
            $admin->active = $request->get('active');
            $isSaved = $admin->save();
            return response()->json([
                'message' => $isSaved ? "تم العملية بنجاح" : "حدث خطاء غير معروف"
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json(['message' => $validator->getMessageBag()->first()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        //
        $isDelete = $admin->delete();
        if ($isDelete) {
            return response()->json(
                ['title' => 'نجاح', 'text' => 'تم حذف المستخدم المشرف بنجاح', 'icon' => 'success'],
                Response::HTTP_OK
            );
        } else {
            return response()->json(
                ['title' => 'فشل', 'text' => 'فشل الحذف ', 'icon' => 'error'],
                Response::HTTP_BAD_REQUEST
            );
        }
    }
}
