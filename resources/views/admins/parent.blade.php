<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> Learn up Admin | @yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('CMS/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->

    <link rel="stylesheet" href="{{ asset('CMS/dist/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('CMS/dist/css/adminlte.min.css') }}">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css2?family=Almarai&display=swap" rel="stylesheet">
    <!-- Bootstrap 4 RTL -->
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css">
    <!-- Custom style for RTL -->
    <link rel="stylesheet" href="{{ asset('CMS/dist/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('CMS/plugins/toastr/toastr.min.css') }}">

    <style>
        * {
            font-family: 'cairo';

        }

    </style>
</head>

<body class="hold-transition sidebar-mini layout-fixed" dir='rtl'>
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ route('dashboard') }}" class="nav-link">دفقة القيادة </a>
                </li>

            </ul>

            <!-- SEARCH FORM -->

        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="index3.html" class="brand-link">
                <img src="{{ asset('CMS/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo"
                    class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light"> &nbsp; Learn Up &nbsp;</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar" id="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src=" {{ asset('storage/image_profile/' . auth('admin')->user()->image) }}"
                            class="img-circle elevation-2" alt="User Image">

                    </div>
                    <div class="info">
                        <a href="#" class="d-block">{{ auth('admin')->user()->name }}</a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">

                        <li class="nav-item ">
                            <a href="{{ route('dashboard') }}" class="nav-link @yield('dashboard')">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    دفقة القيادة
                                </p>
                            </a>
                        </li>

                        <li class="nav-header">الموارد البشرية</li>
                        <li class="nav-item @yield('himen')">
                            <a href="" class="nav-link">
                                <i class="nav-icon fas fa-user-tie"></i>
                                <p>
                                    المشرفون
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('admins.create') }}" class="nav-link">
                                        <i class="far fa-plus-square nav-icon"></i>
                                        <p> انشاء </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admins.index') }}" class="nav-link">
                                        <i class="fas fa-list-ul nav-icon"></i>
                                        <p> عرض </p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        {{-- --------------------------------------------------------------------------- --}}
                        <li class="nav-item @yield('user')">
                            <a href="" class="nav-link">
                                <i class="nav-icon  fas fa-users"></i>
                                <p>
                                    المستخدمون
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('teacher_management.index') }}" class="nav-link">
                                        <i class="fas fa-chalkboard-teacher nav-icon"></i>
                                        <p> معلمين </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('student_management.index') }}" class="nav-link">
                                        <i class="fas fa-user-graduate nav-icon"></i>
                                        <p> طلاب </p>
                                    </a>
                                </li>
                            </ul>
                        </li>





                        <li class="nav-header">ادارة المحتوى</li>
                        <li class="nav-item ">
                            <a href="{{ route('boxing.index') }}" class="nav-link @yield('B')">
                                <i class="nav-icon fas fa-box-open"></i>
                                <p>
                                    الاقتراحات والشكاوي
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-layer-group"></i>
                                <p>
                                    الفئات
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview" style="display: none;">
                                <li class="nav-item">
                                    <a href="{{ route('categories.create') }}" class="nav-link">
                                        <i class="far fa-plus-square nav-icon"></i>
                                        <p>انشاء </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('categories.index') }}" class="nav-link">
                                        <i class="fas fa-list-ul nav-icon"></i>
                                        <p>عرض</p>
                                    </a>
                                </li>

                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-street-view"></i>
                                <p>
                                    المحافظات
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview" style="display: none;">
                                <li class="nav-item">
                                    <a href="{{ route('cities.create') }}" class="nav-link">
                                        <i class="far fa-plus-square nav-icon"></i>
                                        <p>انشاء </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('cities.index') }}" class="nav-link">
                                        <i class="fas fa-list-ul nav-icon"></i>
                                        <p>عرض</p>
                                    </a>
                                </li>

                            </ul>
                        </li>




                        <li class="nav-header">الاعدادات</li>

                        <li class="nav-item">
                            <a href="{{ route('edit-password') }}" class="nav-link">
                                <i class="nav-icon fas fa-lock"></i>
                                <p>
                                    تغير كلمه المرور
                                </p>
                            </a>
                        </li>
                        <li class="nav-item @yield('profile')">
                            <a href="{{ route('edit_profile') }}" class="nav-link">
                                <i class="nav-icon fas fa-edit"></i>
                                <p>
                                    تعديل الملف الشخصي
                                </p>
                            </a>
                        </li>

                        <li class="nav-item @yield('profile_img')">
                            <a href="{{ route('edit_profile_image') }}" class="nav-link">
                                <i class="nav-icon far fa-id-card"></i>
                                <p>
                                    تعديل صورة الملف الشخصي
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('logout') }}" class="nav-link">
                                <i class="nav-icon fas fa-sign-out-alt"></i>
                                <p>
                                    تسجيل الخروج
                                </p>
                            </a>
                        </li>

                    </ul>
                </nav>
            </div>

            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">@yield('bige-title')</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">@yield('main-page')</a></li>
                                <li class="breadcrumb-item active">@yield('sub-page')</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            @yield('content')


        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <strong> رقم الاصدار : {{ env('App_VERSION') }}</strong>

            <div class="float-right d-none d-sm-inline">
                <strong>حقوق التطوير والنشر محفوظة لدى <a href="{{ env('APP_URL') }}">{{ env('APP_NAME') }}</a>
                    &copy;
                </strong>{{ now()->year }} - {{ now()->year + 1 }}
            </div>

        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- jQuery -->
    <script src="{{ asset('CMS/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 rtl -->
    <script src="https://cdn.rtlcss.com/bootstrap/v4.2.1/js/bootstrap.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('CMS/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('CMS/dist/js/adminlte.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <!-- Toastr رسائل الخطاء  -->
    <script src="{{ asset('CMS/plugins/toastr/toastr.min.js') }}"></script>

    <script src="{{ asset('CMS/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>

    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('CMS/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">

    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-46156385-1', 'cssscript.com');
        ga('send', 'pageview');
    </script>

    @yield('JS')
</body>

</html>
