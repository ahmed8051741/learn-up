@extends('admins.parent')

@section('title', ' انشاء منطقة ')
@section('bige-title', 'انشاء منطقة')
@section('main-page', 'الرئيسية')
@section('sub-page', 'المحافظات')

@section('content')
<!-- Main content -->
<section class="content">
<div class="container-fluid">
<div class="row">
<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title" style="float: right"> اضافة منطقة جديدة </h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form id="create-form">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="name">الاسم</label>
                    <input type="text" class="form-control" id="name"
                        placeholder="ادخل اسم المحافظة او المنطقة">
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="button" onclick="store()" class="btn btn-primary" id="button">حفظ
                    ونشر</button>
            </div>
        </form>
    </div>


</div>

</div>
<!-- /.row -->
</div><!-- /.container-fluid -->
</section>

@endsection

@section('JS')
<script>
    function store() {
        axios.post('/admin/cities', {
                name: document.getElementById('name').value,
            })
            .then(function(response) {
                // handle success
                console.log(response);
                toastr.success(response.data.message);
                // document.getElementById('create-form').reset(); // تصفير قيمة الفورم سبمت
                setTimeout(function() {
                    window.location.href = "/admin/cities";
                }, 1500); // التحويل والانتقال
            })
            .catch(function(error) {
                // handle error
                console.log(error);
                toastr.error(error.response.data.message);
            })
            .then(function() {
                // always executed
            });
    }
</script>
@endsection
