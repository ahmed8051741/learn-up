@extends('layout.parent')


@section('Title', 'عرض الطلبات التي طلبتها')

@section('CSS')

<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="{{ asset('assets/css/Star.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/profile.css') }}" rel="stylesheet">
<style>
    #header {
        visibility: hidden;
        margin-top: -50px;
    }



    * {
        font-family: 'cairo';
    }

    .visitor {
        color: white;
        font-size: 15px;


    }

    .row {
        display: flex;
        flex-wrap: wrap;
        margin-right: -15px;
        margin-left: -15px;
    }

    @media (min-width: 768px) {
        .col-md-12 {
            flex: 0 0 100%;
            max-width: 100%;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            font-family: "Al-Jazeera-Arabic";
        }

        .h4,
        h4 {
            font-size: 1.5rem;
        }

        .form-group {
            margin-bottom: 5px;
            margin-top: 5px
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            margin-top: 0;
            margin-bottom: .5rem;
        }

    }

    .card {
        margin-top: 12px
    }

    .form-group {
        margin-bottom: 1rem;
    }

    h4 {
        display: block;
        margin-block-start: 1.33em;
        margin-block-end: 1.33em;
        margin-inline-start: 0px;
        margin-inline-end: 0px;

    }


    /* .btn-group>.btn-group:not(:last-child)>.btn, .btn-group>.btn:not(:last-child):not(.dropdown-toggle) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    border-top-right-radius: 4px;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    border-bottom-right-radius: 4px;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                }

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                .btn-group>.btn-group:not(:first-child)>.btn, .btn-group>.btn:nth-child(n+3), .btn-group>:not(.btn-check)+.btn {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         border-top-left-radius:4px;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         border-bottom-left-radius:4px;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                } */
    .dropdown-menu {
        text-align: right;
        right: 0px;
        min-width: 11rem;
    }

    .pro1 {
        border-top-right-radius: 0px;
        border-bottom-right-radius: 0px;
        border-top-left-radius: 0px;
        border-bottom-left-radius: 0px;
    }

    .pro {
        border-top-right-radius: 0px;
        border-bottom-right-radius: 0px;


    }

    h4 {
        margin-block-start: 0.33em;
        margin-block-end: 0.33em;
        margin-inline-start: 0px;
        margin-inline-end: 0px;
    }

    .table>:not(caption)>*>* {
        padding: .5rem .5rem;
        padding-top: 20px;
    }

</style>
@endsection

@section('dd')
<h2 class="logo" style="font-family: Al-Jazeera-Arabic"><a href="{{ route('showHome') }}"
        style="font-family: Al-Jazeera-Arabic; color: aliceblue"> Learn <span
            style="font-family: Al-Jazeera-Arabic;font-weight: 760;color: #Fd4;"> up
        </span></a></h2>
@endsection
@section('con', 'display: none')




@section('content')



<div class="container bootstrap snippets bootdey">
<div class="row">
    <div class="profile-nav col-md-3">
        <div class="panel">
            <div class="user-heading round">
                <a href="#">
                    <img src="{{ asset('storage/image_profile/' . auth()->user()->image) }}" alt="">
                </a>
                <h1> {{ auth()->user()->name }} </h1>
                <p>{{ auth()->user()->email }} </p>
            </div>

            <ul class="nav">
                <li><a href="{{ route('control') }}"><i class="fas fa-tachometer-alt">
                            &nbsp;</i>
                        دفقة القيادة </a></li>
                <li><a href="{{ route('showprofile') }}"><i class="fa fa-user"> &nbsp;</i> الملف الشخصي </a>
                </li>

                @if (auth()->user()->id == auth('teacher')->id())
                    <li><a href="{{ route('services.index') }}"><i class="fab fa-servicestack">
                                &nbsp;</i>
                            الخدمات
                        </a></li>

                    <li><a href="{{ route('rqserve_all') }}"><i class="fas fa-retweet"> &nbsp;</i>
                            الطلبات الواردة
                        </a></li>
                @else
                    <li class="active"><a href="{{ route('order.index') }}"><i class="fab fa-servicestack">
                                &nbsp;</i>
                            الطلبات
                        </a></li>


                    <li><a href="{{ route('rqserve_allstd') }}"><i class="fas fa-retweet"> &nbsp;</i>
                            العروض المقدمه
                        </a></li>
                @endif
                <li><a href="{{ route('password_edit') }}"><i class="fas fa-lock"> &nbsp;</i> تعديل كلمه
                        المرور</a></li>



                <li><a href="{{ route('logout_log') }}"><i class="fas fa-sign-out-alt"> &nbsp;</i> تسجيل
                        الخروج</a></li>
            </ul>
        </div>
    </div>


    <div class="profile-info col-md-9" style="margin-bottom: 60px">
        <div class="panel">
            <div class="bg-info clearfix">
                <button type="button" class="btn btn-secondary float-right"> الطلبات واعدادتها </button>
            </div>
        </div>


        <div class="card">
            <div class="card-header" style="padding-top: 13px;padding-bottom: 12px;padding-right:18px;width: 100%;">
                <div class="col-4" style="float: left; "> <a href="{{ route('order.create') }}">
                        <button type="button" class="btn btn-outline-primary" style="width: 100%"> <i
                                class="far fa-plus-square">&nbsp;</i> اضف طلب جديد </button>
                    </a>
                </div>

                <div class="col-8">
                    <h4> جميع الطلبات التي قمت بتقديمها </h3>
                </div>


            </div>


            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table id="example1" class="table table-bordered table-striped main-content"
                    style="text-align: center">
                    <thead dir="rtl" class="thead_dark">
                        <tr>
                            <th>#</th>
                            <th>عنوان الطلب</th>
                            <th> حالة الطلب </th>
                            <th> تاريخ اضافة الطلب </th>
                            <th> اجراءات </th>
                        </tr>
                    </thead>
                    <tbody dir="rtl">
                        @foreach ($orders as $order)

                            @if ($order->student->id == auth('student')->user()->id)
                                <tr>
                                    <td>{{ $order->id }}</td>
                                    <td>{{ $order->title }}</td>
                                    <td>
                                        <span class="badge @if ($order->status) bg-success @else bg-danger @endif ">
                                            {{ $order->Status_Keyword }} </span>
                                    </td>
                                    <td> {{ $order->created_at }} </td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('order.edit', $order->id) }}"
                                                class="btn btn-info  pro1">
                                                <i class="fas fa-edit"></i>
                                            </a>

                                            <a href="#" class="btn btn-danger pro"
                                                onclick="confirmDestroy({{ $order->id }}, this)">
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                            <a href="{{ route('requests_Std') }}#{{ $order->id }}"
                                                class="btn btn-success pro" onclick="show()">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                            <a href="{{ route('showstd.Certain', $order->id) }}"
                                                class="btn btn-primary pro">
                                                <i class="fas fa-business-time"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endif

                        @endforeach

                    </tbody>
                    <hr>
                </table>

            </div>


            <!-- /.card-body -->
        </div>

    </div>
</div>
</div>


@endsection


@section('JS')



<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-46156385-1', 'cssscript.com');
    ga('send', 'pageview');
</script>



<!-- jQuery -->
<script src="{{ asset('CMS/plugins/jquery/jquery.min.js') }}"></script>


<script src="{{ asset('CMS/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('CMS/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('CMS/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('CMS/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('CMS/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('CMS/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('CMS/plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('CMS/plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('CMS/plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('CMS/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('CMS/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('CMS/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('dist/js/demo.js') }}"></script>
<!-- Page specific script -->
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": true,
            "autoWidth": false,
            "buttons": ["copy", "excel", "pdf", "print"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });
    });
</script>
<script>
    //عملية الحذف
    function confirmDestroy(id, referince) {
        //    console.log("ID:"+id)    // ->?   (id) تستعمل لفحص الكي اذا تم تمريرة او لا
        Swal.fire({
            title: 'هل انت متأكد من عملية الحذف؟',
            text: "لن تتمكن من التراجع عن هذا!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'الغاء',
            confirmButtonText: 'نعم ، احذفها!',
        }).then((result) => {
            if (result.isConfirmed) {
                destroy(id, referince); // referince لاستقبال الذس من الرابط
            }
        })
    }

    function destroy(id, referince) {
        // Make a request for a user with a given ID
        axios.delete('/profile/control/order/' + id)
            .then(function(response) {
                // handle success
                console.log(response);
                referince.closest('tr')
                    .remove(); //referince في فوق في الرابط او الاشرف وليس شرك ان يسمى بي  this الذي يتبع الى كلمه  referince لحذف الصف دون الحاجة الى تحديث الصفحة بلاستناد على المتغير
                ShowMessage(response.data); // لاستقبال الجايسون من الكنترولر ببيناته رسالة النجاح والايقونة
            })
            .catch(function(error) {
                // handle error
                console.log(error); // اسقبال رسائل وبينات الخطاء
                ShowMessage(error.response.data);
            })
            .then(function() {
                // دائماً منفذة لو بدي انفذ داله معينة في كل الحالات
            });

    }

    function ShowMessage(data) {
        Swal.fire({
            icon: data.icon, // طباعة الايقونة والاعنوان بناء على البينات المستقبلة من الكنترولر
            title: data.title,
            text: data.text,
            showConfirmButton: false,
            timer: 1500
        })
    }
</script>

@endsection
