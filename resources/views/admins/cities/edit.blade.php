@extends('admins.parent')
@section('title', ' تعديل اسم المنطقة ')
@section('bige-title', ' تعديل اسم المنطقة ')
@section('main-page', 'الرئيسية')
@section('sub-page', 'تعديل اسم المنطقة')

@section('content')
<!-- Main content -->
<section class="content">
<div class="container-fluid">
<div class="row">
<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title" style="float: right">تعديل منطقة </h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form id="create-form">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="name">اسم المنطقة</label>
                    <input type="text" class="form-control" id="name" placeholder="Enter name"
                        value="{{ $city->name }}">
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="button" onclick="update({{ $city->id }})" class="btn btn-primary"
                    id="button">تحديث</button>
            </div>
        </form>
    </div>


</div>

</div>
<!-- /.row -->
</div><!-- /.container-fluid -->
</section>

@endsection

@section('JS')
<script>
    function update(id) {
        axios.put('/admin/cities/' + id, {
                name: document.getElementById('name').value,
            })
            .then(function(response) {
                // handle success
                console.log(response);
                toastr.success(response.data.message);
                // document.getElementById('create-form').reset(); // تصفير قيمة الفورم سبمت
                setTimeout(function() {
                    window.location.href = "/admin/cities"; // التحويل والانتقال
                }, 1500); // التحويل والانتقال
            })
            .catch(function(error) {
                // handle error
                console.log(error);
                toastr.error(error.response.data.message);
            })
            .then(function() {
                // always executed
            });
    }
</script>
{{-- <script>
    function update(id){
        axios.put('/cms/admin/categories/'+id,{
            name: document.getElementById('name').value,
            active: document.getElementById('active').checked
        })
        .then(function (response) {
            // handle success
            console.log(response);
            toastr.success(response.data.message);
            // document.getElementById('create-form').reset();
            window.location.href = "/cms/admin/categories";
        })
        .catch(function (error) {
            // handle error
            console.log(error);
            toastr.error(error.response.data.message);
        })
        .then(function () {
            // always executed
        });
    }
</script> --}}
@endsection
