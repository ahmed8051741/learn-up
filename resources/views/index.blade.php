@extends('layout.parent')


@section('Title', 'الرئيسية')


@section('CSS')
    <link href="{{ asset('assets/assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

    {{-- <link href="{{ asset('assets/assets/css/style.css" rel="stylesheet') }}"> --}}

    <link href="{{ asset('assets/assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/assets/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/assets/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">

    {{-- <link href="" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet"> --}}
    <style type="text/css">
        .visitor {
            color: white;
            font-size: 15px
        }

        .visitor:hover {
            color: antiquewhite;

        }

        .btn {
            line-height: 1.6;

        }

        .form-control {
            width: 398px;
        }

        .search {
            width: 98px;
            height: 30px;
            position: relative;
            left: 10px;
            margin-top: 10px;
            margin-right: 10px;
            margin-bottom: 10px;
        }

        .search input {
            position: absolute;
            float: Left;
            border-radius: 1px;
            width: 10px;
        }


        .btnsd {
            height: 38px;
            position: absolute;
            left: 32%;
            border-radius: 1px;
            padding-right: 14px;
        }

    </style>

@endsection

@section('dd')
    <a href="mailto:ahmed8051741@gmail.com" class="visitor"> &nbsp; الدعم الفني عبر البريد الالكتروني
    </a>
@endsection
@section('active', 'active')

@section('content')


    <section id="hero" class="d-flex align-items-center">
        <div class="container" data-aos="zoom-out" id="container" data-aos-delay="100">
            <h1 style="font-family: cairo"> مرحباً بك في موقع <span>Learn up</span></h1>
            <h2 style="font-family: 'Al-Jazeera-Arabic"> ابحث عن المجال الذي تريد تعلمة لنعرض لك افضل المدربين والمعلمين
            </h2>

            @auth
                <form class="row justify-content-center" style="text-align: center" action="{{ route('search') }}"
                    method="POST">
                @endauth
                @guest
                    <form class="row justify-content-center" style="text-align: center" action="{{ route('search_vs') }}"
                        method="POST">
                    @endguest

                    @csrf
                    <div class="col-3">
                        <input type="text" class="btnsd form-control" name="search" id="search"
                            placeholder="ابحث عما تريد .. في خدمات المعلمين ">
                    </div>
                    <div class="col-auto" id="row">
                        <button type="submit" class="btnsd btn btn-primary mb-3">أبحث الآن</button>
                    </div>
                </form>


        </div>


    </section>



    <!-- =======================================================
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * Template Name: Learn up - v->experimental .
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * Created by : Ayat Abu Qasim - Ahmed Farhat - Osama Abu Tawahinah .

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * Type : CSS file .
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ======================================================== -->


    <!-- ======= قسم الخدمات ======= -->
    <section id="featured-services" class="featured-services">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
                    <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                        <div class="icon"><i class="bx bxs-lock-alt"></i></div>
                        <h4 class="title"><a href=""> امن تماماً </a></h4>
                        <p class="description"> معلوماتك الشخصية والمعلومات السرية التي تقدمها امنه تماماً ونحن نحافظ على
                            خصوصيتك </p>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
                    <div class="icon-box" data-aos="fade-up" data-aos-delay="200">
                        <div class="icon"><i class="bx bx-file"></i></div>
                        <h4 class="title"><a href=""> انشاء الخدمات </a></h4>

                        <p class="description">يمكنك من خلالنا انشاء الخدمات وطرحها لجميع الطلاب الراغبين بها </p>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
                    <div class="icon-box" data-aos="fade-up" data-aos-delay="300">
                        <div class="icon"><i class="bx bx-message-alt-detail"></i></div>
                        <h4 class="title"><a href="">طلب الخدمة</a></h4>

                        <p class="description">يمكنك عرض طلب لخدمة معينة فيتمكن المدربين من رؤية طلبك والتواصل معك ويتم ذالك
                            من لوحة القيادة الخاصةبك</p>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
                    <div class="icon-box" data-aos="fade-up" data-aos-delay="400">
                        <div class="icon"><i class="bx bx-tachometer"></i></div>
                        <h4 class="title"><a href=""> سرعة وسهولة التواصل </a></h4>

                        <p class="description"> يمكن للجميع التواصل معك من خلال رقم هاتفك او الواتس اب الذي ستدخلة في بيناتك
                        </p>
                    </div>
                </div>

            </div>

        </div>
    </section>

    <!-- =======================================================
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * Template Name: Learn up - v->experimental .
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * Created by : Ayat Abu Qasim - Ahmed Farhat - Osama Abu Tawahinah .

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * Type : CSS file .
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ======================================================== -->


    <section id="about" class="about section-bg">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2 style="font-family: 'Al-Jazeera-Arabic"> حول موقعنا </h2>
                <h3> أكتشف المزيد <span>حولنا</span></h3>
                <p> موقع الكتروني يقدم خدمات تعليمية سمي بي <span style="color: #106eea"> Learn up </span> نسبة الى التعلم
                    المستمر لأحدث ما يصل من التكنولوجيا المستحدثة والذي يرمز الى استمرارية عملية التطور والتعلم </p>
            </div>

            <div class="row">
                <div class="col-lg-5" data-aos="fade-right" data-aos-delay="100">
                    <img src="assets/img/about.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-lg-7 pt-4 pt-lg-0 content d-flex flex-column justify-content-center" data-aos="fade-up"
                    data-aos-delay="100">
                    <h5 style="font-family: 'Al-Jazeera-Arabic"> يمكنك العثور على مدرسين محليين ومعلمين عبر الإنترنت ومعلمين
                        للمساعدة في التدريس والتدريب والمهام والمشاريع الأكاديمية </h5>
                    <p class="font-italic">

                    </p>
                    <ul>
                        <li>
                            <i class="bx bx-store-alt"></i>
                            &nbsp;
                            &nbsp;
                            <div>
                                <h5> يمكنك توظيف معلمك </h5>
                                <p> وظف معلم لدى شركتك او مؤسستك من خلالنا </p>
                            </div>
                        </li>
                        <li>
                            <i class="bx bx-notepad"></i>
                            &nbsp;
                            &nbsp;
                            <div>
                                <h5> معلمون ذو كفائة عالية </h5>
                                <p> لا نقبل بلمعلمون الغير مؤهلين - لذا فأنت تضمن معلم ذو خبرة وكفائة من خلالنا </p>
                            </div>
                        </li>
                    </ul>

                </div>
            </div>

        </div>
    </section>
    <section id="counts" class="counts">
        <div class="container" data-aos="fade-up">

            <div class="row">

                <div class="col-lg-3 col-md-6">
                    <div class="count-box">
                        <i class="bi bi-headset"></i>
                        <span>24</span>
                        <p style="font-family: 'cairo';"> خدمة الدعم الفني متوفرة دائماً </p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 mt-5 mt-md-0">
                    <div class="count-box">
                        <i class="bi bi-journal-richtext"></i>
                        <span> 250 </span>
                        <p style="font-family: 'cairo';">عدد الخدمات المطروحة</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
                    <div class="count-box">
                        <i class="bi bi-file-earmark-person"></i>
                        <span> 50 </span>
                        <p style="font-family: 'cairo';">عدد المعلمين لدينا</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
                    <div class="count-box">
                        <i class="bi bi-people"></i>
                        <span> 255 </span>
                        <p style="font-family: 'cairo';">عدد الطلاب لدينا</p>
                    </div>
                </div>

            </div>

        </div>
    </section>


    <section id="team" class="team section-bg">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>الفريق</h2>
                <h3> <span>من نحن </span>وما أهدافنا</h3>
                <p style="width: 80%">نحن فريق عمل مكون من 3 اشخاص متخصصون في مجال تصميم وتطوير مواقع الانترنت وقد تم انشاء
                    هذا الموقع كمشروع
                    تخرجناوالذي يهدف الى تسهيل التواصل بين المعليمن والطلاب وطرح وظائف اكبر بنسبة للمعلمين اي تطبيق فكرة
                    مواقع العمل الحر على مستوى القطاع التعليمي والتدريبي .</p>
            </div>

            <div class="row" style="text-align: center ;">

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
                    <div class="member">
                        <div class="member-img">
                            <img src="{{ asset('assets/assets/img/team/team-ayat.jpg') }}" class="img-fluid" alt="">
                            <div class="social" style="text-align: center;">
                                <a href="" style="padding-top: 0px"><i class="bi bi-twitter"></i></a>
                                <a href="" style="padding-top: 0px"><i class="bi bi-facebook"></i></a>
                                <a href="" style="padding-top: 0px"><i class="bi bi-instagram"></i></a>
                                <a href="" style="padding-top: 0px"><i class="bi bi-linkedin"></i></a>
                            </div>
                        </div>
                        <div class="member-info">
                            <h4>أيات ابو قاسم</h4>
                            <span>System Analyst </span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
                    <div class="member">
                        <div class="member-img">
                            <img src="{{ asset('assets/assets/img/team/team-ahmed.jpg') }}" class="img-fluid" alt="">
                            <div class="social">
                                <a href="" style="padding-top: 0px"><i class="bi bi-twitter"></i></a>
                                <a href="" style="padding-top: 0px"><i class="bi bi-facebook"></i></a>
                                <a href="" style="padding-top: 0px"><i class="bi bi-instagram"></i></a>
                                <a href="" style="padding-top: 0px"><i class="bi bi-linkedin"></i></a>
                            </div>
                        </div>
                        <div class="member-info">
                            <h4>أحمد محمد فرحات</h4>
                            <span>Full Sstack Developer</span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="400">
                    <div class="member">
                        <div class="member-img">
                            <img src="{{ asset('assets/assets/img/team/team-osama.jpg') }}" class="img-fluid" alt="">
                            <div class="social">
                                <a href="" style="padding-top: 0px"><i class="bi bi-twitter"></i></a>
                                <a href="" style="padding-top: 0px"><i class="bi bi-facebook"></i></a>
                                <a href="" style="padding-top: 0px"><i class="bi bi-instagram"></i></a>
                                <a href="" style="padding-top: 0px"><i class="bi bi-linkedin"></i></a>
                            </div>
                        </div>
                        <div class="member-info">
                            <h4> أسامه ابو طواحينة</h4>
                            <span>Front End Developer</span>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section><!-- End Team Section -->
    <section id="testimonials" class="testimonials">
        <div class="container" data-aos="zoom-in">

            <div class="testimonials-slider swiper-container" data-aos="fade-up" data-aos-delay="100">
                <div class="swiper-wrapper">

                    <div class="swiper-slide">
                        <div class="testimonial-item">
                            <img src="{{ asset('assets/assets/img/testimonials/testimonials-1.jpg') }}"
                                class="testimonial-img" alt="">
                            <h3>Saul Goodman</h3>
                            <h4>Ceo &amp; Founder</h4>
                            <p>
                                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus.
                                Accusantium
                                quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                            </p>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-item">
                            <img src="{{ asset('assets/assets/img/testimonials/testimonials-2.jpg') }}"
                                class="testimonial-img" alt="">
                            <h3>Sara Wilsson</h3>
                            <h4>Designer</h4>
                            <p>
                                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum
                                eram malis
                                quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
                                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                            </p>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-item">
                            <img src="{{ asset('assets/assets/img/testimonials/testimonials-3.jpg') }}"
                                class="testimonial-img" alt="">
                            <h3>Jena Karlis</h3>
                            <h4>Store Owner</h4>
                            <p>
                                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis
                                minim
                                tempor labore quem eram duis noster aute amet eram fore quis sint minim.
                                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                            </p>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-item">
                            <img src="{{ asset('assets/assets/img/testimonials/testimonials-4.jpg') }}"
                                class="testimonial-img" alt="">
                            <h3>Matt Brandon</h3>
                            <h4>Freelancer</h4>
                            <p>
                                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim
                                velit
                                minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum
                                veniam.
                                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                            </p>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-item">
                            <img src="{{ asset('assets/assets/img/testimonials/testimonials-5.jpg') }}"
                                class="testimonial-img" alt="">
                            <h3>John Larson</h3>
                            <h4>Entrepreneur</h4>
                            <p>
                                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam
                                enim culpa
                                labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum
                                quid.
                                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                            </p>
                        </div>
                    </div><!-- End testimonial item -->

                </div>
                <div class="swiper-pagination"></div>
            </div>

        </div>
    </section><!-- End Testimonials Section -->

@endsection



@section('JS')


@endsection
