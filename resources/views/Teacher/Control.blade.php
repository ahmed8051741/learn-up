@extends('layout.parent')


@section('Title', 'لوحة القيادة')

@section('CSS')

    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

    <link href="{{ asset('assets/css/Star.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/profile.css') }}" rel="stylesheet">

    <style>
        * {
            font-family: 'cairo';
        }

        .badge-success {
            color: #fff;
            background-color: #28a745;
        }

        .badge-info {
            color: #fff;
            background-color: #1c3c94;
        }

        .badge {
            display: inline-block;
            padding: .25em .4em;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25rem;
        }

        .badge-warning {
            color: #fff;
            background-color: #ffc400de;
        }

        .badge-danger {
            color: #fff;
            background-color: #ff0000de;
        }

        .badge-info {
            color: #fff;
            background-color: #1c3c94;
        }

        .visitor {
            color: white;
            font-size: 15px;

        }

        #header {
            visibility: hidden;
            margin-top: -50px;
        }

        .custom-select {
            display: inline-block;
            width: 100%;
            height: calc(2.4375rem + 2px);
            padding: .375rem 1.75rem .375rem .75rem;
            line-height: 1.5;
            color: #495057;
            vertical-align: middle;
            background-size: 8px 10px;
            border: 1px solid rgba(0, 0, 0, .26);
            border-radius: .125rem;
            appearance: none;
        }

        .btn-success {
            color: #fff;
            background-color: #198754;
            border-color: #198754;
        }

        .btn-success:hover {
            color: #fff;
            background-color: #157347;
            border-color: #146c43;
        }

        .time {
            font-size: 9px !important
        }

        .socials i {
            margin-right: 14px;
            font-size: 17px;
            color: #d2c8c8;
            cursor: pointer
        }

        .feed-image img {
            width: 100%;
            height: auto
        }

        .bg-gradient-directional-primary {

            background-image: linear-gradient(45deg, #008385, #00E7EB);

            background-repeat: no-repeat;
            color: white
        }

        .bg-gradient-directional-danger {
            background-image: linear-gradient(45deg, #FF425C, #FFA8B4);
            background-repeat: no-repeat;
            color: white
        }

        .bg-gradient-directional-success {

            background-image: linear-gradient(45deg, #11A578, #32EAB2);
            background-repeat: no-repeat;
            color: white
        }

        .bg-gradient-directional-warning {

            background-image: linear-gradient(45deg, #fa722e, #ffbc9a);
            background-repeat: no-repeat;
            color: white
        }

        .bg-gradient-directional-info {

            background-image: linear-gradient(45deg, #cc4aff, #d943ff);
            background-repeat: no-repeat;
            color: white
        }

        .list-group-item.active {
            z-index: 2;
            color: #fff;
            background-color: #009688;
            border-color: #009688;
        }

        .list-group-item:first-child {
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }

        .list-group-item {
            position: relative;
            display: block;
        }

        .list-group-item {
            position: relative;
            display: block;
            padding: .75rem 1.25rem;
            margin-bottom: 0;
            background-color: inherit;
            border: 0 solid rgba(0, 0, 0, .125);
        }

        .list-group-item-action {
            /* width: 100%; */
            /* color: #495057; */
            text-align: inherit;
        }

        a {
            color: #106eea;
            text-decoration: none;
        }

        .card-body {
            flex: 1 1 auto;
            padding: 1.25rem;
        }

        div {
            display: block;
        }

        .card {
            border: 0;
            box-shadow: 0 2px 2px 0 rgb(0 0 0 / 14%), 0 3px 1px -2px rgb(0 0 0 / 20%), 0 1px 5px 0 rgb(0 0 0 / 12%);
            position: relative;
            display: flex;
            flex-direction: column;
            min-width: 0;

            border-radius: .125rem;
        }

        .cardForm {
            font-size: .875rem;
            font-weight: 400;
            position: relative;
            display: flex;
            flex-direction: column;
            min-width: 0;
            /* word-wrap: break-word; */
            background-color: #fff;
            background-clip: border-box;
            border: 1px solid rgba(0, 0, 0, .12);
            border-radius: .125rem;
        }

        .row {
            display: flex;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }

        @media (min-width: 768px) {
            .col-md-12 {
                flex: 0 0 100%;
                max-width: 100%;
            }

            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                font-family: "Al-Jazeera-Arabic";
            }

            .h4,
            h4 {
                font-size: 1.5rem;
            }

            .form-group {
                margin-bottom: 5px;
                margin-top: 5px
            }

            .h1,
            .h2,
            .h3,
            .h4,
            .h5,
            .h6,
            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                margin-bottom: .5rem;
                font-family: inherit;
                font-weight: 400;
                line-height: 1.2;
                color: inherit;
            }

            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                margin-top: 0;
                margin-bottom: .5rem;
            }

        }

        .card {
            margin-top: 12px
        }

        .form-group {
            margin-bottom: 1rem;

            h4 {
                display: block;
                margin-block-start: 1.33em;
                margin-block-end: 1.33em;
                margin-inline-start: 0px;
                margin-inline-end: 0px;

            }

            .dropdown-menu {
                text-align: right;
                right: 0px;
                min-width: 11rem;
            }

    </style>
@endsection
@section('con', 'display: none')


@section('dd')
    <h2 class="logo" style="font-family: Al-Jazeera-Arabic"><a href="{{ route('showHome') }}"
            style="font-family: Al-Jazeera-Arabic; color: aliceblue"> Learn <span
                style="font-family: Al-Jazeera-Arabic;font-weight: 760;color: #Fd4;"> up
            </span></a></h2>
@endsection


@section('content')


@section('Top')
    <section id="topbar">
        <div class="container d-flex justify-content-center justify-content-md-between" id="topb">
            <div class="contact-info d-flex align-items-center">
                <a href="mailto:ahmed8051741@gmail.com" class="visitor"> &nbsp; الدعم الفني عبر البريد الالكتروني </a>
            </div>
            <div class="d-flex flex-row align-items-center feed-text px-2" dir="ltr">
                &nbsp;&nbsp;
                <div class="d-flex flex-column flex-wrap ml-2"><span class="font-weight-bold">
                        <div class="dropdown">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" style="color: #ffffff;"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="rounded-circle" src="{{ asset('assets/img/my.jpeg') }}" width="44">
                                &nbsp;&nbsp;
                                {{ auth()->user()->name }} &nbsp;</a>

                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="{{ route('showprofile') }}">الملف الشخصي</a>
                                <hr style="margin-top:0.8px;margin-bottom:0.8px">

                                @if (auth()->user()->id == auth('teacher')->id())

                                    <a class="dropdown-item" href="{{ route('services.create') }}">اضافة خدمة جديد</a>

                                @else
                                    <a class="dropdown-item" href="{{ route('services.create') }}">اضافة طلب جديد</a>
                                @endif

                                <a class="dropdown-item" href="{{ route('control') }}">لوحة القيادة</a>
                                <a class="dropdown-item" href="{{ route('logout_log') }}">تسجيل الخروج</a>
                            </div>
                        </div>
                    </span>

                </div>
            </div>
        </div>
    </section>
@endsection

<div class="container bootstrap snippets bootdey">
    <div class="row">
        <div class="profile-nav col-md-3">
            <div class="panel">
                <div class="user-heading round">
                    <a href="#">
                        <img src="{{ asset('storage/image_profile/' . auth()->user()->image) }}" alt="">
                    </a>
                    <h1> {{ auth()->user()->name }} </h1>
                    <p>{{ auth()->user()->email }} </p>
                </div>

                <ul class="nav">
                    <li class="active"><a href="{{ route('control') }}"><i class="fas fa-tachometer-alt">
                                &nbsp;</i>
                            دفقة القيادة </a></li>
                    <li><a href="{{ route('showprofile') }}"><i class="fa fa-user"> &nbsp;</i> الملف الشخصي </a>
                    </li>

                    @if (auth()->user()->id == auth('teacher')->id())
                        <li><a href="{{ route('services.index') }}"><i class="fab fa-servicestack"> &nbsp;</i>
                                الخدمات
                            </a></li>

                        <li><a href="{{ route('rqserve_all') }}"><i class="fas fa-retweet"> &nbsp;</i>
                                الطلبات الواردة
                            </a></li>
                    @else
                        <li><a href="{{ route('order.index') }}"><i class="fab fa-servicestack"> &nbsp;</i>
                                الطلبات
                            </a></li>


                        <li><a href="{{ route('rqserve_allstd') }}"><i class="fas fa-retweet"> &nbsp;</i>
                                العروض المقدمه
                            </a></li>


                        <li><a href="{{ route('std_allserve') }}"><i class="fas fa-cart-arrow-down"> &nbsp;</i>
                                الخدمات المسجل بها
                            </a></li>

                    @endif
                    <li><a href="{{ route('boxing.create') }}"><i class="fas fa-box-open"> &nbsp;</i>
                            صندوق الاقتراحات
                        </a></li>
                    <li><a href="{{ route('password_edit') }}"><i class="fas fa-lock"> &nbsp;</i> تعديل كلمه
                            المرور</a></li>



                    <li><a href="{{ route('logout_log') }}"><i class="fas fa-sign-out-alt"> &nbsp;</i> تسجيل
                            الخروج</a></li>
                </ul>
            </div>
        </div>
        @if (auth()->user()->account_type == 'teacher')
            <div class="profile-info col-md-9">

                <div class="panel">

                    <div class="panel-body bio-graph-info">
                        <div class="container mt-4 mb-5">
                            <div class="d-flex justify-content-center row">

                                <div class="row">

                                    <div class="col-xl-4 col-sm-6 col-12">
                                        <div class="card bg-gradient-directional-primary">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <div class="media d-flex">
                                                        <div class="media-body white text-left" style=" width: 60%">
                                                            <h3>
                                                                <?php
                                                                $i = 0;
                                                                ?>
                                                                @foreach ($services as $service)
                                                                    @if (auth()->user()->id == $service->teacher_id)
                                                                        <?php
                                                                        $i = $i + 1;
                                                                        ?>
                                                                    @endif
                                                                @endforeach
                                                                {{ $i }}
                                                            </h3>

                                                            <span> الخدمات المضافة </span>
                                                        </div>
                                                        <div class="align-self-center col-xl-3 col-sm-6 col-12 "
                                                            style="padding-right:10%;width: 40%">
                                                            <i class="bx bx-credit-card "
                                                                style="font-size:50px;color:white; "></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-xl-4 col-sm-6 col-12">
                                        <div class="card bg-gradient-directional-info">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <div class="media d-flex">
                                                        <div class="media-body white text-left" style=" width: 60%">
                                                            <h3>
                                                                <?php
                                                                $i = 0;
                                                                ?>
                                                                @foreach ($Reses as $Rese)
                                                                    @if (auth()->user()->id == $Rese->teacher_id)
                                                                        <?php
                                                                        $i = $i + 1;
                                                                        ?>
                                                                    @endif
                                                                @endforeach
                                                                {{ $i }}
                                                            </h3>
                                                            <span> اجمالي الطلبات الواردة </span>
                                                        </div>
                                                        <div class="align-self-center col-xl-3 col-sm-6 col-12 "
                                                            style="padding-right:10%;width: 40%">
                                                            <i class="bx bxs-category"
                                                                style="font-size:50px;color:white; "></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-sm-6 col-12">
                                        <div class="card bg-gradient-directional-warning">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <div class="media d-flex">
                                                        <div class="media-body white text-left" style=" width: 60%">
                                                            <h3>
                                                                <?php
                                                                $i = 0;
                                                                ?>
                                                                @foreach ($Reses as $Rese)
                                                                    @if (auth()->user()->id == $Rese->teacher_id)
                                                                        @if ($Rese->Status == 0)
                                                                            <?php
                                                                            $i = $i + 1;
                                                                            ?>
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                                {{ $i }}
                                                            </h3>
                                                            <span style="font-size: 12px"> طلبات لم ترد عليها بعد
                                                            </span>
                                                        </div>
                                                        <div class="align-self-center col-xl-3 col-sm-6 col-12 "
                                                            style="padding-right:10%;width: 40%">
                                                            <i class="fas fa-exclamation-triangle"
                                                                style="font-size:50px;color:white; "></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-xl-4 col-sm-6 col-12">
                                        <div class="card bg-gradient-directional-warning">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <div class="media d-flex">
                                                        <div class="media-body white text-left" style=" width: 60%">
                                                            <h3>
                                                                <?php
                                                                $i = 0;
                                                                ?>
                                                                @foreach ($Reses as $Rese)
                                                                    @if (auth()->user()->id == $Rese->teacher_id)
                                                                        @if ($Rese->Status == 2)
                                                                            <?php
                                                                            $i = $i + 1;
                                                                            ?>
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                                {{ $i }}
                                                            </h3>
                                                            <span> طلبات معلقة </span>
                                                        </div>
                                                        <div class="align-self-center col-xl-3 col-sm-6 col-12 "
                                                            style="padding-right:10%;width: 40%">
                                                            <i class="far fa-pause-circle"
                                                                style="font-size:50px;color:white; "></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-sm-6 col-12">
                                        <div class="card bg-gradient-directional-success">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <div class="media d-flex">
                                                        <div class="media-body white text-left" style=" width: 60%">
                                                            <h3>
                                                                <?php
                                                                $i = 0;
                                                                ?>
                                                                @foreach ($Reses as $Rese)
                                                                    @if (auth()->user()->id == $Rese->teacher_id)
                                                                        @if ($Rese->Status == 1)
                                                                            <?php
                                                                            $i = $i + 1;
                                                                            ?>
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                                {{ $i }}
                                                            </h3> <span> طلبات مقبوله </span>
                                                        </div>
                                                        <div class="align-self-center col-xl-3 col-sm-6 col-12 "
                                                            style="padding-right:10%;width: 40%">
                                                            <i class="bx bx-message-square-check"
                                                                style="font-size:50px;color:white; "></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-sm-6 col-12">
                                        <div class="card bg-gradient-directional-danger">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <div class="media d-flex">
                                                        <div class="media-body white text-left" style=" width: 60%">
                                                            <h3>
                                                                <?php
                                                                $i = 0;
                                                                ?>
                                                                @foreach ($Reses as $Rese)
                                                                    @if (auth()->user()->id == $Rese->teacher_id)

                                                                        @if ($Rese->Status == 3)
                                                                            <?php
                                                                            $i = $i + 1;
                                                                            ?>
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                                {{ $i }}
                                                            </h3>
                                                            <span> طلبات مرفوضة </span>
                                                        </div>
                                                        <div class="align-self-center col-xl-3 col-sm-6 col-12 "
                                                            style="padding-right:10%;width: 40%">
                                                            <i class="bx bx-message-square-x"
                                                                style="font-size:50px;color:white; "></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Left col -->
                                    <div class="col-md-12" style="margin-top: 10px">
                                        <!-- TABLE: LATEST ORDERS -->
                                        <div class="card">
                                            <div class="card-header border-transparent">
                                                <h3 class="card-title"
                                                    style="float: right;font-size: 25px;padding-top: 2px"> اخر الطلبات
                                                    الورادة </h3>

                                                <div class="card-tools" dir="ltr">
                                                    <button type="button" class="btn btn-tool"
                                                        data-card-widget="remove">
                                                        <i class="fas fa-times"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-tool"
                                                        data-card-widget="collapse">
                                                        <i class="fas fa-minus"></i>
                                                    </button>

                                                </div>
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body p-0">
                                                <div class="table-responsive">
                                                    <table class="table m-0" style="text-align: center">
                                                        <thead>
                                                            <tr>
                                                                <th> اسم المتقدم للخدمه </th>
                                                                <th> التاريخ المناسب للتواصل معه </th>
                                                                <th> الساعة المناسبة للتواصل </th>
                                                                <th> الخدمه المتقدم لها </th>
                                                                <th> وقت الطلب </th>
                                                                <th> حالة الخدمه </th>
                                                                <th> اجراءات </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            @foreach ($Reserves as $Rqserv)

                                                                @if ($Rqserv->subReserve->id == auth('teacher')->user()->id)
                                                                    <tr>
                                                                        <td>
                                                                            @if ($Rqserv->account_type == 'teacher')
                                                                                {{ $Rqserv->subReserve->name }}
                                                                            @elseif ($Rqserv->account_type ==
                                                                                'student')
                                                                                {{ $Rqserv->Stud->name }}
                                                                            @endif


                                                                        </td>
                                                                        <td> {{ $Rqserv->date }}</td>
                                                                        <td>{{ $Rqserv->time }}</td>
                                                                        <td>
                                                                            @if ($Rqserv->Service->title != null)
                                                                                {{ $Rqserv->Service->title }}
                                                                            @else
                                                                                لقد تم ايقاف هذه الخدمه
                                                                            @endif


                                                                        </td>

                                                                        <td>{{ $Rqserv->created_at->diffForHumans() }}
                                                                        </td>
                                                                        <td>
                                                                            @if ($Rqserv->Status == 0)

                                                                                <span class="badge badge-info"
                                                                                    style="font-size: 13px"> لم يتم
                                                                                    اتخاذ اي
                                                                                    اجراء </span>
                                                                            @elseif ($Rqserv->Status ==1)
                                                                                <span class="badge badge-success"
                                                                                    style="font-size: 13px"> تم القبول
                                                                                </span>
                                                                            @elseif ($Rqserv->Status ==2)
                                                                                <span class="badge badge-warning"
                                                                                    style="font-size: 13px"> تم التعليق
                                                                                </span>
                                                                            @elseif ($Rqserv->Status ==3)
                                                                                <span class="badge badge-danger"
                                                                                    style="font-size: 13px"> تم الرفض
                                                                                </span>

                                                                            @endif

                                                                        </td>

                                                                        <td>
                                                                            <div class="btn-group">
                                                                                <a href="{{ route('show.Accept', $Rqserv->id) }} "
                                                                                    class="btn btn-primary pro1"
                                                                                    style="border-radius: 5px">
                                                                                    <i class="fas fa-edit"
                                                                                        style="font-size: 22px; "></i>
                                                                                </a>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @endif

                                                            @endforeach


                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!-- /.table-responsive -->
                                            </div>
                                            <!-- /.card-body -->
                                            <div class="card-footer clearfix">
                                                <a href="{{ route('rqserve_all') }}"
                                                    class="btn btn-sm btn-secondary float-right">عرض كل الطلبات الواردة
                                                </a>
                                            </div>
                                            <!-- /.card-footer -->
                                        </div>
                                        <!-- /.card -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </div>

                        </div>
                    </div>




                    {{-- -----------------------------------------------------------لوحة تحكم الطالب-------------------------------------------------------------------------- --}}








                @else
                    <div class="profile-info col-md-9">

                        <div class="panel">

                            <div class="panel-body bio-graph-info">
                                <div class="container mt-4 mb-5">
                                    <div class="d-flex justify-content-center row">

                                        <div class="row">

                                            <div class="col-xl-6 col-sm-8 col-12">
                                                <div class="card bg-gradient-directional-primary">
                                                    <div class="card-content">
                                                        <div class="card-body">
                                                            <div class="media d-flex">
                                                                <div class="media-body white text-left"
                                                                    style=" width: 60%">
                                                                    <h3>
                                                                        <?php
                                                                        $i = 0;
                                                                        ?>
                                                                        @foreach ($orders as $order)
                                                                            @if (auth()->user()->id == $order->student_id)
                                                                                <?php
                                                                                $i = $i + 1;
                                                                                ?>
                                                                            @endif
                                                                        @endforeach
                                                                        {{ $i }}
                                                                    </h3>

                                                                    <span> الطلبات المضافة </span>
                                                                </div>
                                                                <div class="align-self-center col-xl-3 col-sm-6 col-12 "
                                                                    style="padding-right:15%;width: 30%">
                                                                    <i class="bx bx-credit-card "
                                                                        style="font-size:50px;color:white; "></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-xl-6 col-sm-8 col-12">
                                                <div class="card bg-gradient-directional-info">
                                                    <div class="card-content">
                                                        <div class="card-body">
                                                            <div class="media d-flex">
                                                                <div class="media-body white text-left"
                                                                    style=" width: 60%">
                                                                    <h3>
                                                                        <?php
                                                                        $i = 0;
                                                                        ?>
                                                                        @foreach ($Rqorders as $Rqorder)
                                                                            @if (auth()->user()->id == $Rqorder->student_id)
                                                                                <?php
                                                                                $i = $i + 1;
                                                                                ?>
                                                                            @endif
                                                                        @endforeach
                                                                        {{ $i }}
                                                                    </h3>
                                                                    <span>اجمالي المتقدمين لطلباتك </span>
                                                                </div>
                                                                <div class="align-self-center col-xl-3 col-sm-6 col-12 "
                                                                    style="padding-right:15%;width: 30%">
                                                                    <i class="bx bxs-category"
                                                                        style="font-size:50px;color:white; "></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>



                                            <!-- Left col -->
                                            <div class="col-md-12" style="margin-top: 10px">
                                                <!-- TABLE: LATEST ORDERS -->
                                                <div class="card">
                                                    <div class="card-header border-transparent">
                                                        <h3 class="card-title"
                                                            style="float: right;font-size: 25px;padding-top: 2px"> اخر
                                                            المتقدمين لطلبك </h3>

                                                        <div class="card-tools" dir="ltr">
                                                            <button type="button" class="btn btn-tool"
                                                                data-card-widget="remove">
                                                                <i class="fas fa-times"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-tool"
                                                                data-card-widget="collapse">
                                                                <i class="fas fa-minus"></i>
                                                            </button>

                                                        </div>
                                                    </div>
                                                    <!-- /.card-header -->
                                                    <div class="card-body p-0">
                                                        <div class="table-responsive">
                                                            <table class="table m-0" style="text-align: center">
                                                                <thead>
                                                                    <tr>
                                                                        <th> اسم المتقدم للخدمه </th>
                                                                        <th> التاريخ المناسب للتواصل معه </th>
                                                                        <th> الساعة المناسبة للتواصل </th>
                                                                        <th> الطلب المتلقى عليه العرض</th>
                                                                        <th> وقت الطلب </th>
                                                                        <th> اجراءات </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                    $i = 0;
                                                                    ?> @foreach ($Rqorders as $Rqorde)

                                                                        @if ($Rqorde->Studs->id == auth('student')->user()->id)
                                                                            <?php
                                                                            $i = $i + 1;
                                                                            ?>
                                                                            @if ($i < 5)

                                                                                <tr>
                                                                                    <td> <a
                                                                                            href="{{ route('show.teacher', $Rqorde->Teachers->id) }}">
                                                                                            {{ $Rqorde->Teachers->name }}</a>
                                                                                    </td>
                                                                                    <td> {{ $Rqorde->date }}</td>
                                                                                    <td>{{ $Rqorde->time }}</td>
                                                                                    <td>{{ $Rqorde->order->title }}
                                                                                    </td>
                                                                                    <td>{{ $Rqorde->created_at->diffForHumans() }}
                                                                                    </td>


                                                                                    <td>
                                                                                        <div class="btn-group">
                                                                                            <a href="https://wa.me/{{ $Rqorde->Teachers->s_mobile }}{{ $Rqorde->Teachers->mobile }}"
                                                                                                class="btn btn-primary pro1"
                                                                                                style="border-radius:
                                                                                             5px">مراسلة


                                                                                            </a>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            @endif
                                                                        @endif

                                                                    @endforeach


                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- /.table-responsive -->
                                                    </div>
                                                    <!-- /.card-body -->
                                                    <div class="card-footer clearfix">
                                                        <a href="{{ route('rqserve_allstd') }}"
                                                            class="btn btn-sm btn-secondary float-right">عرض كل
                                                            العروض المتلقاة </a>
                                                    </div>
                                                    <!-- /.card-footer -->
                                                </div>
                                                <!-- /.card -->
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                    </div>

                                </div>
                            </div>
        @endif

    </div>
</div>


<br>
<br>
<br>
<div style="padding-top: 100px">
</div>

@endsection


@section('JS')
<script>
    function update(id) {
        axios.put('/profile/profile_setting/' + id, {
                name: document.getElementById('name').value,
                email: document.getElementById('email').value,
                mobile: document.getElementById('mobile').value,
                select: document.getElementById('select').value,
                facebook: document.getElementById('facebook').value,
                instagram: document.getElementById('instagram').value,
                youtube: document.getElementById('youtube').value,
                describe: document.getElementById('describe').value,
            })
            .then(function(response) {
                // handle success
                console.log(response);
                toastr.success(response.data.message);
                window.location.href = "/profile";
            })
            .catch(function(error) {
                // handle error
                console.log(error);
                toastr.error(error.response.data.message);
            })
            .then(function() {
                // always executed
            });
    }
</script>



<script src="{{ asset('cms/plugins/raphael/raphael.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('cms/plugins/chart.js/Chart.min.js') }}"></script>


<!-- jQuery -->
<script src="{{ asset('CMS/plugins/jquery/jquery.min.js') }}"></script>

<script src="{{ asset('CMS/dist/js/adminlte.min.js') }}"></script>

@endsection
