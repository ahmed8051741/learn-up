@extends('admins.parent')
@section('title', 'Edite Page')
@section('bige-title', 'Edite Page')
@section('main-Page', 'Home')
@section('sub-Page', 'Edite_Page')
@section('CSS')
<link rel="stylesheet" href="{{ asset('CMS/plugins/summernote/summernote-bs4.min.css') }}">

@endsection


@section('content')
    <!-- Main content -->
    <section class="content">
        <form id="create-form">
            <div class="col-md-12">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">Edite page</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="form-group">
                    <label for="name">Name Page</label>
                    <input class="form-control" placeholder="Name Page:" id="name" value="{{$Page->name}}">
                     <input class="form-control" value="{{auth('admin')->user()->name}}" hidden id="name_admin">
                 </div>
<hr>
                <div class="form-group">
                    <label for="name">Add your page content</label>
                    <textarea id="compose_textarea" class="form-control" style="height: 300px">

                        {{$Page->compose_textarea}}


                    </textarea>
                </div>
               <!-- /.card-body -->
               <div class="form-group">
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="active"

                    @if ($Page->Active) checked @endif>
                    <label class="custom-control-label" for="active">Active</label>
                </div>
            </div>
              <div class="card-footer col-md-12">
                <div class="float-right col-md-12">

                    <button type="button" onclick="Update()" class="btn btn-primary col-md-12"> Update</button>
                 </div>

               </div>

              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
        </form>

    </section>





    @endsection


 @section('JS')
<script src="{{ asset('CMS/plugins/summernote/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('CMS/dist/js/demo.js') }}"></script>


<script>
  $(function () {
    //Add text editor
    $('#compose_textarea').summernote()
  })
</script>
 <script>
    function Update(id){
        axios.put('/admin/pages/'+id,{
            name: document.getElementById('name').value,
            name_admin: document.getElementById('name_admin').value,
            name_admin: document.getElementById('name_admin').value,
            active: document.getElementById('active').checked,
            compose_textarea : document.getElementById('compose_textarea').value,
         })
        .then(function (response) {
            // handle success
            console.log(response);
            toastr.success(response.data.message);
            // document.getElementById('create-form').reset(); // تصفير قيمة الفورم سبمت
            window.location.href="/admin/pages"; // التحويل والانتقال
        })
        .catch(function (error) {
            // handle error
            console.log(error);
            toastr.error(error.response.data.message);
        })
        .then(function () {
            // always executed
        });
    }
</script>

@endsection


