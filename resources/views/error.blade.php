@extends('layout.parent')


@section('Title', 'طلب التخويل')
@section('CSS')
    <style type="text/css">
        * {
            margin: 0px auto;
            padding: 0px;
            text-align: center;
        }

        body {
            background-color: #D4D9ED;
        }

        .cont_principal {
            position: absolute;
            width: 100%;
            height: 100%;
            overflow: hidden;
        }

        .cont_error {
            position: absolute;
            width: 100%;
            height: 300px;
            top: 50%;
            margin-top: -150px;
        }

        .cont_error>h1 {
            font-family: 'Lato', sans-serif;
            font-weight: 400;
            font-size: 150px;
            color: #fff;
            position: relative;
            left: -100%;
            transition: all 0.5s;
        }


        .cont_error>p {
            font-weight: 400;
            font-size: 24px;
            letter-spacing: 0.8px;
            color: #9294AE;
            position: relative;
            left: 100%;
            transition: all 0.5s;
            transition-delay: 0.5s;
            -webkit-transition: all 0.5s;
            -webkit-transition-delay: 0.5s;
        }

        .cont_aura_1 {
            position: absolute;
            width: 300px;
            height: 120%;
            top: 25px;
            right: -340px;
            background-color: #8A65DF;
            box-shadow: 0px 0px 60px 20px rgba(137, 100, 222, 0.5);
            -webkit-transition: all 0.5s;
            transition: all 0.5s;
        }

        .cont_aura_2 {
            position: absolute;
            width: 100%;
            height: 300px;
            right: -10%;
            bottom: -301px;
            background-color: #8B65E4;
            box-shadow: 0px 0px 60px 10px rgba(131, 95, 214, 0.5), 0px 0px 20px 0px rgba(0, 0, 0, 0.1);
            z-index: 5;
            transition: all 0.5s;
            -webkit-transition: all 0.5s;
        }

        .cont_error_active>.cont_error>h1 {
            left: 0%;
        }

        .cont_error_active>.cont_error>p {
            left: 0%;
        }

        .cont_error_active>.cont_aura_2 {
            animation-name: animation_error_2;
            animation-duration: 4s;
            animation-timing-function: linear;
            animation-iteration-count: infinite;
            animation-direction: alternate;
            transform: rotate(-20deg);
        }

        .cont_error_active>.cont_aura_1 {
            transform: rotate(20deg);
            right: -170px;
            animation-name: animation_error_1;
            animation-duration: 4s;
            animation-timing-function: linear;
            animation-iteration-count: infinite;
            animation-direction: alternate;
        }

        @-webkit-keyframes animation_error_1 {
            from {
                -webkit-transform: rotate(20deg);
                transform: rotate(20deg);
            }

            to {
                -webkit-transform: rotate(25deg);
                transform: rotate(25deg);
            }
        }

        @-o-keyframes animation_error_1 {
            from {
                -webkit-transform: rotate(20deg);
                transform: rotate(20deg);
            }

            to {
                -webkit-transform: rotate(25deg);
                transform: rotate(25deg);
            }

        }

        @-moz-keyframes animation_error_1 {
            from {
                -webkit-transform: rotate(20deg);
                transform: rotate(20deg);
            }

            to {
                -webkit-transform: rotate(25deg);
                transform: rotate(25deg);
            }

        }

        @keyframes animation_error_1 {
            from {
                -webkit-transform: rotate(20deg);
                transform: rotate(20deg);
            }

            to {
                -webkit-transform: rotate(25deg);
                transform: rotate(25deg);
            }
        }




        @-webkit-keyframes animation_error_2 {
            from {
                -webkit-transform: rotate(-15deg);
                transform: rotate(-15deg);
            }

            to {
                -webkit-transform: rotate(-20deg);
                transform: rotate(-20deg);
            }
        }

        @-o-keyframes animation_error_2 {
            from {
                -webkit-transform: rotate(-15deg);
                transform: rotate(-15deg);
            }

            to {
                -webkit-transform: rotate(-20deg);
                transform: rotate(-20deg);
            }
        }
        }

        @-moz-keyframes animation_error_2 {
            from {
                -webkit-transform: rotate(-15deg);
                transform: rotate(-15deg);
            }

            to {
                -webkit-transform: rotate(-20deg);
                transform: rotate(-20deg);
            }
        }

        @keyframes animation_error_2 {
            from {
                -webkit-transform: rotate(-15deg);
                transform: rotate(-15deg);
            }

            to {
                -webkit-transform: rotate(-20deg);
                transform: rotate(-20deg);
            }
        }

        #footer {
            display: none;
        }

        #topbar {
            display: none;

        }

        #header {
            display: none;
            visibility: hidden;
        }

    </style>
@endsection
@section('content')

    <div class="cont_principal">
        <div class="cont_error">
            <h1 style="font-family: cairo;font-size: 55px;color: #000;"> عذراً ... <i class="far fa-hand-paper"></i></h1>
            <br>
            <p>للقيام بهذا الأجراء عليك اولاً
                <a href="#" type="button" onclick="go()"> تسجيل الدخول
                </a>
                او
                <a href="#" type="button" onclick="govs()"> انشاء حساب
                </a>


            </p>

            <h1 class="logo" style="font-family: Al-Jazeera-Arabic">
                <p style="font-family: Al-Jazeera-Arabic;color: rgb(0, 0, 0)">
                    Learn
                    <span style="font-family: Al-Jazeera-Arabic;font-weight: 760;color: rgb(0, 4, 255)"> up
                    </span>
                </p>
            </h1>
        </div>

        <div class="cont_aura_1"></div>
        <div class="cont_aura_2"></div>
    </div>

@endsection
@section('JS')
    <script>
        window.onload = function() {
            document.querySelector('.cont_principal').className = "cont_principal cont_error_active";

        }
    </script>
    <script>
        function go() {
            Swal.fire({
                title: 'كيف تود تسجيل الدخول ',
                showDenyButton: true,
                showCancelButton: false,
                confirmButtonColor: '#155b92',
                denyButtonColor: '#0f5a69',
                confirmButtonText: `حساب معلم`,
                denyButtonText: `حساب طالب`,
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    window.location.href = "/teacher/login"; // التحويل والانتقال
                } else if (result.isDenied) {
                    window.location.href = "/student/login"; // التحويل والانتقال
                }
            })
        }
    </script>
    <script>
        function govs() {
            Swal.fire({
                title: 'اختـر نوع الحساب الذي تود انشاؤه',
                showDenyButton: true,
                showCancelButton: false,
                confirmButtonColor: '#155b92',
                denyButtonColor: '#0f5a69',
                confirmButtonText: `حساب معلم`,
                denyButtonText: `حساب طالب`,
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    window.location.href = "/register/teacher/create"; // التحويل والانتقال
                } else if (result.isDenied) {
                    window.location.href = "/register/student/create"; // التحويل والانتقال
                }
            })
        }
    </script>
@endsection
