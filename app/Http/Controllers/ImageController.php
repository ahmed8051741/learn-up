<?php

namespace App\Http\Controllers;

use App\Models\category;
use App\Models\City;
use App\Models\Image;
use App\Models\Notice;
use App\Models\Rqserve;
use App\Models\Service;
use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')->get();
        $categorise = category::all();
        $cities = City::all();
        $Teacher = Teacher::all();
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $images = Image::with('subimages')->with('student')->orderBy('id', 'DESC')->get();
        return response()->view('Teacher.auth.change-image', ['images' => $images, 'services' => $services, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves]);
    }

    /**d
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $validator = Validator(
            $request->all(),
            [
                'profle_image' => 'required|image',
                'student_id' => '',
                'teacher_id' => '',

            ],
            [
                'profle_image.required' => 'اضف صورة من فضلك',
                'profle_image.image' => 'يسمح فقط بالصور',

            ]
        );

        if (!$validator->fails()) {
            $profle_image = new Image();
            $profle_image->student_id = $request->get('student_id');
            $profle_image->teacher_id = $request->get('teacher_id');

            $image = $request->file('profle_image');

            $imageName = time() . '_profle_image.' . $image->getClientOriginalExtension();
            // $image->move(public_path('images'), $imageName);
            $image->storeAs('image_profile', $imageName, ['disk' => 'public']);
            $profle_image->name = $imageName;

            // $isSaved = $service->save();
            $isSaved = $profle_image->save();
            return response()->json([
                'message' => $isSaved ? "تم تحديث صورتك بنجاح" : "حدث خطاء غير معروف .. الرجاء التواصل مع الدعم الفني"
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            //VALIDATION FAILED
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image, Request $request)
    {
        // $flight = Flight::find(1);

        // $flight->delete();
        $isDelete = $image->delete();
        if ($isDelete) {

            $validator = Validator(
                $request->all(),
                [
                    'profle_image' => 'required|image',
                    'student_id' => '',
                    'teacher_id' => '',

                ],
                [
                    'profle_image.required' => 'اضف صورة من فضلك',
                    'profle_image.image' => 'يسمح فقط بالصور',

                ]
            );

            if (!$validator->fails()) {
                $profle_image = new Image();
                $profle_image->student_id = $request->get('student_id');
                $profle_image->teacher_id = $request->get('teacher_id');

                $image = $request->file('profle_image');

                $imageName = time() . '_profle_image.' . $image->getClientOriginalExtension();
                // $image->move(public_path('images'), $imageName);
                $image->storeAs('image_profile', $imageName, ['disk' => 'public']);
                $profle_image->name = $imageName;

                // $isSaved = $service->save();
                $isSaved = $profle_image->save();
                return response()->json([
                    'message' => $isSaved ? "تم تحديث صورتك بنجاح" : "حدث خطاء غير معروف .. الرجاء التواصل مع الدعم الفني"
                ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
            } else {
                //VALIDATION FAILED
                return response()->json([
                    'message' => $validator->getMessageBag()->first()
                ], Response::HTTP_BAD_REQUEST);
            }
        } else {

            $validator = Validator(
                $request->all(),
                [
                    'profle_image' => 'required|image',
                    'student_id' => '',
                    'teacher_id' => '',

                ],
                [
                    'profle_image.required' => 'اضف صورة من فضلك',
                    'profle_image.image' => 'يسمح فقط بالصور',

                ]
            );

            if (!$validator->fails()) {
                $profle_image = new Image();
                $profle_image->student_id = $request->get('student_id');
                $profle_image->teacher_id = $request->get('teacher_id');

                $image = $request->file('profle_image');

                $imageName = time() . '_profle_image.' . $image->getClientOriginalExtension();
                // $image->move(public_path('images'), $imageName);
                $image->storeAs('image_profile', $imageName, ['disk' => 'public']);
                $profle_image->name = $imageName;

                // $isSaved = $service->save();
                $isSaved = $profle_image->save();
                return response()->json([
                    'message' => $isSaved ? "تم تحديث صورتك بنجاح" : "حدث خطاء غير معروف .. الرجاء التواصل مع الدعم الفني"
                ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
            } else {
                //VALIDATION FAILED
                return response()->json([
                    'message' => $validator->getMessageBag()->first()
                ], Response::HTTP_BAD_REQUEST);
            }
        }
    }
}
