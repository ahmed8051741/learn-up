<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notices', function (Blueprint $table) {
            $table->id();
            // حاسب المستهدف النوع
            //   $table->string('gurde');
            // حالة الطلب
            $table->string('Status')->nullable();
            //اسم المستهدف
            // $table->string('recipient')->nullable();
            // عنوان الاشعار
            $table->string('title')->nullable();
            // مرسل الاشعار
            $table->string('send')->nullable();
            $table->string('send_id')->nullable();
            $table->string('send_name')->nullable();
            $table->string('req_send')->nullable();
            $table->string('req_send_name')->nullable();
            $table->string('req_send_type')->nullable();
            $table->string('serves')->nullable();
            $table->foreignId('Rqserve_id')->nullable();
            $table->foreign('Rqserve_id')->on('rqserves')->references('id');

            $table->foreignId('Rqorder_id')->nullable();
            $table->foreign('Rqorder_id')->on('rqserves')->references('id');

            $table->timestamps();
            $table->string('read')->default('0');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notices');
    }
}
