@extends('layout.parent')
@section('CSS')

    <link href="{{ asset('assets/css/Star.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/profile.css') }}" rel="stylesheet">
    <style>
        .custom-file {
            position: relative;
            display: inline-block;
            width: 100%;
            height: calc(2.25rem + 2px);
            margin-bottom: 0;
        }

        .custom-file-input {
            position: relative;
            z-index: 2;
            width: 100%;
            height: calc(2.25rem + 2px);
            margin: 0;
            opacity: 0;
        }

        .custom-file-label {
            position: absolute;
            font-family: 'cairo';
            top: 0;
            left: 0;
            right: 0;
            z-index: 1;
            height: calc(2.25rem + 2px);
            padding: .375rem .75rem;
            font-weight: 400;
            line-height: 1.5;
            color: #495057;
            background-color: rgb(235, 235, 235);
            border: 1px solid #ced4da;
            border-radius: .25rem;
        }

        #header {
            visibility: hidden;

        }

    </style>

@endsection

@section('con', 'display: none')

    <section id="topbar2" id="asd">
        <div class="container d-flex justify-content-center justify-content-md-between" id="topb">
            <div class="contact-info d-flex align-items-center">
                <h2 class="logo" style="font-family: Al-Jazeera-Arabic"><a href="index.html"
                        style="font-family: Al-Jazeera-Arabic; color: aliceblue"> Learn <span
                            style="font-family: Al-Jazeera-Arabic;font-weight: 760;color: #Fd4;"> up
                        </span></a></h2>

            </div>
            <div class="d-flex flex-row align-items-center feed-text px-2" dir="ltr">
                &nbsp;&nbsp;
                <div class="d-flex flex-column flex-wrap ml-2"><span class="font-weight-bold">
                        <div class="dropdown">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" style="color: #ffffff;"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="rounded-circle" src="{{ asset('assets/img/my.jpeg') }}" width="44">
                                &nbsp;&nbsp;
                                {{ auth()->user()->name }} &nbsp;</a>

                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="{{ route('showprofile') }}">الملف الشخصي</a>
                                <hr style="margin-top:0.8px;margin-bottom:0.8px">

                                @if (auth()->user()->id == auth('teacher')->id())

                                    <a class="dropdown-item" href="{{ route('services.create') }}">اضافة خدمة
                                        جديد</a>

                                @else
                                    <a class="dropdown-item" href="{{ route('order.create') }}">اضافة طلب
                                        جديد</a>
                                @endif

                                <a class="dropdown-item" href="{{ route('control') }}">لوحة القيادة</a>
                                <a class="dropdown-item" href="{{ route('logout_log') }}">تسجيل الخروج</a>
                            </div>
                            &nbsp;
                            <div class="notification dropdown">
                                <a id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                    <span class="badge">
                                        <?php $i = 0; ?>
                                        @foreach ($Reserves as $Reserve)
                                            @if (auth()->user()->id == $Reserve->teacher_id)
                                                @if (auth()->user()->account_type == 'teacher')
                                                    @if ($Reserve->reade != 1)
                                                        <?php
                                                        $i++;
                                                        ?>
                                                    @endif
                                                @endif
                                            @endif
                                        @endforeach

                                        @foreach ($Notices as $Notice)
                                            @if (auth()->user()->id == $Notice->req_send)
                                                @if (auth()->user()->account_type == $Notice->req_send_type)
                                                    @if ($Notice->read != 1)
                                                        <?php $i++; ?>
                                                    @endif
                                                @endif
                                            @endif
                                        @endforeach
                                        {{ $i }}

                                    </span>
                                    <i class="fas fa-bell"></i>

                                </a>
                                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="new"
                                    style="padding-top:20px">
                                    <a href="#" style="padding: 8px;font-size: 12px;padding-bottom: 8px"> تحديد
                                        الكل كمقروء </a>

                                    @foreach ($Reserves as $Reserve)
                                        @if (auth()->user()->id == $Reserve->teacher_id)
                                            @if (auth()->user()->account_type == 'teacher')
                                                <!-- Message Start -->

                                                <a href="{{ route('show.CertainPro', $Reserve->service_id) }}"
                                                    class="dropdown-item" style="max-width: 100%;white-space:unset;">
                                                    <div class="media">
                                                        <div class="media-body">
                                                            <p class="text-sm" style="font-size: 13px;padding-top:5px"
                                                                dir="rtl">
                                                                @if ($Reserve->account_type == 'teacher')
                                                                    تقدم المعلم/ة
                                                                    {{ $Reserve->subReserve->name }}
                                                                    <br>
                                                                    لخدمه {{ $Reserve->Service->title }} التي
                                                                    تثدمها
                                                </a>
                                            @endif
                                            @if ($Reserve->account_type == 'student')

                                                تقدم الطالب/ة {{ $Reserve->Stud->name }}
                                                <br>
                                                لخدمه {{ $Reserve->Service->title }} التي
                                                تقدمها

                                            @endif
                                            <p style="margin-top: -5px ; float: left;font-size: 10px"> <i
                                                    class="far fa-clock mr-1"></i>
                                                {{ $Reserve->created_at->diffForHumans() }}
                                            </p>
                                            </p>
                                            </a>


                                </div>
                            </div>
                            <!-- Message End -->
                            </a>
                            @endif

                            @endif

                            @endforeach

                            @foreach ($Notices as $Notice)
                                @if (auth()->user()->id == $Notice->req_send)
                                    @if (auth()->user()->account_type == $Notice->req_send_type)
                                        <!-- Message Start -->
                                        <a href="{{ route('show.CertainProstd', $Notice->id) }}" class="dropdown-item"
                                            style="max-width: 100%;white-space:unset;">
                                            <div class="media">
                                                <div class="media-body">
                                                    <p class="text-sm" style="font-size: 13px;padding-top:5px" dir="rtl">
                                                        @if ($Notice->send == 'teacher')
                                                            @if ($Notice->title == null)
                                                                @if ($Notice->Status == 1)
                                                                    قام المعلم {{ $Notice->send_name }}
                                                                    : بقبول طلبك في خدمه
                                                                    {{ $Notice->serves }}
                                                                @endif
                                                                @if ($Notice->Status == 2)
                                                                    قام المعلم {{ $Notice->send_name }}
                                                                    : بتعليق طلبك في خدمه
                                                                    {{ $Notice->serves }}
                                                                @endif
                                                                @if ($Notice->Status == 3)
                                                                    قام المعلم {{ $Notice->send_name }}
                                                                    : برفض طلبك في خدمه
                                                                    {{ $Notice->serves }}
                                                                @endif
                                                                @if ($Notice->Status == null)
                                                                    قام المعلم {{ $Notice->send_name }}
                                                                    : بتقديم عرض على خدمتك التي بعنوان
                                                                    {{ $Notice->serves }}
                                                                @endif


                                                            @else
                                                                من خلال : {{ $Notice->send_name }}

                                                                {{ $Notice->title }}
                                                            @endif
                                                        @endif


                                                    </p>

                                                    <p style="margin-top: -5px ; float: left;font-size: 10px"> <i
                                                            class="far fa-clock mr-1"></i>
                                                        {{ $Notice->created_at->diffForHumans() }}
                                                    </p>
                                                </div>
                                            </div>
                                            <!-- Message End -->
                                        </a>
                                    @endif

                                @endif

                            @endforeach
                            <hr>
                            <div style=" text-align: center">
                                <a href="" style="font-size: 13px ;"> عرض جميع الاشعارات
                                </a>
                            </div>
                        </div>



                </div>
                </span>

            </div>
        </div>
        </div>
    </section>



@section('content')
    <div class="container bootstrap snippets bootdey">
        <div class="row">

            <div class="profile-nav col-md-3">
                <div class="panel">
                    <div class="user-heading round">
                        <a href="#">
                            <img src="{{ asset('assets/img/users/images/user.png') }}" alt="">
                        </a>
                        <h1> {{ auth()->user()->name }} </h1>
                        <p>{{ auth()->user()->email }} </p>
                    </div>

                    <ul class="nav">
                        <li><a href="{{ route('showprofile') }}"><i class="fas fa-tachometer-alt"> &nbsp;</i> دفقة
                                القيادة </a></li>
                        <li><a href="{{ route('showprofile') }}"><i class="fa fa-user"> &nbsp;</i> الملف الشخصي </a></li>
                        <li class="active">
                            <a href="{{ route('services.index') }}"><i class="fas fa-indent"> &nbsp;</i>
                                الخدمات
                            </a>

                        </li>

                        <li><a href="{{ route('password_edit') }}"><i class="fas fa-lock"> &nbsp;</i> تعديل كلمه
                                المرور</a></li>
                        <li><a href="{{ route('control') }}"><i class="fa fa-calendar"> &nbsp;</i> العودة الى لوحة التحكم
                            </a></li>
                        <li><a href="{{ route('logout_log') }}"><i class="fas fa-sign-out-alt"> &nbsp;</i> تسجيل
                                الخروج</a></li>
                    </ul>
                </div>
            </div>

            @yield('content_cont')
        @endsection
