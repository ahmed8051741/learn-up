<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AuthTeacherController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\RqserveController;
use App\Http\Controllers\NoticeController;
use App\Http\Controllers\RqorderController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\StarController;
use App\Http\Controllers\SuggestionController;
use App\Http\Controllers\VisitorController;
use App\Models\Star;

/*

|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/pro')->middleware('guest:admin')->group(function () {
    Route::get('{guard}/login', [AuthController::class, 'showLogin'])->name('loginadmin');
    Route::post('/login', [AuthController::class, 'login']);
});

Route::prefix('admin/')->middleware('auth:admin')->group(function () {
    Route::get('', [DashboardController::class, 'show_Dashboard'])->name('dashboard');
    Route::resource('admins', AdminController::class);
    Route::resource('cities', CityController::class);
    Route::get('edit-password', [AuthController::class, 'editPassword'])->name('edit-password');
    Route::PUT('Update-password', [AuthController::class, 'updatePassword'])->name('Update-password');
    Route::get('edit_profile', [AuthController::class, 'edit_profile'])->name('edit_profile');
    Route::get('edit_profile_imgae', [AuthController::class, 'edit_profile_image'])->name('edit_profile_image');
    Route::PUT('Update-profile', [AuthController::class, 'updat_profile'])->name('Update-profile');
    Route::PUT('Update-profile_image/{id}', [AuthController::class, 'updat_profile_image'])->name('Update-profile_image');
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    Route::resource('categories', CategoryController::class);
    Route::resource('pages', PageController::class);
    Route::get('student_management', [DashboardController::class, 'student'])->name('student_management.index');
    Route::get('teacher_management', [DashboardController::class, 'teacher'])->name('teacher_management.index');
    Route::post('search/search_admin', [SearchController::class, 'search_admin'])->name('search_admin');
    // Route::get('show_serves_admin/{id}', [AuthController::class, 'ahmed_serve'])->name('ahmed.serve');
    Route::get('show_serves_admin/{id}', [SearchController::class, 'ahmed_serve'])->name('ahmed.show');
    Route::get('Ahmed_Oeder/{id}', [SearchController::class, 'Ahmed_Oeder'])->name('Ahmed_Oeder');
    Route::get('/Certain/Show/{id}', [HomeController::class, 'show_Certain_Admin'])->name('show.CertainAdmin');
    Route::get('/Certain/show_Certain_Edit/{id}', [HomeController::class, 'show_Certain_Edit'])->name('show_Certain_Edit');
    Route::resource('/control/services', ServiceController::class);
});
//----------------------------------------------------------------------------------------------------

Route::prefix('/')->middleware('guest:teacher,student')->group(function () {
    Route::get('{guard}/login', [AuthTeacherController::class, 'showLogin'])->name('login');
    Route::post('/login', [AuthTeacherController::class, 'login']);
});



// الراوتات الخاصة بتعديل والحذف للادمن
Route::prefix('admin/')->middleware('auth:admin')->group(function () {
});





// Route::view('/', 'index')->name('Home'); // الرئيسية
Route::get('', [VisitorController::class, 'index'])->middleware('guest:teacher,student')->name('Home'); //   الرئيسية


Route::prefix('/register')->group(function () {
    Route::resource('student', StudentController::class);
    Route::resource('teacher', TeacherController::class);
    Route::get('logout_log', [AuthTeacherController::class, 'logout'])->name('logout_log');
});

//----------------------------------------------------------------------------------------------------

Route::get('/showindex', [HomeController::class, 'showindex'])->name('showindex')->middleware(['auth:teacher,student']); // المعلمين وخدماتهم

Route::get('/Home', [HomeController::class, 'showHome'])->name('showHome')->middleware(['auth:teacher,student']); // المعلمين وخدماتهم
Route::get('/requests_Std', [HomeController::class, 'requests_Std'])->name('requests_Std')->middleware('auth:teacher,student'); // الطلاب واحتياجاتهم

Route::prefix('/profile')->middleware('auth:teacher,student')->group(function () {
    Route::get('/', [HomeController::class, 'showprofile'])->name('showprofile');
    Route::get('/control', [HomeController::class, 'profile_settings'])->name('control');
    Route::resource('/control/services', ServiceController::class);
    Route::resource('/control/Profile/Edite/image', ImageController::class);
    // Route::get('/control/servicesshow/{services}', [HomeController::class, 'show_Service'])->name('show_Service');
    Route::get('/Edite_Profile', [HomeController::class, 'Edite_Profile'])->name('Edite_Profile');
    Route::put('/profile_setting/{id}', [HomeController::class, 'profile_update'])->name('profile_update');
    Route::get('/profile_setting/password_edit', [HomeController::class, 'password_edit'])->name('password_edit');
    Route::PUT('/profile_setting/Password/update_Password', [HomeController::class, 'update_Password'])->name('update_Password');
    // Route::get('/Show_Profile/{id}', [HomeController::class, 'Show_Profile'])->name('Show_Profile');
});


Route::prefix('/profile')->middleware('auth:teacher,student')->group(function () {
    Route::get('/control/std', [HomeController::class, 'profile_settings_std'])->name('control_std');
    Route::get('/show/student/{id}', [HomeController::class, 'show'])->name('show.std');
    Route::get('/show/teacher/{id}', [HomeController::class, 'show_teacher'])->name('show.teacher');
    Route::resource('/control/services', ServiceController::class);
    Route::resource('/control/order', OrderController::class);
    // Route::get('/control/servicesshow/{services}', [HomeController::class, 'show_Service'])->name('show_Service');
    Route::get('/Edite_Profile', [HomeController::class, 'Edite_Profile'])->name('Edite_Profile');
    Route::put('/profile_setting/{id}', [HomeController::class, 'profile_update'])->name('profile_update');
    Route::get('/profile_setting/password_edit', [HomeController::class, 'password_edit'])->name('password_edit');
    Route::PUT('/profile_setting/Password/update_Password', [HomeController::class, 'update_Password'])->name('update_Password');
});

//-------------complete medicine-----------------------------------------------------order route ----------------------------------------------------------------

Route::prefix('/serves')->middleware('auth:admin')->group(
    function () {
        Route::resource('/boxing', SuggestionController::class);
    }
);




Route::prefix('/serves')->middleware('auth:teacher,student')->group(function () {
    Route::get('/complete-medicine/{id}', [HomeController::class, 'Reqserves'])->name('complete_oeder');

    Route::resource('Certain', RqserveController::class);
    Route::resource('Notice', NoticeController::class);

    Route::get('/Certain/Show/{id}', [HomeController::class, 'show_Certain'])->name('show.Certain');
    Route::get('/Certain/Show/read/{id}', [HomeController::class, 'show_CertainPro'])->name('show.CertainPro');
    Route::get('/Certain/Show/Accept/{id}', [HomeController::class, 'Accept'])->name('show.Accept');
    Route::put('/Certain/Show/Accept/Satus/{id}', [HomeController::class, 'Accept_UPdate'])->name('Accept.UPdate');
    Route::get('/Certain/Show/read/std/read/all/{id}', [HomeController::class, 'read_all'])->name('read_all');


    Route::resource('Certain/rqstd', RqorderController::class);
    Route::get('/complete-medicine/Rqorder/{id}', [HomeController::class, 'Rqorder'])->name('complete_oederstd');
    Route::get('/Certain/Show/std/{id}/', [HomeController::class, 'showstd_Certain'])->name('showstd.Certain');
    Route::get('/Certain/Show/read/std/{id}', [HomeController::class, 'show_CertainProstd'])->name('show.CertainProstd');
    Route::get('/Certain/Show/read/std/read_all/{id}', [HomeController::class, 'read_allstd'])->name('read_allstd');


    Route::get('/View/all/notificationsstd/{id}', [HomeController::class, 'view_allstd'])->name('view_allstd');
    Route::get('/View/all/notifications/teach/{id}', [HomeController::class, 'view_all'])->name('view_all');
    //--------------------------------------------------------------عرض كافة الطلبات الواردة----------------------------------------------------------
    Route::get('/View/all/notifications/Incoming_requests/', [HomeController::class, 'rqserve_all'])->name('rqserve_all');
    Route::get('/View/all/notifications/Incoming_requests_std/', [HomeController::class, 'rqserve_allstd'])->name('rqserve_allstd');

    Route::Delete('destroy/{id}', [HomeController::class, 'destroy'])->name('destroy');
    Route::Delete('destroy_N/{id}', [HomeController::class, 'destroy_N'])->name('destroy_N');
    Route::put('update/image/{id}', [AuthController::class, 'updat_image'])->name('image.update');


    Route::get('/View/all/notifications/Incoming_requests_std/std_allserve', [HomeController::class, 'std_allserve'])->name('std_allserve');

    Route::get('star/{id}', [HomeController::class, 'star'])->name('star');
    Route::resource('star/teacher/rate', StarController::class);


    // البحث
    Route::post('search/', [SearchController::class, 'search'])->name('search');
    Route::post('search_2/', [SearchController::class, 'search_2'])->name('searcht_22');
    Route::post('search/std2/', [SearchController::class, 'search_std2'])->name('search_std2');
});


Route::view('/Authorization', 'error')->name('Authorization');



Route::prefix('/visitor')->middleware('guest:teacher,student')->group(
    function () {
        Route::get('/Home', [VisitorController::class, 'showHome'])->name('showHome_vs'); // للزوار المعلمين وخدماتهم
        Route::get('/requests_Std', [VisitorController::class, 'requests_Std'])->name('requests_Std_vs'); // الطلاب واحتياجاتهم
        Route::get('/whoami', [VisitorController::class, 'whoami'])->name('whoami'); // الطلاب واحتياجاتهم
        Route::post('search_2/', [VisitorController::class, 'search_viset'])->name('search_2');
        Route::post('search_2/vs_st', [VisitorController::class, 'search_visetstd'])->name('search_vsstd2');
        Route::post('search/vs_search', [SearchController::class, 'search_vs'])->name('search_vs');
    }
);


Route::resource('/serves/boxing', SuggestionController::class)->middleware('auth:admin,teacher,student');
