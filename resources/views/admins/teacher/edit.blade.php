@extends('admins.parent')
@section('title', ' تعديل بينات طالب ')
@section('bige-title', 'تعديل بينات طالب ')
@section('main-page', 'الرئيسية')
@section('sub-page', ' تعديل بينات طالب ')
@section('user', 'menu-open')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title" style="float: right">تعديل بينات طالب </h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="create-form">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name"> اسم الطالب </label>
                                    <input type="text" class="form-control" id="name" value="{{ $teacher->name }}">
                                </div>
                                <div class="form-group">
                                    <label for="name"> البريد الالكتروني</label>
                                    <input type="text" class="form-control" id="email" value="{{ $teacher->email }}">
                                </div>
                                <!-- /.card-body -->
                                <div class="form-group">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="active" @if ($teacher->Active) checked @endif>
                                        <label class="custom-control-label" for="active">تفعيل الحساب</label>
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="button" onclick="update({{ $teacher->id }})" class="btn btn-primary"
                                    id="button">تحديث</button>
                            </div>
                        </form>
                    </div>


                </div>

            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>

@endsection

@section('JS')
    <script>
        function update(id) {
            axios.put('/register/teacher/' + id, {
                    name: document.getElementById('name').value,
                    email: document.getElementById('email').value,
                    active: document.getElementById('active').checked,
                })
                .then(function(response) {
                    // handle success
                    console.log(response);
                    toastr.success(response.data.message);
                    // document.getElementById('create-form').reset(); // تصفير قيمة الفورم سبمت
                    window.location.href = "/admin/teacher_management"; // التحويل والانتقال
                })
                .catch(function(error) {
                    // handle error
                    console.log(error);
                    toastr.error(error.response.data.message);
                })
                .then(function() {
                    // always executed
                });
        }

    </script>
    {{-- <script>
    function update(id){
        axios.put('/cms/admin/categories/'+id,{
            name: document.getElementById('name').value,
            active: document.getElementById('active').checked
        })
        .then(function (response) {
            // handle success
            console.log(response);
            toastr.success(response.data.message);
            // document.getElementById('create-form').reset();
            window.location.href = "/cms/admin/categories";
        })
        .catch(function (error) {
            // handle error
            console.log(error);
            toastr.error(error.response.data.message);
        })
        .then(function () {
            // always executed
        });
    }
</script> --}}
@endsection
