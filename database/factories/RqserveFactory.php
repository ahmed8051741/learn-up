<?php

namespace Database\Factories;

use App\Models\Rqserve;
use Illuminate\Database\Eloquent\Factories\Factory;

class RqserveFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Rqserve::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
