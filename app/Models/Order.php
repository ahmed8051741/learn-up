<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SoftDeletes;

    public function link()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }


    public function suborder()
    {
        return $this->belongsTo(Rqorder::class, 'order_id', 'id');
    }


    public function Student()
    {
        return $this->belongsTo(Student::class, 'student_id', 'id');
    }

    public function City()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function getStatusKeywordAttribute()
    {
        return $this->status ? "فعال" : "معطل";
    }
}
