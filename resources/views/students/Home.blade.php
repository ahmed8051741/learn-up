@extends('layout.parent')


@section('Title', 'طلبات الطلاب')

@section('CSS')
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

<link href="{{ asset('assets/css/Star.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/profile.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet">
<style type="text/css">
    body {
        background-color: #f1f6fe;
        font-family: 'cairo';
        font-size: 15px;
    }

    body {
        /* background-color: #eee */
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: cover;
    }

    .time {
        font-size: 9px !important
    }

    .socials i {
        margin-right: 14px;
        font-size: 17px;
        color: #d2c8c8;
        cursor: pointer
    }

    .feed-image img {
        width: 100%;
        height: auto
    }

    .btn {
        line-height: 1.6;

    }



    .visitor {
        color: white;
        font-size: 15px
    }

    .visitor:hover {
        color: antiquewhite;

    }

    .border {
        border-radius: 8px;
        border: 0px solid #dee2e6 !important;
        box-shadow: 1px 1px 10px 10px #cecece42;

    }

    .btn {
        line-height: 1.6;

    }

    #footer {
        float: right;
        width: 100%;
    }

    .time {
        font-size: 9px !important
    }

    .socials i {
        margin-right: 14px;
        font-size: 17px;
        color: #d2c8c8;
        cursor: pointer
    }

    .feed-image img {
        width: 100%;
        height: auto
    }

    .bg-white {
        background-color: #ffffff !important;
    }

    .mt-4 {
        margin-top: 0rem !important;
    }

    .mb-5 {
        margin-bottom: 0rem !important;
    }


    .dropdown-menu {

        text-align: right;
        right: 0px;
        min-width: 11rem;

    }

    #topbar {
        height: 48px;

    }

    #mop {
        float: right;

    }

    .footer-top {
        float: right;
        width: 100%;
        margin-bottom: 25px
    }

    @media only screen and (max-width: 700px) {
        #footer-top {
            float: right;
            width: 100%;
        }

        #mop {
            float: right;
        }

        .ahmed {
            width: 100%
        }

    }

    .alert {
        position: relative;
        padding: 1rem 1rem;
        margin-bottom: 1rem;
        border: 0px solid transparent;
        border-radius: 0px;
    }




    .search {
        width: 198px;
        height: 30px;
        position: relative;
        left: 10px;
        margin-top: 10px;
        margin-right: 10px;
        margin-bottom: 10px;
    }

    .search input {
        position: absolute;
        float: Left;
        border-radius: 1px;
        width: 198px;
    }


    .btnsd {
        height: 38px;
        position: absolute;
        left: -50px;
        border-radius: 1px;
        padding-right: 14px;
    }

</style>
@endsection


@section('req', 'active')
@section('content')


@switch(Null)
@case(auth()->user()->describe)
    <div class="alert alert-warning" role="alert" dir="rtl">
        بينات حسابك غير مكتمله الرجاء الذهاب الى<a href="{{ route('showprofile') }}"> ملفك الشخصي </a> و اكمال اعداده
        ..

    </div>
@break


@case(auth()->user()->mobile)
    <div class="alert alert-warning" role="alert" dir="rtl">
        بينات حسابك غير مكتمله الرجاء الذهاب الى<a href="{{ route('showprofile') }}"> ملفك الشخصي </a> و اكمال اعداده
        ..

    </div>
@break


@case(auth()->user()->major)
    <div class="alert alert-warning" role="alert" dir="rtl">
        بينات حسابك غير مكتمله الرجاء الذهاب الى<a href="{{ route('showprofile') }}"> ملفك الشخصي </a> و اكمال اعداده
        ..

    </div>
@break



@endswitch


<div class="container" style="margin-top: 55px; margin-bottom: 70px">
<div class="row">
    <div class="col-sm-3">
        <div class="card">
            <div class="card-body">

                <div class="carda__body">
                    <form action="{{ route('search_std2') }}" method="post">
                        @csrf
                        <div class="row">

                            <div class="search">
                                {{-- <form action="{{ route('search') }}" method="GET"> --}}
                                <input type="text" name="search" class="form-control input-sm" maxlength="64"
                                    placeholder="ابحث عن خدمه ما" value="{{ $search }}" id="search" />

                                <button type="submit" class="btnsd btn btn-primary btn-sm">
                                    <i class="fas fa-search"
                                        style="text-align: center;margin-left: 3px;"></i></button>
                                {{-- </form> --}}
                            </div>

                            <div class="col-sm-12 mrg--vt" style="margin-top: 7px">
                                <div class="form-group">
                                    <label for="#"> التصنيفات </label>
                                    @foreach ($categorise as $category)
                                        <div class="form-check">
                                            <label>
                                                <label>
                                                    <input type="checkbox" name="category"
                                                        class="custom-control-input" value="{{ $category->id }}"
                                                        @if ($category->id == $categoryx) checked @endif>
                                                    <span class="label-text">
                                                        {{ $category->name }}</span>
                                                </label>
                                        </div>
                                    @endforeach


                                </div>
                            </div>


                            <div class="col-sm-12 mrg--vt" style="padding-top: 9px">
                                <div class="form-group">
                                    <label for=""> الميزانية المالية </label>
                                    <select class="form-control" id="case" name="case">
                                        <option value="{{ $case }}">
                                            @if ($case == null)
                                            @elseif($case != 'غير ذالك')
                                                أقل من او يساوي {{ $case }}
                                            @elseif($case == 'غير ذالك')
                                                غير ذالك
                                            @endif
                                        </option>
                                        <option value="50"> أقل من او يساوي 50</option>
                                        <option value="100"> أقل من او يساوي 100</option>
                                        <option value="200"> أقل من او يساوي 200</option>
                                        <option value="300"> أقل من او يساوي 300</option>
                                        <option value="400"> أقل من او يساوي 400</option>
                                        <option value="500"> أقل من او يساوي 500</option>
                                        <option value="غير ذالك"> غير ذالك </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 mrg--vt" style="margin-top: 10px">
                                <div class="form-group">
                                    <label for="#"> المنطقة الجغرافية </label>
                                    @foreach ($cities as $City)
                                        <div class="form-check">
                                            <label>
                                                <input type="checkbox" name="city" class="city"
                                                    value="{{ $City->id }}" @if ($City->id == $Cityx) checked @endif>
                                                <span class="label-text">
                                                    {{ $City->name }}</span>
                                            </label>

                                        </div>
                                    @endforeach
                                    <button type="submit" class="btnsd btn btn-primary"
                                        style="position: inherit; width: 100%;text-align: center">
                                        <i class="fas fa-sliders-h"> &nbsp; </i>
                                        فلترة
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@section('dd')
    <a href="mailto:ahmed8051741@gmail.com" class="visitor"> &nbsp; الدعم الفني عبر البريد الالكتروني
    </a>
@endsection

<div class="col-sm-9">
    <div class="card">
        @foreach ($orders as $order)

            <div class="container" style="border-bottom: solid 1px #d3d3d3 " id="{{ $order->id }}">
                <div class="row">
                    <div>
                        <div
                            class="d-flex flex-row justify-content-between align-items-center p-2 border-bottom">
                            <div class="d-flex flex-row align-items-center feed-text px-2">
                                <a href="{{ route('show.std', $order->student->id) }}"> <img
                                        class="rounded-circle"
                                        src="{{ asset('storage/image_profile/' . $order->student->image) }}"
                                        width="47">
                                    &nbsp;&nbsp;
                                    <div class="d-flex flex-column flex-wrap ml-2"><span
                                            class="font-weight-bold">
                                            {{ $order->student->name }}</span>
                                </a> <span class="text-black-50 time">
                                    &nbsp; {{ $order->created_at->diffForHumans() }} | &nbsp;


                                    <span class="font-weight-bold"> تصنيف الخدمة
                                        :{{ $order->link->name }}</span>
                                </span>

                            </div>
                        </div>

                        <div class="feed-icon px-2"><i class="fa fa-ellipsis-v text-black-50"
                                aria-hidden="true"></i></div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <br>
                    <img src="{{ asset('storage/images/' . $order->image) }}" class="card-img" alt="">
                </div>

                <div class="col-sm-9">
                    <div class="card-body" style="padding-bottom:50px">
                        <h5 class="card-title"> {{ $order->title }} </h5>
                        <p class="card-text"> {{ $order->describe }}</p>

                        @if (auth()->user()->id == auth('teacher')->id())
                            <a href="{{ route('complete_oederstd', $order->id) }}">
                                <button type="button" class="btn btn-primary" style="float: left">
                                    <i class="fas fa-folder-plus"
                                        style="color: #ffffff;padding: 0.6px;margin: 0px"></i>&nbsp;
                                    أطلب الأن

                                </button>
                            </a>
                        @else
                            <p style="float: left">
                                <i class="fas fa-times"
                                    style="color: #ff0000;padding: 0.6px;margin: 0px"></i>&nbsp;
                                انت طالب يمكنك التصفح فقط
                            </p>
                        @endif



                        <p class="card-text">
                            <span> <i class="fas fa-hand-holding-usd"> </i> {{ $order->price }}
                                شيكل </span> &nbsp;
                            <span> <i class="nav-icon fas fa-street-view"> </i> {{ $order->City->name }}
                            </span>&nbsp;
                            <span> <i class="fab fa-galactic-senate"> </i>
                                {{ $order->select_CAT }}
                            </span>
                        </p>

                    </div>

                </div>
            </div>

            <br>


    </div>
    @endforeach

</div>

</div>

</div>

</div>





</div>
</div>

@endsection


@section('JS')
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
</script>
<script src="{{ asset('assets/css/select2.min.js') }}"></script>
<script>
    $(".js-example-theme-single").select2({
        theme: "classic"
    });

    $(".js-example-theme-multiple").select2({
        theme: "classic"
    });
    $(".js-example-responsive").select2({
        width: 'resolve' // need to override the changed default
    });


    $(".js-example-disabled").select2();
    $(".js-example-disabled-multi").select2();

    $(".js-programmatic-enable").on("click", function() {
        $(".js-example-disabled").prop("disabled", false);
        $(".js-example-disabled-multi").prop("disabled", false);
    });

    $(".js-programmatic-disable").on("click", function() {
        $(".js-example-disabled").prop("disabled", true);
        $(".js-example-disabled-multi").prop("disabled", true);
    });
</script>

<script type="text/javascript">
    $('.custom-control-input').on('change', function() {
        $('.custom-control-input').not(this).prop('checked', false);
    });
</script>
<script type="text/javascript">
    $('.city').on('change', function() {
        $('.city').not(this).prop('checked', false);
    });
</script>
@endsection
