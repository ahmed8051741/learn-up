@extends('layout.parent')


@section('Title', 'اضافة خدمه')

@section('CSS')

    <link href="{{ asset('assets/css/Star.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/profile.css') }}" rel="stylesheet">
    <style>
        .custom-file {
            position: relative;
            display: inline-block;
            width: 100%;
            height: calc(2.25rem + 2px);
            margin-bottom: 0;
        }

        .custom-file-input {
            position: relative;
            z-index: 2;
            width: 100%;
            height: calc(2.25rem + 2px);
            margin: 0;
            opacity: 0;
        }

        .custom-file-label {
            position: absolute;
            font-family: 'cairo';
            top: 0;
            left: 0;
            right: 0;
            z-index: 1;
            height: calc(2.25rem + 2px);
            padding: .375rem .75rem;
            font-weight: 400;
            line-height: 1.5;
            color: #495057;
            background-color: rgb(235, 235, 235);
            border: 1px solid #ced4da;
            border-radius: .25rem;
        }

        #header {
            visibility: hidden;
            margin-top: -50px;
        }

    </style>

@endsection

@section('dd')
    <h2 class="logo" style="font-family: Al-Jazeera-Arabic"><a href="{{ route('showHome') }}"
            style="font-family: Al-Jazeera-Arabic; color: aliceblue"> Learn <span
                style="font-family: Al-Jazeera-Arabic;font-weight: 760;color: #Fd4;"> up
            </span></a></h2>
@endsection
@section('con', 'display: none')


@section('content')
    <div class="container bootstrap snippets bootdey">
        <div class="row">

            <div class="profile-nav col-md-3">
                <div class="panel">
                    <div class="user-heading round">
                        <a href="#">
                            <img src="{{ asset('storage/image_profile/' . auth()->user()->image) }}" alt="">
                        </a>
                        <h1> {{ auth()->user()->name }} </h1>
                        <p>{{ auth()->user()->email }} </p>
                    </div>
                    <ul class="nav">
                        <li><a href="{{ route('control') }}"><i class="fas fa-tachometer-alt">
                                    &nbsp;</i>
                                دفقة القيادة </a></li>
                        <li><a href="{{ route('showprofile') }}"><i class="fa fa-user"> &nbsp;</i> الملف الشخصي </a>
                        </li>

                        @if (auth()->user()->id == auth('teacher')->id())
                            <li><a href="{{ route('services.index') }}"><i class="fab fa-servicestack">
                                        &nbsp;</i>
                                    الخدمات
                                </a></li>

                            <li><a href="{{ route('rqserve_all') }}"><i class="fas fa-retweet"> &nbsp;</i>
                                    الطلبات الواردة
                                </a></li>
                        @else
                            <li><a href="{{ route('order.index') }}"><i class="fab fa-servicestack"> &nbsp;</i>
                                    الطلبات
                                </a></li>


                            <li><a href="{{ route('rqserve_allstd') }}"><i class="fas fa-retweet"> &nbsp;</i>
                                    العروض المقدمه
                                </a></li>
                        @endif

                                     <li  class="active"><a href="{{ route('boxing.create') }}"><i class="fas fa-box-open"> &nbsp;</i>
                                        صندوق الاقتراحات
                                </a></li>


                        <li><a href="{{ route('password_edit') }}"><i class="fas fa-lock"> &nbsp;</i> تعديل كلمه
                                المرور</a></li>



                        <li><a href="{{ route('logout_log') }}"><i class="fas fa-sign-out-alt"> &nbsp;</i> تسجيل
                                الخروج</a></li>
                    </ul>

                </div>
            </div>
            <div class="profile-info col-md-9" style="margin-bottom: 60px">
                <div class="panel">
                    <div class="bg-info clearfix">
                        <button type="button" class="btn btn-secondary float-right"> صندوق الاقتراحات </button>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header" style="padding-top: 13px;padding-bottom: 12px;padding-right:18px;width: 100%;">


                        <div class="col-8">
                            <h4> صندوق الاقتراحات </h3>
                        </div>


                    </div>


                    <!-- /.card-header -->
                    <div class="card-body">

                        <form>


                              <div class="form-group row">
                                <label for="describe" class="col-12 col-form-label mb-2" style="color: rgb(0, 0, 0)"> ادخل ما تود اقتراحه هنا - او ادخل مشكلتك ان كان لديك مشكلة </label>
                                <div class="col-12">
                                    <textarea id="describe" cols="40" rows="4" class="form-control"> </textarea>
                                </div>
                            </div>

                             <br>
                            <br>

                            <button type="button" onclick="store()" class="btn btn-primary"
                                id="button" style="float: left; width: 100%"> ارسل الاقتراح </button>
                            <br>
                            <br>
                            <br>
                        </form>
                    </div>

                    <!-- /.card-body -->
                </div>

            </div>



        @endsection


        @section('JS')

          <script>
    function store(id) {
        Swal.fire({

            title: 'تحذير !',
            text: 'لن تتمكن من الغاء هذه العملية او التعديل عليها هل انت متأكد من الأرسال الى مدير النظام',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'الغاء',
            confirmButtonText: 'نعم ، اكمل من فضلك',
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                axios.post('/serves/boxing/', {
                        describe: document.getElementById('describe').value,
                    })
                    .then(function(response) {
                        // handle success
                        console.log(response);
                        toastr.success(response.data.message);
                        document.getElementById('create-form').reset(); // تصفير قيمة الفورم سبمت
                        // window.location.href = "/profile/show/teacher/" + id; // التحويل والانتقال
                    })
                    .catch(function(error) {
                        // handle error
                        console.log(error);
                        toastr.error(error.response.data.message);
                    })
                    .then(function() {
                        // always executed
                    });
            }
        })
    }
</script>

        @endsection
