@extends('admins.parent')

@section('title', 'تغير كلمه المرور')
@section('bige-title', 'تغير كلمه المرور')
@section('main-page', 'الرئيسية')
@section('sub-page', 'تغير كلمه المرور')
@section('profile', 'active')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title" style="float: right">تغير كلمه المرور</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="create-form">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">كلمه المرور الحالية</label>
                                    <input type="password" class="form-control" id="current_password"
                                        placeholder="ادخل كلمه المرور الحالية">
                                </div>
                                <div class="form-group">
                                    <label for="name">كلمه المرور الجديدة</label>
                                    <input type="password" class="form-control" id="new_password"
                                        placeholder="ادخل كلمه المرور الجديدة">
                                </div>
                                <div class="form-group">
                                    <label for="name">ادخل كلمه المرور الجديدة مرة اخرى</label>
                                    <input type="password" class="form-control" id="new_password_confirmation"
                                        placeholder="اعد ادخال كلمه المرور مرة اخرى">
                                </div>

                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="button" onclick="UpdatePassword()" class="btn btn-primary" id="button"> تحديث
                                    كلمه المرور </button>
                            </div>
                        </form>
                    </div>


                </div>

            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>

@endsection

@section('JS')
    <script>
        function UpdatePassword() {
            axios.put('/admin/Update-password/', {
                    current_password: document.getElementById('current_password').value,
                    new_password: document.getElementById('new_password').value,
                    new_password_confirmation: document.getElementById('new_password_confirmation').value
                })
                .then(function(response) {
                    // handle success
                    console.log(response);
                    toastr.success(response.data.message);
                    document.getElementById('create-form').reset(); // تصفير قيمة الفورم سبمت
                    // window.location.href="/cms/admin/Update-password/"; // التحويل والانتقال
                })
                .catch(function(error) {
                    // handle error
                    console.log(error);
                    toastr.error(error.response.data.message);
                })
                .then(function() {
                    // always executed
                });
        }

    </script>
@endsection
