@extends('admins.parent')

@section('title', 'لوحة القيادة')
@section('bige-title', 'لوحة القيادة')
@section('main-page', 'لوحة القيادة')
@section('sub-page', 'الرئيسية')

@section('CSS')
<style>
.badge {
font-size: 16px;
}

.card-headers {
background-color: ;
}

</style>

@endsection

@section('dashboard', 'active')

@section('content')

<section class="content">
<div class="container-fluid">
<!-- Info boxes -->
<div class="row">
<div class="col-12 col-sm-6 col-md-3">
<div class="info-box">
    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>

    <div class="info-box-content">
        <span class="info-box-text">المشرفون </span>
        <span class="info-box-number">
            {{ $adminsCounts }}
            <small>مشرفون</small>
        </span>
    </div>
    <!-- /.info-box-content -->
</div>
<!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-12 col-sm-6 col-md-3">
<div class="info-box mb-3">
    <span class="info-box-icon bg-dark elevation-1"><i class="fas fa-chalkboard-teacher"></i></span>

    <div class="info-box-content">
        <span class="info-box-text"> المعلمون </span>
        <span class="info-box-number">{{ $TeacherCounts }} <small>معلم</small></span>


    </div>
    <!-- /.info-box-content -->
</div>
<!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-12 col-sm-6 col-md-3">
<div class="info-box mb-3">
    <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

    <div class="info-box-content">
        <span class="info-box-text"> الطلاب </span>
        <span class="info-box-number">{{ $StudentiesCount }} <small>طالب</small> </span>
    </div>
    <!-- /.info-box-content -->
</div>
<!-- /.info-box -->
</div>
<!-- fix for small devices only -->
<div class="clearfix hidden-md-up"></div>

<div class="col-12 col-sm-6 col-md-3">
<div class="info-box mb-3">
    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-shopping-cart"></i></span>

    <div class="info-box-content">
        <span class="info-box-text">القيمة المالية</span>
        <span class="info-box-number">760</span>
    </div>
    <!-- /.info-box-content -->
</div>
<!-- /.info-box -->
</div>
<!-- /.col -->

<!-- /.col -->
</div>
<!-- /.row -->
<!-- Main row -->
<div class="row">
<!-- Left col -->
<div class="col-md-12">
<!-- TABLE: LATEST ORDERS -->
<div class="card">
    <div class="card-header border-transparent" style="background-color: rgb(23 162 184)">
        <h3 class="card-title"></h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table m-0" style="text-align: center">
                <thead>
                    <tr>
                        <th>معرف المعلم</th>
                        <th>اسم المعلم </th>
                        <th>هاتف المعلم </th>
                        <th>حالة النشاط</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($Teachers as $Teacher)
                        <tr>
                            <td><a href="">{{ $Teacher->id }}</a></td>
                            <td> {{ $Teacher->name }} </td>
                            @if ($Teacher->mobile != null)
                                <td> {{ $Teacher->s_mobile }}&nbsp;{{ $Teacher->mobile }}+ </td>
                            @else
                                <td> لم يقم هذا المستخدم بوضع رقم الهاتف</td>
                            @endif
                            <td><span class="badge badge-success" style="font-size: 14px">
                                    نشط {{ $Teacher->updated_at->diffForHumans() }} </span>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.card-body -->
    <div class="card-footer clearfix">
        <a href="javascript:void(0)" class="btn btn-sm btn-info float-right">عرض جميع المعلمين</a>
    </div>
    <!-- /.card-footer -->
</div>
<div class="card">
    <div class="card-header border-transparent" style="background-color: rgb(255 193 7)">
        <h3 class="card-title"></h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table m-0" style="text-align: center">
                <thead>
                    <tr>
                        <th>معرف الطالب </th>
                        <th>اسم الطالب </th>
                        <th>هاتف الطالب </th>
                        <th>حالة النشاط</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($stds as $std)
                        <tr>
                            <td><a href="">{{ $std->id }}</a></td>
                            <td> {{ $std->name }} </td>
                            @if ($std->mobile != null)
                                <td> {{ $std->s_mobile }}&nbsp;{{ $std->mobile }}+ </td>
                            @else
                                <td> لم يقم هذا المستخدم بوضع رقم الهاتف </td>
                            @endif
                            <td><span class="badge badge-success" style="font-size: 14px">
                                    نشط {{ $std->updated_at->diffForHumans() }} </span>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>

    <!-- /.card-body -->
    <div class="card-footer clearfix">
        <a href="javascript:void(0)" class="btn btn-sm btn-info float-right">عرض جميع الطلاب </a>
    </div>
    <!-- /.card-footer -->
</div>

<!-- /.card -->
</div>

<!-- /.col -->
</div>
<!-- /.row -->
</div>
</div>

<div style="background-color: #F4F6F9; height: auto;">
@foreach ($stds as $std)
<br>
</div>
@endforeach
@foreach ($Teachers as $Teacher)
<br>
</div>
@endforeach
<!--/. container-fluid -->
</section>

@endsection

<!-- overlayScrollbars -->

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{ asset('cms/plugins/raphael/raphael.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('cms/plugins/chart.js/Chart.min.js') }}"></script>



@section('JS')
