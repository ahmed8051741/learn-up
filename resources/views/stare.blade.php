@extends('layout.parent')


@section('Title', 'الملف الشخصي')

@section('CSS')

    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

    <link href="{{ asset('assets/css/Star.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/profile.css') }}" rel="stylesheet">
    <style>
        #re {
            display: none;
        }

        .alert .close,
        .alert .mailbox-attachment-close {
            color: #000;
            opacity: .2;
        }

        .alert-dismissible .close,
        .alert-dismissible .mailbox-attachment-close {
            position: absolute;
            top: 0;
            right: 0;
            padding: .75rem 1.25rem;
            color: inherit;
        }

        [type=button]:not(:disabled),
        [type=reset]:not(:disabled),
        [type=submit]:not(:disabled),
        button:not(:disabled) {
            cursor: pointer;
        }

        button.close,
        button.mailbox-attachment-close {
            padding: 0;
            background-color: transparent;
            border: 0;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }

        element.style {}

        .close:not(:disabled):not(.disabled):focus,
        .close:not(:disabled):not(.disabled):hover,
        .mailbox-attachment-close:not(:disabled):not(.disabled):focus,
        .mailbox-attachment-close:not(:disabled):not(.disabled):hover {
            opacity: .75;
        }

        .alert .close:hover,
        .alert .mailbox-attachment-close:hover {
            opacity: .5;
        }

        .alert .close,
        .alert .mailbox-attachment-close {
            color: #000;
            opacity: .2;
        }

        .close:hover,
        .mailbox-attachment-close:hover {
            color: #000;
            text-decoration: none;
        }

        .alert-dismissible .close,
        .alert-dismissible .mailbox-attachment-close {
            position: absolute;
            top: 0;
            right: 0;
            padding: .75rem 1.25rem;
            color: inherit;
        }

        [type=button]:not(:disabled),
        [type=reset]:not(:disabled),
        [type=submit]:not(:disabled),
        button:not(:disabled) {
            cursor: pointer;
        }

        button.close,
        button.mailbox-attachment-close {
            padding: 0;
            background-color: transparent;
            border: 0;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }

        .close,
        .mailbox-attachment-close {
            float: right;
            font-size: 1.5rem;
            font-weight: 700;
            line-height: 1;
            color: #000;
            text-shadow: 0 1px 0 #fff;
            opacity: .5;
        }

    </style>

@endsection

@section('content')


@section('Top')
    <section id="topbar">
        <div class="container d-flex justify-content-center justify-content-md-between" id="topb">
            <div class="contact-info d-flex align-items-center">
                <a href="mailto:ahmed8051741@gmail.com" class="visitor"> &nbsp; الدعم الفني عبر البريد الالكتروني </a>
            </div>
            <div class="d-flex flex-row align-items-center feed-text px-2" dir="ltr">
                &nbsp;&nbsp;
                <div class="d-flex flex-column flex-wrap ml-2"><span class="font-weight-bold">
                        <div class="dropdown">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" style="color: #ffffff;"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="rounded-circle" src="assets/img/my.jpeg" width="42"> &nbsp;&nbsp;
                                {{ auth()->user()->name }} &nbsp;</a>

                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="{{ route('showprofile') }}">الملف الشخصي</a>
                                <hr style="margin-top:0.8px;margin-bottom:0.8px">
                                <a class="dropdown-item" href="{{ route('services.create') }}">اضافة خدمة جديد</a>
                                <a class="dropdown-item" href="{{ route('control') }}">لوحة القيادة</a>
                                <a class="dropdown-item" href="{{ route('logout_log') }}">تسجيل الخروج</a>
                            </div>
                        </div>
                    </span>

                </div>
            </div>
        </div>
    </section>
@endsection

@section('dd')
    <a href="mailto:ahmed8051741@gmail.com" class="visitor"> &nbsp; الدعم الفني عبر البريد الالكتروني
    </a>
@endsection
<div class="container bootstrap snippets bootdey">
    <div class="row">
        <div class="profile-nav col-md-3">
            <div class="panel">
                <div class="user-heading round">
                    <a href="#">
                        <img src="{{ asset('storage/image_profile/' . $teacher->image) }}" alt="">
                    </a>
                    <h1> {{ $teacher->name }}</h1>
                    <p> التخصص :

                        @if ($teacher->major == null)
                            لم يتم اختيار التخصص بعد
                        @else
                            {{ $teacher->major }}
                        @endif

                    </p>
                    <p> هاتف :
                        @if ($teacher->mobile == null)
                            لا يوجد رقم هاتف

                        @else
                            {{ $teacher->mobile }}

                        @endif

                </div>

                <ul class="nav">

                    <li class="active"><a href=" "><i class="fa fa-user"> &nbsp;</i>
                            الملف
                            الشخصي</a></li>
                    @if ($teacher->email != null)
                        <li><a href="mailto:{{ $teacher->email }}" target="_blank"><i class="far fa-envelope">
                                    &nbsp;</i>
                                تواصل من
                                خلال البريد الالكتروني </a></li>
                    @endif
                    @if ($teacher->facebook != null)

                        <li><a href="{{ $teacher->facebook }}" target="_blank"><i class="fab fa-facebook-square">
                                    &nbsp;</i> تواصل من
                                خلال فيسبوك
                                بوك</a></li>
                    @endif

                    @if ($teacher->mobile != null)

                        <li><a href="https://wa.me/{{ $teacher->s_mobile }}{{ $teacher->mobile }}"
                                target="_blank"><i class="fab fa-whatsapp" style="  font-size: 14px"> &nbsp;</i>
                                تواصل من خلال واتس
                                اب </a>
                        </li>
                    @endif

                    @if ($teacher->instagram != null)

                        <li><a href="https://wa.me/{{ $teacher->instagram }}" target="_blank"><i
                                    class="fab fa-instagram" style="  font-size: 14px"> &nbsp;</i>
                                تواصل من خلال انستجرام </a>
                        </li>
                    @endif
                    @if ($teacher->youtube != null)

                        <li><a href="{{ $teacher->youtube }}" target="_blank"><i class="fab fa-youtube">
                                    &nbsp;</i>الذهاب
                                الى قناه
                                اليوتيوب
                            </a></li>
                    @endif



                </ul>
            </div>
        </div>
        <div class="profile-info col-md-9">
            <div class="panel">
                <div class="bg-info clearfix">
                    <button type="button" class="btn btn-secondary float-right"> الملف الشخصي </button>
                </div>
            </div>
            <div class="panel">

                <div class="panel-body bio-graph-info">
                    <div style="margin: 5px; margin-right: 16px;margin-top:20px; ">
                        <h4> نبذة تعريفية عن {{ $teacher->name }} </h4>
                        <div align="center">
                            <hr width="96%">
                        </div>


                        <div class="row" style="margin: 5px;margin-top: 25px">
                            <div align="center">
                                <p style="text-align: right ; width: 80%"><span>
                                        @if ($teacher->describe == null)
                                            لم
                                            يتم اضافة اي وصف
                                            <span style="color: red"> لذالك نحن لا ننصح بتعامل مع هذا المدرس </span>
                                        @else
                                            {{ $teacher->describe }}
                                        @endif
                                    </span>
                                </p>
                            </div>
                        </div>
                        @if (auth()->user()->account_type == 'student')

                            <div style="margin: 5px; margin-right: 16px ">
                                <div align="center">
                                    <hr width="96%">
                                </div>

                                <div>

                                    <div id="sd">
                                        <form action="{{ route('rate.store') }}" method="POST">

                                            @if ($errors->any())
                                                <div class="alert alert-danger alert-dismissible">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">×</button>
                                                    <h5><i class="icon fas fa-ban"></i> خطاء </h5>
                                                    <li> لقد تم ايقاف هذه الخدمه لذالك لن تسطيع تقيمها</li>

                                                </div>
                                            @endif

                                            @if (session()->has('message'))
                                                <div class="alert alert-success alert-dismissible">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">×</button>
                                                    <h5><i class="icon fas fa-check"></i> نجاح !</h5>
                                                    {{ session()->get('message') }}
                                                </div>
                                            @endif



                                            @csrf
                                            <span style="font-size: 18px ; text-align: right" dir="rtl">
                                                قيم هذه الخدمه :
                                            </span>
                                            <input class="star star-5" id="star-5-2" type="radio" name="star"
                                                value="5" />
                                            <label class="star star-5" for="star-5-2"></label>
                                            <input class="star star-4" id="star-4-2" type="radio" value="4"
                                                name="star" />
                                            <label class="star star-4" for="star-4-2"></label>
                                            <input class="star star-3" id="star-3-2" type="radio" name="star"
                                                value="3" />
                                            <label class="star star-3" for="star-3-2"></label>
                                            <input class="star star-2" id="star-2-2" type="radio" name="star"
                                                value="2" />
                                            <label class="star star-2" for="star-2-2"></label>
                                            <input class="star star-1" id="star-1-2" type="radio" name="star"
                                                value="1" />
                                            <label class="star star-1" for="star-1-2"></label>
                                            <p style="color: red;"> تحذير لن تتمكن من التراجع عن هذا التقيم لاحقاً في
                                                الوقت
                                                الحالي </p>

                                            {{-- ---------------------------------------------------------------------------------------------------------------------------- --}}
                                            <div style="display: none">
                                                <input type="text" value="{{ auth()->user()->id }}" name="id_std">

                                                @foreach ($services as $service)
                                                    @if ($service->Teacher->id == $teacher->id)
                                                        <input type="text" value="{{ $teacher->id }}"
                                                            name="id_teacher">
                                                        <input type="text" value="{{ $service->id }}"
                                                            name="id_service">
                                                        <?php
                                                        $stop = $service->id;
                                                        ?>
                                                    @break
                                                @endif

                        @endforeach

                        @foreach ($stars as $star)
                            @if ($stop == $star->service_id)
                                <style>
                                    #sd {
                                        display: none;
                                    }

                                    #re {
                                        display: block;
                                    }

                                </style>
                            @endif
                        @endforeach
                    </div>
                    <br>
                    {{-- ---------------------------------------------------------------------------------------------------------------------------- --}}
                    <div class="card">
                        <button type="submit" class="btn btn-primary" id="button">
                            تأكيد التقيم </button>
                    </div>
                    </form>
                </div>

                <p style="color: red" id="re"> لقد قمت بتقيم هذالمعلم مسبقاً </p>

            </div>
        </div>
        @endif

    </div>
</div>

<div>

    <br>
    <br>
    <br>

</div>
<br>
</div>
</div>
</div>

<br>
<br>
<br>

</div>



@endsection


@section('JS')



<script>
    function store(id) {
        Swal.fire({

            title: 'تحذير !',
            text: 'لن تتمكن من التراجع عن التقيم او تعديلة لاحقاً  ..!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'الغاء',
            confirmButtonText: 'نعم ، اكمل من فضلك',
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                axios.post('/serves/Certain/rqstd/', {
                        order: document.getElementById('order').value,
                    })
                    .then(function(response) {
                        // handle success
                        console.log(response);
                        toastr.success(response.data.message);
                        // document.getElementById('create-form').reset(); // تصفير قيمة الفورم سبمت
                        window.location.href = "/profile/show/teacher/" + id; // التحويل والانتقال
                    })
                    .catch(function(error) {
                        // handle error
                        console.log(error);
                        toastr.error(error.response.data.message);
                    })
                    .then(function() {
                        // always executed
                    });
            }
        })
    }
</script>
@endsection
