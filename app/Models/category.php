<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class category extends Model
{
    use HasFactory, SoftDeletes;
    //status_next واذا كانت تتكون من مقطعين يوضع كتالي  git , Attribute خاصية ابنت وهي عبارة عن خاصية لاضافة الاتربيوت ليتعرف علية الموبايل -- وتضاف الكلمة بدون
    protected $appends = ['status'];

    //Append attribute
    //بدل معمل فحص لقيمة الاكتف من البليد بعملها من هنا

    // public function get_____Attribute(){}

    public function getStatusAttribute()
    {
        return $this->active ? "Active" : "Disabled";
    }


    public function sublink()
    {
        return $this->hasMany(Order::class, 'category_id', 'id');
    }

    public function subCategories()
    {
        // return $this->hasMany(Service::class);
        return $this->hasMany(Service::class, 'category_id', 'id');
    }
}
