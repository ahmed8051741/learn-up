@extends('layout.parent')


@section('Title', 'حالة الطلب')

@section('CSS')

    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="{{ asset('assets/css/Star.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/profile.css') }}" rel="stylesheet">
    <script>
        $(document).ready(function() {
            $("#button1").click(function() {
                $("#titles").fadeToggle();
            });

        });

        // $(document).ready(function(){
        //   $("#flip").click(function(){
        //     $("#panel").slideDown("slow");
        //   });
        // });

        $(document).ready(function() {
            $("#button1").click(function() {
                //   $("#title").css("background-color", "yellow");

                $("#titles").css("display", "initial");
            });
        });
    </script>

    <style>
        * {
            font-family: 'cairo';
        }

        .visitor {
            color: white;
            font-size: 15px;


        }

        .row {
            display: flex;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }

        @media (min-width: 768px) {
            .col-md-12 {
                flex: 0 0 100%;
                max-width: 100%;
            }

            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                font-family: "Al-Jazeera-Arabic";
            }

            .h4,
            h4 {
                font-size: 1.5rem;
            }

            .form-group {
                margin-bottom: 5px;
                margin-top: 5px
            }

            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                margin-top: 0;
                margin-bottom: .5rem;
            }

        }

        .card {
            margin-top: 12px
        }

        .form-group {
            margin-bottom: 1rem;
        }

        h4 {
            display: block;
            margin-block-start: 1.33em;
            margin-block-end: 1.33em;
            margin-inline-start: 0px;
            margin-inline-end: 0px;

        }


        /* .btn-group>.btn-group:not(:last-child)>.btn, .btn-group>.btn:not(:last-child):not(.dropdown-toggle) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                border-top-right-radius: 4px;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                border-bottom-right-radius: 4px;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            }

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            .btn-group>.btn-group:not(:first-child)>.btn, .btn-group>.btn:nth-child(n+3), .btn-group>:not(.btn-check)+.btn {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     border-top-left-radius:4px;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     border-bottom-left-radius:4px;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            } */
        .dropdown-menu {
            text-align: right;
            right: 0px;
            min-width: 11rem;
        }

        .pro1 {
            border-top-right-radius: 0px;
            border-bottom-right-radius: 0px;
            border-top-left-radius: 0px;
            border-bottom-left-radius: 0px;
        }

        .pro {
            border-top-right-radius: 0px;
            border-bottom-right-radius: 0px;


        }

        h4 {
            margin-block-start: 0.33em;
            margin-block-end: 0.33em;
            margin-inline-start: 0px;
            margin-inline-end: 0px;
        }

        .table>:not(caption)>*>* {
            padding: .5rem .5rem;
            padding-top: 20px;
        }

        .badge-success {
            color: #fff;
            background-color: #28a745;
        }

        .badge-info {
            color: #fff;
            background-color: #1c3c94;
        }

        .badge {
            display: inline-block;
            padding: .25em .4em;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25rem;
        }

        .btn-success {
            color: #fff;
            background-color: #28a745;
            border: #28a745;
        }

        .btn-success:hover {
            color: #fff;
            background-color: #1f8b38;
            border: #28a745;
        }

    </style>
@endsection



@section('dd')
    <a href="mailto:ahmed8051741@gmail.com" class="visitor"> &nbsp; الدعم الفني عبر البريد الالكتروني
    </a>
@endsection

@section('content')



    <div class="container bootstrap snippets bootdey">
        <div class="row">
            <div class="profile-nav col-md-3">
                <div class="panel">
                    <div class="user-heading round">
                        <a href="#">
                            <img src="{{ asset('storage/image_profile/' . auth()->user()->image) }}" alt="">
                        </a>
                        <h1> {{ auth()->user()->name }} </h1>
                        <p>{{ auth()->user()->email }} </p>
                    </div>

                    <ul class="nav">
                        <li><a href="{{ route('control') }}"><i class="fas fa-tachometer-alt"> &nbsp;</i> دفقة
                                القيادة </a></li>
                        <li><a href="{{ route('showprofile') }}"><i class="fa fa-user"> &nbsp;</i> الملف الشخصي </a></li>
                        <li class="active"><a href="{{ route('services.index') }}"><i class="fas fa-indent"> &nbsp;</i>
                                الخدمات </a></li>

                        <li><a href="{{ route('password_edit') }}"><i class="fas fa-lock"> &nbsp;</i> تعديل كلمه
                                المرور</a></li>
                        <li><a href="{{ route('control') }}"><i class="fa fa-calendar"> &nbsp;</i> العودة الى لوحة التحكم
                            </a></li>
                        <li><a href="{{ route('logout_log') }}"><i class="fas fa-sign-out-alt"> &nbsp;</i> تسجيل
                                الخروج</a></li>
                    </ul>
                </div>
            </div>


            <div class="profile-info col-md-9" style="margin-bottom: 60px">
                <div class="panel">
                    <div class="bg-info clearfix">
                        <button type="button" class="btn btn-secondary float-right"> تغير حالة الطلب </button>
                    </div>
                </div>






                <div class="card">



                    <div class="card-header" style="padding-top: 13px;padding-bottom: 12px;padding-right:18px;width: 100%;">


                        <div class="col-8">
                            <h4> تغير حالة الطلب -
                                @foreach ($Rqserve as $Rqserv)

                                    @if ($Rqserv->account_type == 'teacher')
                                        {{ $Rqserv->subReserve->name }}
                                    @break
                                @elseif ($Rqserv->account_type == 'student')
                                    {{ $Rqserv->Stud->name }}
                                @break
                                @endif
                                @endforeach
                                <h4>
                        </div>


                    </div>

                    <!-- /.card-header -->
                    <div class="card-body">
                        <div style="margin-right: 10px">
                            <h3>تعليمات هامه </h3>
                            <ul style="margin-right: 30px ;  line-height: 2">
                                <li> عند قيامك بإرسال اشعار لطلب معين بأمكانك تعديل الاشعار ولكن لتتمكن من حذف الاشعار
                                    السابق اي سيتم ارسال اشعار جديد </li>
                                <li> سيصل اشعار للسمتخدم الذي تقدم لخدمتك في حال قبولة او رفضة او تعليقة مع العلم انه
                                    بأمكانك ارسال محتوى اشعارات خاص بك مثلاً في حالة تعليق الطلب بامكانك ارسال اشعار كتالي
                                    <span style="color: sandybrown"> || اعتذر لذالك لقد قمت بتعليق طلبك في الوقت الحالي
                                        نظراً للعدد الكبير سأقوم بمراسلتك الاسبوع المقبل انتظر اشعار مني </span>
                                </li>
                                <li> في حالة <span style="color: green"> قبول الطلب </span> تكون رسالة الاشعار الافتراضية
                                    لقد تم قبول طلبك في {{ $Rqserv->Service->title }} اما في حالة ,<span
                                        style="color:rgb(179, 161, 0) "> تعليق الطلب </span> تكون الرسالة الافتراضية لقد تم
                                    تعليق طلبك في خدمة {{ $Rqserv->Service->title }} للوقت الحالي يمكنك طلب خدمه اخرى ان
                                    اردت والغاء الطلب الحالي </span> في حالة <span style="color: red"> رفض الطلب</span>
                                    ستكون الرسالة للاسف لم استطيع قبولك في خدمه {{ $Rqserv->Service->title }} التي اقدمها
                                    يمكنك البحث عن خدمه اخرى </li>
                            </ul>
                        </div>
                        <form>

                            <div class="form-group row">
                                <label for="status" class="col-4 col-form-label mb-2"> اختر الحالة التي تريدها لطلب </label>
                                <div class="col-8">
                                    <select id="status" name="status" class="form-select">
                                        <option> </option>
                                        <option value="1" style="color:green"> قبول الطلب </option>
                                        <option value="2" style="color:rgb(179, 161, 0)"> تعليق الطلب </option>
                                        <option value="3" style="color:rgb(230, 4, 4)"> رفض الطلب </option>
                                    </select>
                                </div>
                            </div>
                            <div style="display: none">
                                <input id="Rqserve_id" class="form-control" required="required" type="text"
                                    value="{{ $Rqserv->id }}">
                            </div>
                            <div style="display: none">
                                {{-- نوع الحساب --}}
                                <input id="send" class="form-control" required="required" type="text"
                                    value="{{ auth()->user()->account_type }}">
                                <input id="send_id" class="form-control" required="required" type="text"
                                    value="{{ auth()->user()->id }}">
                                <input id="send_name" class="form-control" required="required" type="text"
                                    value="{{ auth()->user()->name }}">
                                <input id="serves" class="form-control" required="required" type="text"
                                    value=" {{ $Rqserv->Service->title }}">
                                {{-- المتلقي --}}
                                @foreach ($Rqserve as $Rqserv)
                                    @if ($Rqserv->account_type == 'teacher')
                                        <input id="req_send" class="form-control" required="required" type="text"
                                            value=" {{ $Rqserv->teacher->id }}">
                                        <input id="req_send_name" class="form-control" required="required" type="text"
                                            value=" {{ $Rqserv->teacher->name }}">
                                        <input id="req_send_type" class="form-control" required="required" type="text"
                                            value=" {{ $Rqserv->teacher->account_type }}">

                                    @break
                                @elseif ($Rqserv->account_type == 'student')
                                    <input id="req_send" class="form-control" required="required" type="text"
                                        value="{{ $Rqserv->Stud->id }}">
                                    <input id="req_send_name" class="form-control" required="required" type="text"
                                        value="{{ $Rqserv->Stud->name }}">
                                    <input id="req_send_type" class="form-control" required="required" type="text"
                                        value="{{ $Rqserv->Stud->account_type }}">
                                @break
                                @endif
                                @endforeach

                            </div>
                            <button type="button" class="btn btn-outline-secondary" id="button1">ضع اشعار مخصص لطالب
                                الخدمه</button><br><br>
                            <div class="form-group row" id="titles" style="display:none">
                                <div class="col-12">
                                    <input id="title" placeholder="ادخل هنا الاشعار المراد ارسالة الى صاحب الطلب"
                                        class="form-control" type="text" value="">
                                </div>
                            </div>

                            <br>

                            <button type="button" onclick="update({{ $Rqserv->id }})" class="btn btn-primary" id="button"
                                style="float: left; width: 100%"> ارسل الاشعار للمستخدم بإجراء الحالة المتخذ </button>
                            <br>
                            <br>
                            <br>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
    </div>

    <!-- /.container-fluid -->
    </section>


@endsection


@section('JS')



    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-46156385-1', 'cssscript.com');
        ga('send', 'pageview');
    </script>



    <!-- jQuery -->
    <script src="{{ asset('CMS/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>


    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": true,
                "autoWidth": false,
                "buttons": ["copy", "excel", "pdf", "print"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
    <script>
        function update(id) {
            Swal.fire({
                title: 'تحذير !',
                text: 'سيتم اشعار المستخدم بذالك ولن تتمكن من الغاء ذالك الاشعار ولكن يمكنك اراسل اشعار جديد له',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'الغاء',
                confirmButtonText: 'نعم ، اكمل من فضلك',
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    axios.put('/serves/Certain/Show/Accept/Satus/' + id, {
                            status: document.getElementById('status').value,
                            title: document.getElementById('title').value,
                            send: document.getElementById('send').value,
                            Rqserve_id: document.getElementById('Rqserve_id').value,
                            send_id: document.getElementById('send_id').value,
                            send_name: document.getElementById('send_name').value,
                            req_send: document.getElementById('req_send').value,
                            req_send_name: document.getElementById('req_send_name').value,
                            req_send_type: document.getElementById('req_send_type').value,
                            serves: document.getElementById('serves').value,

                        })
                        .then(function(response) {
                            // handle success
                            console.log(response);
                            toastr.success(response.data.message);
                            // document.getElementById('create-form').reset(); // تصفير قيمة الفورم سبمت
                            window.location.href =
                                "/serves/View/all/notifications/Incoming_requests/"; // التحويل والانتقال
                        })
                        .catch(function(error) {
                            // handle error
                            console.log(error);
                            toastr.error(error.response.data.message);
                        })
                        .then(function() {
                            // always executed
                        });
                }
            })
        }
    </script>

@endsection
