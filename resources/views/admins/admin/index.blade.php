@extends('admins.parent')

@section('title', 'المشرفين')
@section('bige-title', 'المشرفين')
@section('main-page', 'الرئيسية')
@section('sub-page', 'المشرفين')

@section('himen', 'menu-open')

@section('content')
<section class="content">
<div class="container-fluid">

<!-- /.row -->
<div class="row">
<div class="col-12">
<div class="card">
    <div class="card-header">
        <h3 class="card-title" style="float: right"> جدول عرض جميع المشرفين </h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive p-0">
        <table class="table table-hover text-nowrap table-bordered table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>الاسم</th>
                    <th>البريد الالكتروني</th>
                    <th>الحالة</th>
                    <th>تاريخ الانشاء </th>
                    <th> تاريخ التعديل </th>
                    <th>الاعدات</th>

                </tr>
            </thead>
            <tbody>
                @foreach ($admins as $admin)
                    {{-- @if ($admin->id != auth('admin')->id()) --}}
                    <tr>
                        <td>{{ $admin->id }}</td>
                        <td>{{ $admin->name }}</td>
                        <td>{{ $admin->email }}</td>
                        {{-- <td>@if ($admin->active) Active @else Disabled @endif </td> --}}
                        {{-- <td>{{$admin->status}}</td> بدل مستدعي الاكتف بتدعي الكولم الوهمي الي عملتو بلمودل باسم ستيتس --}}
                        <td>
                            <span class="badge @if ($admin->Active) bg-success @else bg-danger @endif ">
                                {{ $admin->Status_Keyword }} </span>
                        </td>

                        <td>{{ $admin->created_at }}</td>
                        <td>{{ $admin->updated_at }}</td>
                        <td>
                            <div class="btn-group">
                                <a href="{{ route('admins.edit', $admin->id) }}" class="btn btn-info">
                                    <i class="fas fa-edit "></i>
                                </a>

                                <a href="#" class="btn btn-danger"
                                    onclick="confirmDestroy({{ $admin->id }}, this)">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                    {{-- @endif --}}

                @endforeach


            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
</div>
</div>
</div><!-- /.container-fluid -->
</section>
@endsection

@section('JS')


<script>
    //عملية الحذف
    function confirmDestroy(id, referince) {
        //    console.log("ID:"+id)    // ->?   (id) تستعمل لفحص الكي اذا تم تمريرة او لا


        Swal.fire({
            title: 'هل انت متأكد من عملية الحذف؟',
            text: "لن تتمكن من التراجع عن هذا!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'الغاء',
            confirmButtonText: 'نعم ، احذفها!',
        }).then((result) => {
            if (result.isConfirmed) {
                destroy(id, referince); // referince لاستقبال الذس من الرابط
            }
        })
    }

    function destroy(id, referince) {
        // Make a request for a user with a given ID
        axios.delete('/admin/admins/' + id)
            .then(function(response) {
                // handle success
                console.log(response);
                referince.closest('tr')
                    .remove(); //referince في فوق في الرابط او الاشرف وليس شرك ان يسمى بي  this الذي يتبع الى كلمه  referince لحذف الصف دون الحاجة الى تحديث الصفحة بلاستناد على المتغير
                ShowMessage(response.data); // لاستقبال الجايسون من الكنترولر ببيناته رسالة النجاح والايقونة
            })
            .catch(function(error) {
                // handle error
                console.log(error); // اسقبال رسائل وبينات الخطاء
                ShowMessage(error.response.data);
            })
            .then(function() {
                // دائماً منفذة لو بدي انفذ داله معينة في كل الحالات
            });

    }

    function ShowMessage(data) {
        Swal.fire({
            icon: data.icon, // طباعة الايقونة والاعنوان بناء على البينات المستقبلة من الكنترولر
            title: data.title,
            text: data.text,
            showConfirmButton: false,
            timer: 1500
        })
    }
</script>

@endsection
