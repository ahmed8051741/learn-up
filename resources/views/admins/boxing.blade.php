@extends('admins.parent')

@section('title', 'عرض اقتراحات المستخدمين')
@section('bige-title', 'الاقتراحات والشكاوي')
@section('main-page', 'الرئيسية')
@section('sub-page', 'الاقتراحات والشكاوي')

@section('B', 'active')


@section('content')
    <section class="content">
        <div class="container-fluid">

            <!-- /.row -->
            <div class="row" dir="rtl">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title" style="float: right"> جدول عرض اقتراحات وشكاوي المستخدمين </h3>
                        </div>
                        <!-- /.card-header -->
                   {{-- <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped main-content"
                            style="text-align: center">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                         <th> تفاصيل الاقتراح او الشكوى </th>
                                        <th>تاريخ الانشاء </th>
                                        <th> وقت الاقتراح </th>
                                        <th>الاجراءات</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($boxing as $boxin)
                                        <tr>
                                            <td>{{ $boxin->id }}</td>
                                            <td>{{ $boxin->suggestion }}</td>


                                            <td>{{ $boxin->created_at }}</td>
                                            <td>{{ $boxin->created_at->diffForHumans() }}</td>
                                             <td>
                                                <div class="btn-group">
                                                    <a href="#" class="btn btn-danger"
                                                        onclick="confirmDestroy({{ $boxin->id }}, this)">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach


                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div> --}}

                        <div class="card-body table-responsive p-1" style="margin: 15px; margin-left: 30px">
                        <table id="example1" class="table table-bordered table-striped main-content "
                            style="text-align: center">
                            <thead dir="rtl" >
                                <tr>
                                                <th>#</th>
                                         <th> تفاصيل الاقتراح او الشكوى </th>
                                        <th>تاريخ الانشاء </th>
                                        <th> وقت الاقتراح </th>
                                        <th>الاجراءات</th>

                                </tr>
                            </thead>
                            <tbody dir="rtl">
                                 @foreach ($boxing as $boxin)
                                        <tr>
                                            <td>{{ $boxin->id }}</td>
                                            <td>{{ $boxin->suggestion }}</td>


                                            <td>{{ $boxin->created_at }}</td>
                                            <td>{{ $boxin->created_at->diffForHumans() }}</td>
                                             <td>
                                                <div class="btn-group">
                                                    <a href="#" class="btn btn-danger"
                                                        onclick="confirmDestroy({{ $boxin->id }}, this)">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach

                            </tbody>
                            <hr>
                        </table>

                    </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection

@section('JS')




    <!-- jQuery -->
    <script src="{{ asset('CMS/plugins/jquery/jquery.min.js') }}"></script>


    <script src="{{ asset('CMS/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dist/js/demo.js') }}"></script>
    <!-- Page specific script -->
    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": true,
                "autoWidth": true,
             }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": false,
            });
        });
    </script>


    <script>
        //عملية الحذف
        function confirmDestroy(id, referince) {
            //    console.log("ID:"+id)    // ->?   (id) تستعمل لفحص الكي اذا تم تمريرة او لا
            Swal.fire({
                title: 'هل انت متأكد من عملية الحذف؟',
                text: "لن تتمكن من التراجع عن هذا!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'الغاء',
                confirmButtonText: 'نعم ، احذفها!',
            }).then((result) => {
                if (result.isConfirmed) {
                    destroy(id, referince); // referince لاستقبال الذس من الرابط
                }
            })
        }

        function destroy(id, referince) {
            // Make a request for a user with a given ID
            axios.delete('/register/teacher/' + id)
                .then(function(response) {
                    // handle success
                    console.log(response);
                    referince.closest('tr')
                        .remove(); //referince في فوق في الرابط او الاشرف وليس شرك ان يسمى بي  this الذي يتبع الى كلمه  referince لحذف الصف دون الحاجة الى تحديث الصفحة بلاستناد على المتغير
                    ShowMessage(response.data); // لاستقبال الجايسون من الكنترولر ببيناته رسالة النجاح والايقونة
                })
                .catch(function(error) {
                    // handle error
                    console.log(error); // اسقبال رسائل وبينات الخطاء
                    ShowMessage(error.response.data);
                })
                .then(function() {
                    // دائماً منفذة لو بدي انفذ داله معينة في كل الحالات
                });

        }

        function ShowMessage(data) {
            Swal.fire({
                icon: data.icon, // طباعة الايقونة والاعنوان بناء على البينات المستقبلة من الكنترولر
                title: data.title,
                text: data.text,
                showConfirmButton: false,
                timer: 1500
            })
        }

    </script>

@endsection
