<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Pages = Page::all();
        return response()->view('admins.pages.index', ['Pages' => $Pages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->view('admins.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:3|max:30',
            'name_admin' => 'required|string|min:3|max:30',
            'active' => 'required|boolean',
            'compose_textarea' => 'required|string',
         ]);

        if (!$validator->fails()) {
            //TODO: CREATE NEW CATEGORY
            $Pages = new Page();
            $Pages->name = $request->get('name');
            $Pages->name_admin = $request->get('name_admin');
            $Pages->compose_textarea = $request->get('compose_textarea');
            $Pages->active = $request->get('active');
             $isSaved = $Pages->save();
            return response()->json([
                'message' => $isSaved ? "Saved Successfully" : "Failed to save"
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            //VALIDATION FAILED
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
     return response()->view('admins.pages.edit', ['Page' => $page]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:3|max:30',
            'name_admin' => 'required|string|min:3|max:30',
            'active' => 'required|boolean',
            'compose_textarea' => 'required|string',
         ]);

        if (!$validator->fails()) {
            $page->name = $request->get('name');
            $page->name_admin = $request->get('name_admin');
            $page->compose_textarea = $request->get('compose_textarea');
            $page->active = $request->get('active');
            $isUpdated = $page->save;
             return response()->json([
                'message' => $isUpdated ? "Updated Successfully" : "Failed to update"
            ], $isUpdated ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        //
        $isDelete = $page->delete();
        if ($isDelete) {
            return response()->json(
                ['title' => 'Success', 'text' => 'Page Deleted Successfully', 'icon' => 'success'],
                Response::HTTP_OK);
        } else {
            return response()->json(
                ['title' => 'Failed', 'text' => 'Page Deleted Failed', 'icon' => 'error'],
                Response::HTTP_BAD_REQUEST);
        }
    }
}
