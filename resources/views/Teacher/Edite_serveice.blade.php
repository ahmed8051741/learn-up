@extends('layout.parent')


@section('Title', 'تعديل الخدمه')

@section('CSS')

<link href="{{ asset('assets/css/Star.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/profile.css') }}" rel="stylesheet">
<style>
    .custom-file-input {
        position: relative;
        z-index: 2;
        width: 100%;
        height: calc(2.25rem + 2px);
        margin: 0;
        opacity: 0;
    }

    .custom-file-label {
        position: absolute;
        font-family: 'cairo';
        top: 0;
        left: 0;
        right: 0;
        z-index: 1;
        height: calc(2.25rem + 2px);
        padding: .375rem .75rem;
        font-weight: 400;
        line-height: 1.5;
        color: #495057;
        background-color: rgb(235, 235, 235);
        border: 1px solid #ced4da;
        border-radius: .25rem;
    }


    .custom-file {
        position: relative;
        display: inline-block;
        width: 100%;
        height: calc(2.25rem + 2px);
        margin-bottom: 0;
    }

    .custom-file-input {
        position: relative;
        z-index: 2;
        width: 100%;
        height: calc(2.25rem + 2px);
        margin: 0;
        opacity: 0;
    }

    .custom-file-label {
        position: absolute;
        font-family: 'cairo';
        top: 0;
        left: 0;
        right: 0;
        z-index: 1;
        height: calc(2.25rem + 2px);
        padding: .375rem .75rem;
        font-weight: 400;
        line-height: 1.5;
        color: #495057;
        background-color: rgb(235, 235, 235);
        border: 1px solid #ced4da;
        border-radius: .25rem;
    }

    #header {
        visibility: hidden;
        margin-top: -50px;
    }

</style>

@endsection
@section('dd')
<h2 class="logo" style="font-family: Al-Jazeera-Arabic"><a href="{{ route('showHome') }}"
        style="font-family: Al-Jazeera-Arabic; color: aliceblue"> Learn <span
            style="font-family: Al-Jazeera-Arabic;font-weight: 760;color: #Fd4;"> up
        </span></a></h2>
@endsection
@section('con', 'display: none')



@section('content')


@section('Top')

<section id="topbar">
<div class="container d-flex justify-content-center justify-content-md-between" id="topb">
    <div class="contact-info d-flex align-items-center">
        <a href="mailto:ahmed8051741@gmail.com" class="visitor"> &nbsp; الدعم الفني عبر البريد الالكتروني </a>
    </div>
    <div class="d-flex flex-row align-items-center feed-text px-2" dir="ltr">
        &nbsp;&nbsp;
        <div class="d-flex flex-column flex-wrap ml-2"><span class="font-weight-bold">
                <div class="dropdown">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" style="color: #ffffff;"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="rounded-circle" src="{{ asset('assets/img/my.jpeg') }}" width="42">
                        &nbsp;&nbsp;
                        {{ auth('teacher')->user()->name }} &nbsp;</a>

                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="{{ route('showprofile') }}">الملف الشخصي</a>
                        <hr style="margin-top:0.8px;margin-bottom:0.8px">
                        <a class="dropdown-item" href="{{ route('services.create') }}">اضافة خدمة جديد</a>
                        <a class="dropdown-item" href="{{ route('control') }}">لوحة القيادة</a>
                        <a class="dropdown-item" href="{{ route('logout_log') }}">تسجيل الخروج</a>
                    </div>
                </div>
            </span>

        </div>
    </div>
</div>
</section>
@endsection

<div class="container bootstrap snippets bootdey">
<div class="row">
<div class="profile-nav col-md-3">
    <div class="panel">
        <div class="user-heading round">
            <a href="#">
                <img src="{{ asset('storage/image_profile/' . auth()->user()->image) }}" alt="">
            </a>
            <h1> {{ auth()->user()->name }} </h1>
            <p>{{ auth()->user()->email }} </p>
        </div>

        <ul class="nav">
            <li><a href="{{ route('control') }}"><i class="fas fa-tachometer-alt"> &nbsp;</i> دفقة
                    القيادة </a></li>
            <li><a href="{{ route('showprofile') }}"><i class="fa fa-user"> &nbsp;</i> الملف الشخصي </a></li>
            <li class="active"><a href="{{ route('services.index') }}"><i class="fas fa-indent"> &nbsp;</i>
                    الخدمات
                </a></li>
            <li><a href="{{ route('password_edit') }}"><i class="fas fa-lock"> &nbsp;</i> تعديل كلمه
                    المرور</a></li>
            <li><a href="{{ route('control') }}"><i class="fa fa-calendar"> &nbsp;</i> العودة الى لوحة التحكم
                </a></li>
            <li><a href="{{ route('logout_log') }}"><i class="fas fa-sign-out-alt"> &nbsp;</i> تسجيل
                    الخروج</a></li>
        </ul>
    </div>
</div>


<div class="profile-info col-md-9" style="margin-bottom: 60px">
    <div class="panel">
        <div class="bg-info clearfix">
            <button type="button" class="btn btn-secondary float-right"> الخدمات واعدادتها </button>
        </div>
    </div>


    <div class="card">
        <div class="card-header" style="padding-top: 13px;padding-bottom: 12px;padding-right:18px;width: 100%;">
            <div class="col-4" style="float: left; "> <a href="{{ route('services.index') }}">
                    <button type="button" class="btn btn-outline-primary" style="width: 100%"> <i
                            class="fas fa-indent">&nbsp;</i> عرض الخدمات المضافة </button>
                </a>
            </div>

            <div class="col-8">
                <h4> تعديل بينات الخدمه </h3>
            </div>


        </div>


        <!-- /.card-header -->
        <div class="card-body">

            <form>
                <div class="form-group row">
                    <label for="name" class="col-4 col-form-label mb-2"> عنوان الخدمة </label>
                    <div class="col-8">
                        <input id="name" placeholder="عنوان للخدمة التي ستقدمها" class="form-control"
                            required="required" type="text" value="{{ $service->title }}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="price" class="col-4 col-form-label mb-2"> سعر الخدمه التي تقدمها بالشيكل
                    </label>
                    <div class="col-8">
                        <input id="price" placeholder="ادخل السعر هنا بالشيكل" class="form-control"
                            required="required" type="text" value="{{ $service->price }}">
                    </div>
                </div>

                <div class=" form-group row">
                    <label for="select" class="col-4 col-form-label mb-2"> تصنيف الخدمه </label>
                    <div class="col-8">
                        <select id="select" name="select" class="form-select">
                            <option value=" {{ $service->category->id }} "> {{ $service->category->name }}
                            </option>
                            @foreach ($categorise as $categoris)
                                <option value=" {{ $categoris->id }} "> {{ $categoris->name }}
                                </option>
                            @endforeach

                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="city" class="col-4 col-form-label mb-2"> المنطقة التي تتيح فيها الخدمة
                    </label>
                    <div class="col-8">
                        <select id="city" name="city" class="form-select">
                            <option value=" {{ $service->City->id }}  ">{{ $service->City->name }}
                            </option>
                            @foreach ($city as $cities)
                                <option value=" {{ $cities->id }} "> {{ $cities->name }} </option>
                            @endforeach

                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="select_CAT" class="col-4 col-form-label mb-2"> الفئة المستهدفة </label>
                    <div class="col-8">
                        <select id="select_CAT" name="select" class="form-select">
                            <option value=" {{ $service->select_CAT }} "> {{ $service->select_CAT }}
                            </option>
                            <option value="المرحلة الاعدادية"> المرحلة الاعدادية </option>
                            <option value="الثانوية العامه"> الثانوية العامه </option>
                            <option value="المرحلة الثانوية بدون توجيهي"> المرحلة الثانوية بدون توجيهي</option>
                            <option value="الثانوية العامه (توجيهي)"> الثانوية العامه (توجيهي) </option>
                            <option value="المرحلة الاساسية و الاعدادية"> المرحلة الاساسية و الاعدادية </option>
                            <option value="المرحلة الاساسية و الاعدادية والثانوية"> المرحلة الاساسية و الاعدادية
                                والثانوية</option>
                            <option value="المرحلة الاعدادية والثانوية"> المرحلة الاعدادية والثانوية</option>
                            <option value="المرحلة الجامعية"> المرحلة الجامعية </option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="describe" class="col-4 col-form-label mb-2"> </label>
                    <div class="col-8">
                        <input id="teacher_id" type="text" value="{{ auth()->user()->id }}"
                            style="visibility: hidden" disabled>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="describe" class="col-4 col-form-label mb-2"> تفاصيل الخدمه </label>
                    <div class="col-8">
                        <textarea id="describe" cols="40" rows="4"
                            class="form-control"> {{ $service->describe }}</textarea>
                    </div>
                </div>
                <br>
                <br>

                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="category_image" accept="image/*,.">
                    <label class=" custom-file-label" for="category_image">
                        {{ $service->image }}
                    </label>
                </div>

                <br>
                <br>
                <br>

                <button type="button" onclick="update({{ $service->id }})" class="btn btn-primary"
                    id="button" style="float: left; width: 100%"> تعديل الخدمه </button>
                <br>
                <br>
                <br>
            </form>
        </div>

        <!-- /.card-body -->
    </div>

</div>
</div>
</div>


@endsection


@section('JS')

<script>
    function update(id) {
        let formData = new FormData();
        formData.append('_method', 'PUT');
        formData.append('name', document.getElementById('name').value);
        formData.append('price', document.getElementById('price').value);
        formData.append('select', document.getElementById('select').value);
        formData.append('city', document.getElementById('city').value);
        formData.append('teacher_id', document.getElementById('teacher_id').value);
        formData.append('describe', document.getElementById('describe').value);
        formData.append('select_CAT', document.getElementById('select_CAT').value);
        formData.append('category_image', document.getElementById('category_image').files[0]);

        axios.post('/profile/control/services/' + id, formData)
            .then(function(response) {
                // handle success
                console.log(response);
                toastr.success(response.data.message);
                // document.getElementById('create-form').reset();
                setTimeout(function() {
                    window.location.href = "/profile/control/services";
                }, 1500);

            })
            .catch(function(error) {
                // handle error
                console.log(error);
                toastr.error(error.response.data.message);
            })
            .then(function() {
                // always executed
            });
    }
</script>




@endsection
