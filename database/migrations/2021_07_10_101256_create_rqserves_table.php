<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRqservesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rqserves', function (Blueprint $table) {
            $table->id();
            $table->string('pay');
            $table->string('time');
            $table->string('date');
            $table->string('account_type');
            $table->string('Status')->default('0');
            $table->string('reade')->default('0');

            $table->foreignId('teacher_id');
            $table->foreign('teacher_id')->on('teachers')->references('id');

            $table->foreignId('service_id');
            $table->foreign('service_id')->on('services')->references('id');

            $table->foreignId('teacher_sd')->nullable();
            $table->foreign('teacher_sd')->on('teachers')->references('id');

            $table->foreignId('student_id')->nullable();
            $table->foreign('student_id')->on('students')->references('id');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rqserves');
    }
}
