<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rqorder extends Model
{
    use HasFactory, SoftDeletes;

    public function subRqorder()
    {
        return $this->hasMany(Notice::class, 'Rqorder_id', 'id');
    }


    public function Teachers()
    {
        return $this->belongsTo(Teacher::class, 'teacher_id', 'id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id')->withTrashed();
    }

    public function Studs()
    {
        return $this->belongsTo(Student::class, 'student_id', 'id');
    }
}
