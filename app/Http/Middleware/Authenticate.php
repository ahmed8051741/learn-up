<?php

namespace App\Http\Middleware;

use App\Models\Page;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            // if ($request->has('guard')) {
            //     switch ($request->get('guard')) {
            //         case 'admin':
            //             return route('loginadmin', 'admin');

            //         case 'teacher':
            //             return route('login', 'student');

            //         case 'teacher':
            //             return route('login', 'teacher');
            //     }
            // }
            // dd($request->url());
            if (strpos($request->url(), 'admin')) {
                return route('loginadmin', 'admin');
            } else {
                return route('Authorization');
            }
        }
    }
}
