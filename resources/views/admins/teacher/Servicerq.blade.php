@extends('admins.parent')

@section('title', 'جميع الطلبات')
@section('bige-title', 'المعلمين')
@section('main-page', 'جميع الطلبات')
@section('sub-page', 'جميع الطلبات')

@section('user', 'menu-open')


@section('content')
<section class="content">
<div class="container-fluid">
<!-- /.row -->
<div class="row">
<div class="col-12">
<div class="card">
    <div class="card-header">
        <h3 class="card-title" style="float: right ; margin-top: 10px">

            جميع الطلبات الواردة للخدمة
            @foreach ($Rqserve as $Rqserv)
                {{ $Rqserv->Service->title }}
            @break
            @endforeach
        </h3>
        <div class="card-tools" style="float: left">

            <?php
            $i = 0;
            ?>
            @foreach ($Rqserve as $Rqserv)
                @if ($Rqserv->Status == 1)
                    <?php
                    $sum = $Rqserv->Service->price;
                    $i = $i + $sum;
                    ?>
                @endif
            @endforeach

        </div>
        <button type="button" class="btn btn-outline-secondary" style="float: left; ">أجمالي الدخل
            {{ $i }}
        </button>

    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive p-0">
        <table class="table table-hover text-nowrap table-bordered table-striped"
            style="text-align: center;font-size: 14px">
            <thead>
                <tr>
                    <th>#</th>
                    <th> اسم المتقدم للخدمه </th>
                    <th> اليوم المناسب </th>
                    <th> الساعة التقريبية المناسبة </th>
                    <th> الوقت المنقضي للطلب </th>
                    <th> الحالة </th>
                    <th> القيمه المالية </th>
                    <th> اجراءات اخرى </th>
                </tr>
            </thead>
            <tbody dir="rtl">
                @foreach ($Rqserve as $Rqserv)

                    <tr>
                        <td>{{ $Rqserv->id }}</td>
                        <td>
                            @if ($Rqserv->account_type == 'teacher')
                                {{ $Rqserv->subReserve->name }}
                            @elseif ($Rqserv->account_type == 'student')
                                {{ $Rqserv->Stud->name }}
                            @endif


                        </td>
                        <td> {{ $Rqserv->date }}</td>
                        <td>{{ $Rqserv->time }}</td>

                        <td>{{ $Rqserv->created_at->diffForHumans() }}</td>
                        <td>
                            @if ($Rqserv->Status == 0)

                                <span class="badge badge-info" style="font-size: 13px"> لم يتم اتخاذ
                                    اي
                                    اجراء </span>
                            @elseif ($Rqserv->Status ==1)
                                <span class="badge badge-success" style="font-size: 13px"> تم القبول
                                </span>
                            @elseif ($Rqserv->Status ==2)
                                <span class="badge badge-warning" style="font-size: 13px"> تم
                                    التعليق
                                </span>
                            @elseif ($Rqserv->Status ==3)
                                <span class="badge badge-danger" style="font-size: 13px"> تم الرفض
                                </span>

                            @endif

                        </td>

                        <td>
                            @if ($Rqserv->Status == 1)
                                {{ $Rqserv->Service->price }}
                            @else
                                0
                            @endif

                        </td>
                        <td>
                            <div class="btn-group">

                                &nbsp;
                                <form action="{{ url('/serves/destroy/' . $Rqserv->id) }}"
                                    method="post">
                                    @csrf
                                    @method('Delete')
                                    <button type="submit" class="btn btn-danger"><i
                                            class="far fa-trash-alt"
                                            style="font-size: 22px; "></i></button>
                                </form>

                            </div>

                        </td>
                    </tr>

                @endforeach

            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
</div>
</div>
</div><!-- /.container-fluid -->
</section>
@endsection

@section('JS')

<script>
    //عملية الحذف
    function confirmDestroy(id, referince) {
        //    console.log("ID:"+id)    // ->?   (id) تستعمل لفحص الكي اذا تم تمريرة او لا
        Swal.fire({
            title: 'هل انت متأكد من عملية الحذف؟',
            text: "لن تتمكن من التراجع عن هذا!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'الغاء',
            confirmButtonText: 'نعم ، احذفها!',
        }).then((result) => {
            if (result.isConfirmed) {
                destroy(id, referince); // referince لاستقبال الذس من الرابط
            }
        })
    }

    function destroy(id, referince) {
        // Make a request for a user with a given ID
        axios.delete('/profile/control/services/' + id)
            .then(function(response) {
                // handle success
                console.log(response);
                referince.closest('tr')
                    .remove(); //referince في فوق في الرابط او الاشرف وليس شرك ان يسمى بي  this الذي يتبع الى كلمه  referince لحذف الصف دون الحاجة الى تحديث الصفحة بلاستناد على المتغير
                ShowMessage(response.data); // لاستقبال الجايسون من الكنترولر ببيناته رسالة النجاح والايقونة
            })
            .catch(function(error) {
                // handle error
                console.log(error); // اسقبال رسائل وبينات الخطاء
                ShowMessage(error.response.data);
            })
            .then(function() {
                // دائماً منفذة لو بدي انفذ داله معينة في كل الحالات
            });

    }

    function ShowMessage(data) {
        Swal.fire({
            icon: data.icon, // طباعة الايقونة والاعنوان بناء على البينات المستقبلة من الكنترولر
            title: data.title,
            text: data.text,
            showConfirmButton: false,
            timer: 1500
        })
    }
</script>


@endsection
