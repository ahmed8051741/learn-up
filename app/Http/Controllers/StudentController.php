<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $student = Student::where('id', '!=', auth('student')->id())->get();
        return response()->view('Register', ['student' => $student]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->view('Register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator(
            $request->all(),
            [
                'name' => 'required|string|min:8',
                'email' => 'required|string|min:3|max:90|email',
                'password' => 'required|string|min:6|max:16|confirmed',

            ],
            [
                'name.required' => 'يجب عليك ادخال اسمك ثلاثي',
                'name.min' => 'يجب عليك ادخال اسمك ثلاثي',
                'email.required' => 'يجب عليك تعبئة حقل البريد الالكتروني',
                'email.min' => 'تأكد من صيغة البريد الالكتروني',
                'email.max' => 'تأكد من صيغة البريد الالكتروني',
                'email.email' => 'الرجاء ادخال بريد الكتروني صالح',
                'password.required' => 'عذاً عليك تعين كلمة مرور تتكون من 6-18 حرفاً ',
                'password.min' => 'قم بوضع كلمة مرور ما بين 6-16 رقم وحرف ورمز',
                'password.max' => 'قم بوضع كلمة مرور ما بين 6-16 رقم وحرف ورمز',
                'password.confirmed' => 'كلمتان المرور غير متطابقتان',
            ]
        );
        if (!$validator->fails()) {

            $Student = new Student();
            $Student->name = $request->get('name');
            $Student->email = $request->get('email');
            $Student->password = Hash::make($request->get('password'));
            $isSaved = $Student->save();

            return response()->json([
                'message' => $isSaved ? "تم انشاء حسابك بنجاح ...حظاً سعيداً" : "عذاً تعذر انشاء الحساب لسبب مجهول"
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            //VALIDATION FAILED
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return response()->view('Show_profile_std', ['student' => $student]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        return response()->view('admins.std.edit', ['student' => $student]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:3|max:30',
            'email' => 'required|email',
            'active' => 'required|boolean',
        ]);
        if (!$validator->fails()) {
            $student->name = $request->get('name');
            $student->email = $request->get('email');
            $student->active = $request->get('active');
            $isSaved = $student->save();
            return response()->json([
                'message' => $isSaved ? "تمت التحديث بنجاح" : "فشل التحديث"
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json(['message' => $validator->getMessageBag()->first()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    { {
            //
            $isDelete = $student->delete();
            if ($isDelete) {
                return response()->json(
                    ['title' => 'Success', 'text' => 'تم ازاله المستخدم بنجاح', 'icon' => 'success'],
                    Response::HTTP_OK
                );
            } else {
                return response()->json(
                    ['title' => 'Failed', 'text' => 'حدث خطاء اثناء تنفيذ الاجراء', 'icon' => 'error'],
                    Response::HTTP_BAD_REQUEST
                );
            }
        }
    }
}
