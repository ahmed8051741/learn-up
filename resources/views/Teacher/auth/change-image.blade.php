@extends('layout.parent')


@section('Title', 'تعديل صورة ملفك الشخصي')

@section('CSS')

    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

    <link href="{{ asset('assets/css/Star.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/profile.css') }}" rel="stylesheet">
    <style>
        .custom-file {
            position: relative;
            display: inline-block;
            width: 100%;
            height: calc(2.25rem + 2px);
            margin-bottom: 0;
        }

        .custom-file-input {
            position: relative;
            z-index: 2;
            width: 100%;
            height: calc(2.25rem + 2px);
            margin: 0;
            opacity: 0;
        }

        .custom-file-label {
            position: absolute;
            font-family: 'cairo';
            top: 0;
            left: 0;
            right: 0;
            z-index: 1;
            height: calc(2.25rem + 2px);
            padding: .375rem .75rem;
            font-weight: 400;
            line-height: 1.5;
            color: #495057;
            background-color: rgb(235, 235, 235);
            border: 1px solid #ced4da;
            border-radius: .25rem;
        }


        * {
            font-family: 'cairo';
        }

        .visitor {
            color: white;
            font-size: 15px;

        }


        #header {
            visibility: hidden;
            margin-top: -50px;
        }

    </style>
@endsection

@section('dd')
    <h2 class="logo" style="font-family: Al-Jazeera-Arabic"><a href="{{ route('showHome') }}"
            style="font-family: Al-Jazeera-Arabic; color: aliceblue"> Learn <span
                style="font-family: Al-Jazeera-Arabic;font-weight: 760;color: #Fd4;"> up
            </span></a></h2>
@endsection
@section('con', 'display: none')



@section('content')




    <div class="container bootstrap snippets bootdey">
        <div class="row">
            <div class="profile-nav col-md-3">
                <div class="panel">
                    <div class="user-heading round">
                        <a href="#">

                            <img src=" {{ asset('storage/image_profile/' . auth()->user()->image) }}">

                        </a>
                        <h1> {{ auth()->user()->name }} </h1>
                        <p>{{ auth()->user()->email }} </p>
                    </div>

                    <ul class="nav">
                        <li><a href="{{ route('showprofile') }}"><i class="fa fa-user"> &nbsp;</i>
                                الملف
                                الشخصي</a></li>



                        @if (auth()->user()->id == auth('teacher')->id())
                            <li><a href="{{ route('show.teacher', auth()->user()->id) }}"><i class="fas fa-eye">
                                        &nbsp;</i>
                                    عرض ملفك الشخصي كما يظهر للاخرين
                                </a></li>

                        @else
                            <li><a href="{{ route('show.std', auth()->user()->id) }}"><i class="fas fa-eye">
                                        &nbsp;</i>
                                    عرض ملفك الشخصي كما يظهر للاخرين
                                </a></li>
                        @endif




                        <li><a href="{{ route('Edite_Profile') }}"><i class="fa fa-edit"> &nbsp;</i> تعديل الملف
                                الشخصي</a></li>

                        <li class="active"><a href="{{ route('image.index') }}"><i class="far fa-images"> &nbsp;</i>
                                تعديل صورة الملف
                                الشخصي</a></li>
                        <li><a href="{{ route('password_edit') }}"><i class="fas fa-lock"> &nbsp;</i> تعديل كلمه
                                المرور</a></li>
                        <li><a href="{{ route('control') }}"><i class="fa fa-calendar"> &nbsp;</i> العودة الى
                                لوحة
                                التحكم
                            </a></li>
                        <li><a href="{{ route('logout_log') }}"><i class="fas fa-sign-out-alt"> &nbsp;</i> تسجيل
                                الخروج</a></li>

                    </ul>
                </div>
            </div>
            <div class="profile-info col-md-9">
                <div class="panel">
                    <div class="bg-info clearfix">
                        <button type="button" class="btn btn-secondary float-right"> صورة الملف الشخصي </button>
                    </div>
                </div>
                <div class="panel">

                    <div class="panel-body bio-graph-info">
                        <div style="margin: 5px; margin-right: 22px ; margin-top: 20px">
                            <h4> تعديل صورة الملف الشخصي الخاصة بك </h4>
                            <div align="center">
                                <hr width="96%">
                            </div>
                            <div class="row" style="margin: 5px;margin-top: 25px">

                                <div class="row">
                                    <div class="col-md-12">

                                        <form>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="images">
                                                <label class="custom-file-label" for="images"> انقر لاضافة صورة
                                                    جديدة لملفك الشخصي
                                                </label>
                                            </div>
                                            {{-- -------------------------------------------- --}}






                                            <br>
                                            <br>
                                            <br>



                                            <button type="button" onclick="update({{ auth()->id() }})"
                                                class="btn btn-primary" id="button" style="float: left; width: 100%"> تحديث
                                                صورة الملف الشخصي </button>


                                            <br>
                                            <br> <br>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div>

                    </div>
                </div>
            </div>
        </div>

        <br>
        <br>
        <br>

    </div>

@endsection


@section('JS')
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-46156385-1', 'cssscript.com');
        ga('send', 'pageview');
    </script>



@section('JS')

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
    </script>



    <script>
        function update(id) {
            let formData = new FormData();
            formData.append('_method', 'PUT');
            formData.append('images', document.getElementById('images').files[0]);
            axios.post('/serves/update/image/' + id, formData)
                .then(function(response) {
                    // handle success
                    console.log(response);
                    toastr.success(response.data.message);
                    // document.getElementById('create-form').reset();
                    // window.location.href = "/profile";
                    // window.location.reload(10000);
                    setTimeout(function() {
                        window.location.href = '/profile';
                    }, 1500);

                }, 10000)
                .catch(function(error) {
                    // handle error
                    console.log(error);
                    toastr.error(error.response.data.message);
                })
                .then(function() {
                    // always executed
                });

        }
    </script>

@endsection
