@extends('layout.parent')


@section('Title', 'معاينه خدمتك')

@section('CSS')

    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="{{ asset('assets/css/Star.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/profile.css') }}" rel="stylesheet">
    <style>
        #header {
            visibility: hidden;
            margin-top: -50px;
        }

        * {
            font-family: 'cairo';
        }

        .bg-white {
            background-color: #ffffff !important;
        }

        .visitor {
            color: white;
            font-size: 15px;


        }

        .row {
            display: flex;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }

        @media (min-width: 768px) {
            .col-md-12 {
                flex: 0 0 100%;
                max-width: 100%;
            }

            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                font-family: "Al-Jazeera-Arabic";
            }

            .h4,
            h4 {
                font-size: 1.5rem;
            }

            .form-group {
                margin-bottom: 5px;
                margin-top: 5px
            }

            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                margin-top: 0;
                margin-bottom: .5rem;
            }

        }

        .card {
            margin-top: 12px
        }

        .form-group {
            margin-bottom: 1rem;
        }

        h4 {
            display: block;
            margin-block-start: 1.33em;
            margin-block-end: 1.33em;
            margin-inline-start: 0px;
            margin-inline-end: 0px;

        }


        /* .btn-group>.btn-group:not(:last-child)>.btn, .btn-group>.btn:not(:last-child):not(.dropdown-toggle) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    border-top-right-radius: 4px;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    border-bottom-right-radius: 4px;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                }

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                .btn-group>.btn-group:not(:first-child)>.btn, .btn-group>.btn:nth-child(n+3), .btn-group>:not(.btn-check)+.btn {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         border-top-left-radius:4px;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         border-bottom-left-radius:4px;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                } */
        .dropdown-menu {
            text-align: right;
            right: 0px;
            min-width: 11rem;
        }

        .pro1 {
            border-top-right-radius: 0px;
            border-bottom-right-radius: 0px;
            border-top-left-radius: 0px;
            border-bottom-left-radius: 0px;
        }

        .pro {
            border-top-right-radius: 0px;
            border-bottom-right-radius: 0px;


        }

        h4 {
            margin-block-start: 0.33em;
            margin-block-end: 0.33em;
            margin-inline-start: 0px;
            margin-inline-end: 0px;
        }

        .table>:not(caption)>*>* {
            padding: .5rem .5rem;
            padding-top: 20px;
        }

    </style>
@endsection

@section('dd')
    <h2 class="logo" style="font-family: Al-Jazeera-Arabic"><a href="{{ route('showHome') }}"
            style="font-family: Al-Jazeera-Arabic; color: aliceblue"> Learn <span
                style="font-family: Al-Jazeera-Arabic;font-weight: 760;color: #Fd4;"> up
            </span></a></h2>
@endsection
@section('con', 'display: none')



@section('content')

    <div class="container bootstrap snippets bootdey">
        <div class="row">
            <div class="profile-nav col-md-3">
                <div class="panel">
                    <div class="user-heading round">
                        <a href="#">
                            <img src="{{ asset('storage/image_profile/' . auth()->user()->image) }}" alt="">
                        </a>
                        <h1> {{ auth()->user()->name }} </h1>
                        <p>{{ auth()->user()->email }} </p>
                    </div>

                    <ul class="nav">
                        <li><a href="{{ route('control') }}"><i class="fas fa-tachometer-alt">
                                    &nbsp;</i>
                                دفقة القيادة </a></li>
                        <li><a href="{{ route('showprofile') }}"><i class="fa fa-user"> &nbsp;</i> الملف الشخصي </a>
                        </li>

                        @if (auth()->user()->id == auth('teacher')->id())
                            <li class="active"><a href="{{ route('services.index') }}"><i class="fab fa-servicestack">
                                        &nbsp;</i>
                                    الخدمات
                                </a></li>

                            <li><a href="{{ route('rqserve_all') }}"><i class="fas fa-retweet"> &nbsp;</i>
                                    الطلبات الواردة
                                </a></li>
                        @else
                            <li><a href="{{ route('order.index') }}"><i class="fab fa-servicestack"> &nbsp;</i>
                                    الطلبات
                                </a></li>


                            <li><a href="{{ route('rqserve_allstd') }}"><i class="fas fa-retweet"> &nbsp;</i>
                                    العروض المقدمه
                                </a></li>
                        @endif
                        <li><a href="{{ route('password_edit') }}"><i class="fas fa-lock"> &nbsp;</i> تعديل كلمه
                                المرور</a></li>



                        <li><a href="{{ route('logout_log') }}"><i class="fas fa-sign-out-alt"> &nbsp;</i> تسجيل
                                الخروج</a></li>
                    </ul>
                </div>
            </div>


            <div class="profile-info col-md-9" style="margin-bottom: 60px">
                <div class="panel">
                    <div class="bg-info clearfix">
                        <button type="button" class="btn btn-secondary float-right"> الخدمات واعدادتها </button>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header" style="padding-top: 13px;padding-bottom: 12px;padding-right:18px;width: 100%;">
                        <div class="col-4" style="float: left; "> <a href="{{ route('services.create') }}">
                                <button type="button" class="btn btn-outline-primary" style="width: 100%"> <i
                                        class="far fa-plus-square">&nbsp;</i> اضف خدمة جديدة </button>
                            </a>
                        </div>

                        <div class="col-8">
                            <h4> معاينة الخدمة التي تحمل اسم {{ $Service->title }}</h3>
                        </div>


                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="feed p-2">
                                <div class="bg-white border mt-2">
                                    <div>
                                        <div
                                            class="d-flex flex-row justify-content-between align-items-center p-2 border-bottom">
                                            <div class="d-flex flex-row align-items-center feed-text px-2"><img
                                                    class="rounded-circle" src="assets/img/my.jpeg" width="47">
                                                &nbsp;&nbsp;
                                                <div class="d-flex flex-column flex-wrap ml-2"><span
                                                        class="font-weight-bold">
                                                        {{ $Service->Teacher->name }}</span> <span
                                                        class="text-black-50 time"> منذ
                                                        &nbsp; 40 دقيقة | &nbsp;

                                                        <span class="font-weight-bold"> تصنيف الخدمة
                                                            :{{ $Service->category->name }}</span>
                                                    </span>

                                                </div>
                                            </div>
                                            <div class="feed-icon px-2"><i class="fa fa-ellipsis-v text-black-50"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-2 px-3">
                                        <span>
                                            {{ $Service->describe }}
                                        </span>
                                    </div>
                                    <div style="margin-right: 19px; color: rgb(26, 91, 231)">
                                        متاحة في <span> : {{ $Service->City->name }} </span>
                                        <br>
                                        &nbsp;
                                    </div>
                                    <div style="text-align: center">
                                        <img src="{{ asset('storage/images/' . $Service->image) }}" alt="Photo of Blog"
                                            width="99.8%">
                                        <div class="image-overlay"></div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                </div>


                <!-- /.card-body -->
            </div>

        </div>
    </div>
    </div>
    {{-- <td> {{ $Service->created_at }} </td> --}}


@endsection


@section('JS')



    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-46156385-1', 'cssscript.com');
        ga('send', 'pageview');
    </script>



    <!-- jQuery -->
    <script src="{{ asset('CMS/plugins/jquery/jquery.min.js') }}"></script>


    <script src="{{ asset('CMS/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('CMS/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dist/js/demo.js') }}"></script>
    <!-- Page specific script -->
    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": true,
                "autoWidth": false,
                "buttons": ["copy", "excel", "pdf", "print"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>


@endsection
