<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;


    public function subimages()
    {
        return $this->belongsTo(Teacher::class, 'teacher_id', 'id');
    }


    public function Student()
    {
        // return $this->hasMany(Service::class);
        return $this->belongsTo(Student::class, 'student_id', 'id');
    }
}
