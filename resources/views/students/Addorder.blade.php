@extends('layout.parent')

@section('Title', 'اضافة طلب')

@section('CSS')

<link href="{{ asset('assets/css/Star.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/profile.css') }}" rel="stylesheet">
<style>
    .custom-file {
        position: relative;
        display: inline-block;
        width: 100%;
        height: calc(2.25rem + 2px);
        margin-bottom: 0;
    }

    .custom-file-input {
        position: relative;
        z-index: 2;
        width: 100%;
        height: calc(2.25rem + 2px);
        margin: 0;
        opacity: 0;
    }

    .custom-file-label {
        position: absolute;
        font-family: 'cairo';
        top: 0;
        left: 0;
        right: 0;
        z-index: 1;
        height: calc(2.25rem + 2px);
        padding: .375rem .75rem;
        font-weight: 400;
        line-height: 1.5;
        color: #495057;
        background-color: rgb(235, 235, 235);
        border: 1px solid #ced4da;
        border-radius: .25rem;
    }



    #header {
        visibility: hidden;
        margin-top: -50px;
    }

</style>
@endsection


@section('dd')
<h2 class="logo" style="font-family: Al-Jazeera-Arabic"><a href="{{ route('showHome') }}"
        style="font-family: Al-Jazeera-Arabic; color: aliceblue"> Learn <span
            style="font-family: Al-Jazeera-Arabic;font-weight: 760;color: #Fd4;"> up
        </span></a></h2>
@endsection
@section('con', 'display: none')


@section('content')




<div class="container bootstrap snippets bootdey">
<div class="row">
    <div class="profile-nav col-md-3">
        <div class="panel">
            <div class="user-heading round">
                <a href="#">
                    <img src="{{ asset('storage/image_profile/' . auth()->user()->image) }}" alt="">
                </a>
                <h1> {{ auth()->user()->name }} </h1>
                <p>{{ auth()->user()->email }} </p>
            </div>

            <ul class="nav">
                <li><a href="{{ route('control') }}"><i class="fas fa-tachometer-alt">
                            &nbsp;</i>
                        دفقة القيادة </a></li>
                <li><a href="{{ route('showprofile') }}"><i class="fa fa-user"> &nbsp;</i> الملف الشخصي </a>
                </li>

                @if (auth()->user()->id == auth('teacher')->id())
                    <li><a href="{{ route('services.index') }}"><i class="fab fa-servicestack">
                                &nbsp;</i>
                            الخدمات
                        </a></li>

                    <li><a href="{{ route('rqserve_all') }}"><i class="fas fa-retweet"> &nbsp;</i>
                            الطلبات الواردة
                        </a></li>
                @else
                    <li class="active"><a href="{{ route('order.index') }}"><i class="fab fa-servicestack">
                                &nbsp;</i>
                            الطلبات
                        </a></li>


                    <li><a href="{{ route('rqserve_allstd') }}"><i class="fas fa-retweet"> &nbsp;</i>
                            العروض المقدمه
                        </a></li>
                @endif
                <li><a href="{{ route('password_edit') }}"><i class="fas fa-lock"> &nbsp;</i> تعديل كلمه
                        المرور</a></li>



                <li><a href="{{ route('logout_log') }}"><i class="fas fa-sign-out-alt"> &nbsp;</i> تسجيل
                        الخروج</a></li>
            </ul>

        </div>
    </div>


    <div class="profile-info col-md-9" style="margin-bottom: 60px">
        <div class="panel">
            <div class="bg-info clearfix">
                <button type="button" class="btn btn-secondary float-right"> الطلبات واعدادتها </button>
            </div>
        </div>


        <div class="card">
            <div class="card-header" style="padding-top: 13px;padding-bottom: 12px;padding-right:18px;width: 100%;">
                <div class="col-4" style="float: left; "> <a href="{{ route('order.index') }}">
                        <button type="button" class="btn btn-outline-primary" style="width: 100%"> <i
                                class="fas fa-indent">&nbsp;</i> عرض الطلبات المضافة </button>
                    </a>
                </div>

                <div class="col-8">
                    <h4> تقديم طلب جديد </h3>
                </div>


            </div>


            <!-- /.card-header -->
            <div class="card-body">

                <form>



                    <div class="form-group row">
                        <label for="name" class="col-4 col-form-label mb-2"> عنوان الطلب </label>
                        <div class="col-8">
                            <input id="name" placeholder="عنوان للخدمة التي تحتاجها" class="form-control"
                                required="required" type="text">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="select" class="col-4 col-form-label mb-2"> تصنيف طلب الخدمة </label>
                        <div class="col-8">
                            <select id="select" name="select" class="form-select">
                                <option> </option>
                                @foreach ($categorise as $categoris)
                                    <option value=" {{ $categoris->id }} "> {{ $categoris->name }} </option>
                                @endforeach

                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="city" class="col-4 col-form-label mb-2">
                            المنطقة التي تتيح فيها طلبك
                        </label>
                        <div class="col-8">
                            <select id="city" name="city" class="form-select">
                                <option> </option>
                                @foreach ($city as $cities)
                                    <option value=" {{ $cities->id }} "> {{ $cities->name }} </option>
                                @endforeach

                            </select>
                        </div>
                    </div>



                    <div class="form-group row">
                        <label for="select_CAT" class="col-4 col-form-label mb-2"> المرحلة العمرية الخاصة بك
                        </label>
                        <div class="col-8">
                            <select id="select_CAT" name="select" class="form-select">
                                <option value="المرحله الاساسية (الابتدائية)"> المرحله الاساسية (الابتدائية)
                                </option>
                                <option value="المرحلة الاعدادية"> المرحلة الاعدادية </option>
                                <option value="الثانوية العامه"> الثانوية العامه </option>
                                <option value="المرحلة الثانوية بدون توجيهي"> المرحلة الثانوية بدون توجيهي</option>
                                <option value="الثانوية العامه (توجيهي)"> الثانوية العامه (توجيهي) </option>
                                <option value="المرحلة الاساسية و الاعدادية"> المرحلة الاساسية و الاعدادية </option>
                                <option value="المرحلة الاساسية و الاعدادية والثانوية"> المرحلة الاساسية و الاعدادية
                                    والثانوية</option>
                                <option value="المرحلة الاعدادية والثانوية"> المرحلة الاعدادية والثانوية</option>
                                <option value="المرحلة الجامعية"> المرحلة الجامعية </option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="price" class="col-4 col-form-label mb-2">السقف المالي الخاص بك للخدمه التي
                            طلبتها
                        </label>
                        <div class="col-8">
                            <input id="price" placeholder="ادخل السعر هنا بالشيكل" class="form-control"
                                required="required" type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="describe" class="col-4 col-form-label mb-2"> </label>
                        <div class="col-8">
                            <input id="student_id" type="text" value="{{ auth()->user()->id }}"
                                style="visibility: hidden" disabled>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="describe" class="col-4 col-form-label mb-2"> تفاصيل الطلب </label>
                        <div class="col-8">
                            <textarea id="describe" cols="40" rows="4" class="form-control"> </textarea>
                        </div>
                    </div>
                    <br>
                    <br>

                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="category_image" value="image.png">
                        <label class="custom-file-label" for="category_image">أختر صورة توضيحية لطلبك
                        </label>
                    </div>

                    <br>
                    <br>
                    <br>

                    <button type="button" onclick="store({{ auth()->user()->id }})" class="btn btn-primary"
                        id="button" style="float: left; width: 100%">انشئ طلب الخدمه </button>
                    <br>
                    <br>
                    <br>
                </form>
            </div>

            <!-- /.card-body -->
        </div>

    </div>
</div>
</div>


@endsection


@section('JS')

<script>
    function store() {

        let formData = new FormData();

        formData.append('name', document.getElementById('name').value);
        formData.append('price', document.getElementById('price').value);
        formData.append('select_CAT', document.getElementById('select_CAT').value);
        formData.append('select', document.getElementById('select').value);
        formData.append('city', document.getElementById('city').value);
        formData.append('student_id', document.getElementById('student_id').value);
        formData.append('describe', document.getElementById('describe').value);
        formData.append('category_image', document.getElementById('category_image').files[0]);

        axios.post('/profile/control/order/', formData)
            .then(function(response) {
                // handle success
                console.log(response);
                toastr.success(response.data.message);
                // document.getElementById('create-form').reset();
                window.location.href = "/profile/control/order";
            })
            .catch(function(error) {
                // handle error
                console.log(error);
                toastr.error(error.response.data.message);
            })
            .then(function() {
                // always executed
            });
    }
</script>




@endsection
