<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRqordersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rqorders', function (Blueprint $table) {
            $table->id();
            $table->string('pay');
            $table->string('time');
            $table->string('date');
            $table->string('account_type');
            $table->string('Status')->default('0');

            $table->foreignId('teacher_id');
            $table->foreign('teacher_id')->on('teachers')->references('id');

            $table->foreignId('order_id');
            $table->foreign('order_id')->on('orderss')->references('id');

            $table->foreignId('student_id')->nullable();
            $table->foreign('student_id')->on('students')->references('id');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rqorders');
    }
}
