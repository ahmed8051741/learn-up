<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use HasFactory, SoftDeletes;



    public function subService()
    {
        // return $this->hasMany(Service::class);
        return $this->hasMany(Service::class, 'city_id', 'id');
    }


    public function subOrder()
    {
        return $this->hasMany(Order::class, 'city_id', 'id');
    }
}
