@extends('admins.parent')
@section('title', 'تعديل بينات المشرف')
@section('bige-title', 'تعديل بينات المشرف')
@section('main-page', 'الرئيسية')
@section('sub-page', 'تعديل المشرف')
@section('profile_img', 'Active')

@section('content')
<!-- Main content -->
<section class="content">
<div class="container-fluid">
<div class="row">
<!-- left column -->
<div class="col-md-12">
<!-- general form elements -->
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title" style="float: right">تعديل صورة المشرف</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form id="create-form">
        @csrf
        <div class="card-body">




            <div class="form-group">
                <!-- <label for="customFile">Custom File</label> -->

                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="image">
                    <label class="custom-file-label" for="image">Choose file</label>
                </div>
            </div>

        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            {{-- <button type="button" onclick="update({{$admin->id}}, '{{$admin->id !=auth('admin')->id()}}')" class="btn btn-primary" id="button">Update</button> --}}
            {{-- <button type="button" onclick="update({{$admin->id}},'{{$redirect ?? true}}')" class="btn btn-primary" id="button">Update</button> --}}
            <button type="button" onclick="update({{ auth()->user()->id }})" class="btn btn-primary"
                id="button"> تحديث
                الصورة</button>

        </div>
    </form>
</div>


</div>

</div>
<!-- /.row -->
</div><!-- /.container-fluid -->
</section>

@endsection

@section('JS')

<script>
    function update(id) {
        let formData = new FormData();
        formData.append('_method', 'PUT');
        formData.append('image', document.getElementById('image').files[0]);
        axios.post('/admin/Update-profile_image/' + id, formData)
            .then(function(response) {
                // handle success
                console.log(response);
                toastr.success(response.data.message);
                // document.getElementById('create-form').reset();
                // window.location.href = "/profile";
                // window.location.reload(10000);
                setTimeout(function() {
                    window.location.href = '/admin/edit_profile_imgae/';
                }, 1500);

            }, 10000)
            .catch(function(error) {
                // handle error
                console.log(error);
                toastr.error(error.response.data.message);
            })
            .then(function() {
                // always executed
            });

    }
</script>

@endsection
