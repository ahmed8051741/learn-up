<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rqserve extends Model
{
    use HasFactory, SoftDeletes;


    public function subRqserve()
    {
        return $this->hasMany(Notice::class, 'Rqserve_id', 'id');
    }


    public function subReserve()
    {
        return $this->belongsTo(Teacher::class, 'teacher_id', 'id');
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class, 'teacher_sd', 'id');
    }


    public function Service()
    {
        return $this->belongsTo(Service::class, 'service_id', 'id')->withTrashed();
    }
    public function Stud()
    {
        return $this->belongsTo(Student::class, 'student_id', 'id');
    }
}
