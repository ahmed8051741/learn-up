@extends('layout.parent')


@section('Title', 'الملف الشخصي واعداته')

@section('CSS')

    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

    <link href="{{ asset('assets/css/Star.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/profile.css') }}" rel="stylesheet">
    <style>
        #header {
            visibility: hidden;
            margin-top: -50px;
        }

        .custom-file {
            position: relative;
            display: inline-block;
            width: 100%;
            height: calc(2.25rem + 2px);
            margin-bottom: 0;
        }

        .custom-file-input {
            position: relative;
            z-index: 2;
            width: 100%;
            height: calc(2.25rem + 2px);
            margin: 0;
            opacity: 0;
        }

        .custom-file-label {
            position: absolute;
            font-family: 'cairo';
            top: 0;
            left: 0;
            right: 0;
            z-index: 1;
            height: calc(2.25rem + 2px);
            padding: .375rem .75rem;
            font-weight: 400;
            line-height: 1.5;
            color: #495057;
            background-color: rgb(235, 235, 235);
            border: 1px solid #ced4da;
            border-radius: .25rem;
        }

    </style>

@endsection


@section('dd')
    <h2 class="logo" style="font-family: Al-Jazeera-Arabic"><a href="{{ route('showHome') }}"
            style="font-family: Al-Jazeera-Arabic; color: aliceblue"> Learn <span
                style="font-family: Al-Jazeera-Arabic;font-weight: 760;color: #Fd4;"> up
            </span></a></h2>
@endsection
@section('con', 'display: none')


@section('content')



    <div class="container bootstrap snippets bootdey">
        <div class="row">
            <div class="profile-nav col-md-3">
                <div class="panel">
                    <div class="user-heading round">
                        <a href="#">
                            <img src="{{ asset('storage/image_profile/' . auth()->user()->image) }}" alt="">
                        </a>
                        <h1> {{ auth()->user()->name }}</h1>
                        <p> {{ auth()->user()->email }}</p>
                    </div>

                    <ul class="nav">
                        <li class="active"><a href="{{ route('showprofile') }}"><i class="fa fa-user"> &nbsp;</i>
                                الملف
                                الشخصي</a></li>



                        @if (auth()->user()->id == auth('teacher')->id())
                            <li><a href="{{ route('show.teacher', auth()->user()->id) }}"><i class="fas fa-eye">
                                        &nbsp;</i>
                                    عرض ملفك الشخصي كما يظهر للاخرين
                                </a></li>

                        @else
                            <li><a href="{{ route('show.std', auth()->user()->id) }}"><i class="fas fa-eye">
                                        &nbsp;</i>
                                    عرض ملفك الشخصي كما يظهر للاخرين
                                </a></li>
                        @endif




                        <li><a href="{{ route('Edite_Profile') }}"><i class="fa fa-edit"> &nbsp;</i> تعديل الملف
                                الشخصي</a></li>

                        <li><a href="{{ route('image.index') }}"><i class="far fa-images"> &nbsp;</i> تعديل صورة الملف
                                الشخصي</a></li>
                        <li><a href="{{ route('password_edit') }}"><i class="fas fa-lock"> &nbsp;</i> تعديل كلمه
                                المرور</a></li>
                        <li><a href="{{ route('control') }}"><i class="fa fa-calendar"> &nbsp;</i> العودة الى
                                لوحة
                                التحكم
                            </a></li>
                        <li><a href="{{ route('logout_log') }}"><i class="fas fa-sign-out-alt"> &nbsp;</i> تسجيل
                                الخروج</a></li>

                    </ul>
                </div>
            </div>
            <div class="profile-info col-md-9">
                <div class="panel">
                    <div class="bg-info clearfix">
                        <button type="button" class="btn btn-secondary float-right"> المعلومات الشخصية </button>
                    </div>
                </div>
                <div class="panel">

                    <div class="panel-body bio-graph-info">
                        <div style="margin: 5px; margin-right: 22px ; margin-top: 20px">
                            <h4> عرض بيناتك الشخصية </h4>
                            <div align="center">
                                <hr width="96%">
                            </div>
                            <div class="row" style="margin: 5px;margin-top: 25px">
                                <div class=" bio-row">
                                    <p><span> الأسم </span>: {{ auth()->user()->name }}</p>
                                </div>
                                <div class="bio-row">
                                    <p><span> قناة اليوتيوب </span>:<a href="{{ auth()->user()->youtube }}"
                                            target="_blank">
                                            انقر هنا لذهاب
                                            الى القناه </a></p>
                                </div>
                                <div class="bio-row">
                                    <p><span> البريد الالكتروني </span>: {{ auth()->user()->email }}</p>
                                </div>
                                <div class="bio-row">
                                    <p><span> حساب الفيس بوك </span>:<a href="{{ auth()->user()->facebook }}"
                                            target="_blank"> انقر هنا
                                            لذهاب الى الحساب </a></p>
                                </div>
                                <div class="bio-row">
                                    <p><span>التخصص </span>: {{ auth()->user()->major }} @if (auth()->user()->major == null)
                                            لم يتم اختيار تخصص بعد
                                        @endif
                                    </p>
                                </div>
                                <div class="bio-row">
                                    <p><span> حساب الانستجرام </span>:<a href="{{ auth()->user()->instagram }}"
                                            target="_blank"> انقر هنا لذهاب الى الحساب
                                        </a></p>
                                </div>
                                <div class="bio-row">
                                    <p><span>رقم الهاتف </span>:
                                        {{ auth()->user()->mobile }} - {{ auth()->user()->s_mobile }} +
                                        @if (auth()->user()->mobile == null)
                                            لم يتم وضع رقم الهاتف بعد <br> <br>
                                            <span style="color: red">
                                                * تحذير : يعد رقم الهاتف
                                                وسيلة
                                                التواصل لدينا
                                            </span>
                                        @endif
                                    </p>
                                </div>
                                <?php
                                $i = 0;
                                ?>
                                <div class="bio-row">
                                    @if (auth()->user()->account_type != 'student')
                                        <p><span> عدد الاصوات </span>:

                                            @foreach ($stars as $star)
                                                @if ($star->teacher_id == auth()->user()->id)
                                                    <?php
                                                    $i = $i + 1;
                                                    ?>
                                                @endif
                                            @endforeach
                                            {{ $i }}
                                        </p>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body bio-graph-info">
                        <div style="margin: 5px; margin-right: 16px">
                            <h4 style="padding-right: 30px">نبذة تعريفية </h4>
                            <div align="center">
                                <hr width="96%">
                            </div>


                            <div class="row" style="margin: 5px;margin-top: 25px">
                                <div align="center">
                                    <p style="text-align: right ; width: 80%"><span>
                                            {{ auth()->user()->describe }}
                                            @if (auth()->user()->describe == null)
                                                لم يتم وضع وصف حتى الأن
                                            @endif
                                        </span> </p>
                                </div>
                            </div>


                        </div>
                    </div>

                    <div>

                        <br>
                        <br>
                        <br>

                    </div>
                    <br>
                </div>
            </div>
        </div>

        <br>
        <br>
        <br>

    </div>


@endsection


@section('JS')
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-46156385-1', 'cssscript.com');
        ga('send', 'pageview');
    </script>
@endsection
