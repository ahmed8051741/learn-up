<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Service extends Authenticatable
{
    use HasFactory, SoftDeletes;
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function Teacher()
    {
        return $this->belongsTo(Teacher::class, 'teacher_id', 'id');
    }

    public function City()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function getStatusKeywordAttribute()
    {
        return $this->status ? "فعال" : "معطل";
    }

    public function subService()
    {
        // return $this->hasMany(Service::class);
        return $this->hasMany(Rqserve::class, 'service_id', 'id');
    }
}
