<?php

namespace App\Http\Controllers;

use App\Models\category;
use App\Models\City;
use App\Models\Notice;
use App\Models\Order;
use App\Models\Rqserve;
use App\Models\Student;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $orders = Order::all();
        return response()->view('students.order', ['orders' => $orders, 'Notices' => $Notices, 'Reserves' => $Reserves]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $categorise = category::all();
        $city = City::all();
        return response()->view('students.Addorder', ['categorise' => $categorise, 'city' => $city, 'Notices' => $Notices, 'Reserves' => $Reserves]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $validator = Validator(
            $request->all(),
            [
                'name' => 'required|string|min:3|max:30',
                'select' => 'required|string',
                'describe' => 'required|string|min:3|max:10000',
                'student_id' => 'required|numeric',
                'category_image' => 'required|image',
                'city' => 'required|string',
                'price' => 'required|numeric',
                'select_CAT' => 'required|string',

                // 'category_image' => 'required|image|max:2048|mimes:png,jpg'
            ],
            [
                'price.required' => 'ضع سقفك المالي للحصول على طلبات افضل من فضلك',
                'price.numeric' => 'يسمح فقط بلارقام في حقل سقفك المالي',

                'select_CAT.required' => 'اختر مرحلتك العمرية ',
                'select_CAT.string' => 'حدث خلل امني الرجاء المحاولة لاحقاً ',

                'name.required' => 'عذراً لايمكنك ترك عنوان الطلب فارغاً',
                'name.string' => 'في حقل عنوان الحدمة يسمح فقط بالنصوص',
                'name.min' => 'عنوان خدمتك قصير جداً',
                'name.max' => 'عنوان خدمتك طويل جداً',
                'select.required' => 'عذراً لايمكنك ترك  تصنيف الطلب فارغاً',
                'select.string' => 'في حقل تصنيف الطلب يسمح فقط بالنصوص',

                'city.required' => 'عذراً لايمكنك ترك حقل منطقة الطلب فارغاً',
                'city.string' => 'في حقل منطقة الطلب يسمح فقط بالنصوص',

                'describe.string' => 'في حقل وصف الطلب يسمح فقط بالنصوص',
                'describe.required' => 'عذراً لايمكنك ترك  وصف الطلب فارغاً',
                'describe.min' => 'وصف خدمتك قصير جداً',
                'describe.max' => 'وصف خدمتك طويل جداً',
                'student_id.required' => 'في حال عبثك في هذا الحقل انت وحدد تتحمل مسؤلية ضياع بيناتك',
                'category_image.image' => 'يمكنك فقط اضافة الصور',
                'category_image.required' => 'اضف صورة توضيحية للخدمتك من فضلك',


            ]
        );

        if (!$validator->fails()) {
            $order = new Order();
            $order->title = $request->get('name');
            $order->select_CAT = $request->get('select_CAT');
            $order->price = $request->get('price');
            $order->describe = $request->get('describe');
            $order->category_id = $request->get('select');
            $order->city_id = $request->get('city');
            $order->student_id = $request->get('student_id');
            $image = $request->file('category_image');

            $imageName = time() . '_Service.' . $image->getClientOriginalExtension();
            // $image->move(public_path('images'), $imageName);
            $image->storeAs('images', $imageName, ['disk' => 'public']);
            $order->image = $imageName;

            // $isSaved = $order->save();
            $isSaved = $order->save();
            return response()->json([
                'message' => $isSaved ? "تم اضافة طلبك بنجاح" : "حدث خطاء غير معروف .. الرجاء التواصل مع الدعم الفني"
            ], $isSaved ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            //VALIDATION FAILED
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        // $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->get();
        // $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->get();
        // return response()->view('Teacher.ShowServices', ['order' => $order, 'Notices' => $Notices, 'Reserves' => $Reserves]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
        $categorise = category::all();
        $city = City::all();
        return response()->view('students.Edite_order', ['order' => $order, 'categorise' => $categorise, 'city' => $city, 'Notices' => $Notices, 'Reserves' => $Reserves]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
        $validator = Validator(
            $request->all(),
            [
                'name' => 'required|string|min:3|max:30',
                'select' => 'required|string',
                'describe' => 'required|string|min:3|max:10000',
                'category_image' => 'nullable',
                'city' => 'required|string',
                'select_CAT' => 'required|string',
                'price' => 'required|numeric'
                // 'category_image' => 'required|image|max:2048|mimes:png,jpg'
            ],
            [
                'price.required' => 'الرجاء ادخل سعر الخدمه التي تقدمها',
                'price.numeric' => 'يسمح فقط بلارقام في حقل سعر الخدمه',
                'name.required' => 'عذراً لايمكنك ترك عنوان الخدمة فارغاً',
                'name.string' => 'في حقل عنوان الحدمة يسمح فقط بالنصوص',
                'name.min' => 'عنوان خدمتك قصير جداً',
                'name.max' => 'عنوان خدمتك طويل جداً',
                'select.required' => 'عذراً لايمكنك ترك  تصنيف الخدمة فارغاً',
                'select.string' => 'في حقل تصنيف الخدمة يسمح فقط بالنصوص',

                'city.required' => 'عذراً لايمكنك ترك حقل منطقة الخدمة فارغاً',
                'city.string' => 'في حقل منطقة الخدمة يسمح فقط بالنصوص',

                'describe.string' => 'في حقل وصف الخدمة يسمح فقط بالنصوص',
                'describe.required' => 'عذراً لايمكنك ترك  وصف الخدمة فارغاً',
                'describe.min' => 'وصف خدمتك قصير جداً',
                'describe.max' => 'وصف خدمتك طويل جداً',
                'category_image.image' => 'نعتذر ولكن عليك تحديث الصورة',
                'category_image.required' => 'اضف صورة توضيحية للخدمتك من فضلك',
            ]
        );


        if (!$validator->fails()) {

            $order->title = $request->get('name');
            $order->price = $request->get('price');
            $order->describe = $request->get('describe');
            $order->select_CAT = $request->get('select_CAT');
            $order->city_id = $request->get('city');
            $order->category_id = $request->get('select');


            $image = $request->file('category_image');

            if ($image != null) {
                $imageName = time() . '_Service.' . $image->getClientOriginalExtension();
                $image->storeAs('images', $imageName, ['disk' => 'public']);
                $order->image = $imageName;
            }



            $isUpdated = $order->save();

            return response()->json([
                'message' => $isUpdated ? "نجح تحديث طلبك" : "فشل تحديث طلبك"
            ], $isUpdated ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $isDelete = $order->delete();
        if ($isDelete) {
            return response()->json(
                ['title' => 'نجاح', 'text' => 'تم حذف الطلب ', 'icon' => 'success'],
                Response::HTTP_OK
            );
        } else {
            return response()->json(
                ['title' => 'فشل', 'text' => 'فشلت عملية حذف الطلب لسبب ما ..', 'icon' => 'error'],
                Response::HTTP_BAD_REQUEST
            );
        }
    }
}
