<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\VarDumper\Cloner\Data;

class AuthTeacherController extends Controller
{
    public function showLogin(Request $request, $guard)
    {
        return response()->view('teacher.login', ['guard' => $guard]);
    }


    public function login(Request $request)
    {
        $validator = Validator($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6|max:18',
            'customSwitches' => 'required|boolean',
            'guard' => 'required|string|in:teacher,student'
        ], [
            'guard.in' => 'عذراً عليك التأكد من عنوان الURL  ',
            'email.required' => 'عذراً ادخل البريد الالكتروني الخاص بك',
            'email.email' => 'صيغة البريد الالكتروني خاطئة',
            'password.required' => 'ادخل كلمه المرور',
            'password.min' => 'كلمه المرور محصورة ما بين 18-6 احرف او رموز',
            'password.max' => 'كلمه المرور محصورة ما بين 18-6 احرف او رموز',
        ]);

        if (!$validator->fails()) {
            $credentials = [
                'email' => $request->get('email'),
                'password' => $request->get('password'),
            ];
            $email = $request->get('email');
            $password = $request->get('password');
            // if (Auth::guard($request->get('guard')) == "teacher") {
            //     $table = 'teachers';
            // } else {
            //     $table = 'students';
            // }
            $logout = Auth::guard($request->get('guard'));

            if (Auth::guard($request->get('guard'))->attempt(['email' => $email, 'password' => $password, 'active' => 1], $request->get('remember'))) {
                $guard = auth('teacher')->check() ? 'teacher' : 'student';

                if ($guard == "teacher") {
                    $table = 'teachers';
                } else {
                    $table = 'students';
                }


                DB::table($table)
                    ->where('email', '=', $email)
                    ->update(['updated_at' => Carbon::now()]);

                return response()->json([
                    'message' => 'تم تسجيل الدخول بنجاح'
                ], Response::HTTP_OK);
            } elseif (Auth::guard($request->get('guard'))->attempt(['email' => $email, 'password' => $password, 'active' => 0])) {
                $logout->logout();
                return response()->json([
                    'message' => '
                    نعتذر - لقد تم ايقاف حسابك من قبل مدير النظام لتفاصيل تواصل مع
                        الدعم الفني
                     '
                ], Response::HTTP_BAD_REQUEST);
            } else {
                return response()->json([
                    'message' => 'فشل تسجيل الدخول , معلوماتك خاطئة '
                ], Response::HTTP_BAD_REQUEST);
            }
        } else {
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }



    public function logout(Request $request)
    {
        $guard = auth('teacher')->check() ? 'teacher' : 'student';
        Auth::guard($guard)->logout();
        $request->session()->invalidate();
        return redirect()->route('login', $guard);
    }
}
