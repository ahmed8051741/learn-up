@extends('admins.parent')

@section('title', 'المعلمين')
@section('bige-title', 'المعلمين')
@section('main-page', 'الرئيسية')
@section('sub-page', 'المعلمين')

@section('user', 'menu-open')


@section('content')
<section class="content">
<div class="container-fluid">
<!-- /.row -->
<div class="row">
<div class="col-12">
<div class="card">
    <div class="card-header">
        <h3 class="card-title" style="float: right"> جدول عرض جيمع المعلمين </h3>

        <div class="card-tools" style="float: left">
            <form action="{{ route('search_admin') }}" method="POST">
                @csrf
                <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="search" class="form-control float-right"
                        placeholder="Search" value="{{ $search }}">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-default">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive p-0">
        <table class="table table-hover text-nowrap table-bordered table-striped"
            style="text-align: center">
            <thead>
                <tr>
                    <th>#</th>
                    <th>الاسم</th>
                    <th>الحالة</th>
                    <th>تاريخ الانشاء </th>
                    <th>اخر تسحيل دخول</th>
                    <th>الاجراءات</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($teachers as $teacher)
                    <tr>
                        <td>{{ $teacher->id }}</td>
                        <td>{{ $teacher->name }}</td>
                        <td>
                            <span class="badge @if ($teacher->Active) bg-success @else bg-danger @endif ">
                                {{ $teacher->Status_Keyword }} </span>
                        </td>

                        <td>{{ $teacher->created_at }} -||-
                            <span
                                style="color: rgb(0, 0, 0)">{{ $teacher->created_at->diffForHumans() }}</span>
                        </td>

                        <td>
                            <span class="badge badge-success" style="font-size: 14px">
                                نشط {{ $teacher->updated_at->diffForHumans() }} </span>
                        </td>

                        <td>
                            <div class="btn-group">

                                <a href="{{ route('ahmed.show', $teacher->id) }}"
                                    class="btn btn-primary ">
                                    <i class="fas fa-eye"></i>
                                </a>

                                <a href="{{ route('teacher.edit', $teacher->id) }}"
                                    class="btn btn-info">
                                    <i class="fas fa-edit "></i>
                                </a>

                                <a href="#" class="btn btn-danger"
                                    onclick="confirmDestroy({{ $teacher->id }}, this)"
                                    style="margin-right: 1.2px">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach


            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
</div>
</div>
</div><!-- /.container-fluid -->
</section>
@endsection

@section('JS')


<script>
    //عملية الحذف
    function confirmDestroy(id, referince) {
        //    console.log("ID:"+id)    // ->?   (id) تستعمل لفحص الكي اذا تم تمريرة او لا
        Swal.fire({
            title: 'هل انت متأكد من عملية الحذف؟',
            text: "لن تتمكن من التراجع عن هذا!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'الغاء',
            confirmButtonText: 'نعم ، احذفها!',
        }).then((result) => {
            if (result.isConfirmed) {
                destroy(id, referince); // referince لاستقبال الذس من الرابط
            }
        })
    }

    function destroy(id, referince) {
        // Make a request for a user with a given ID
        axios.delete('/register/teacher/' + id)
            .then(function(response) {
                // handle success
                console.log(response);
                referince.closest('tr')
                    .remove(); //referince في فوق في الرابط او الاشرف وليس شرك ان يسمى بي  this الذي يتبع الى كلمه  referince لحذف الصف دون الحاجة الى تحديث الصفحة بلاستناد على المتغير
                ShowMessage(response.data); // لاستقبال الجايسون من الكنترولر ببيناته رسالة النجاح والايقونة
            })
            .catch(function(error) {
                // handle error
                console.log(error); // اسقبال رسائل وبينات الخطاء
                ShowMessage(error.response.data);
            })
            .then(function() {
                // دائماً منفذة لو بدي انفذ داله معينة في كل الحالات
            });

    }

    function ShowMessage(data) {
        Swal.fire({
            icon: data.icon, // طباعة الايقونة والاعنوان بناء على البينات المستقبلة من الكنترولر
            title: data.title,
            text: data.text,
            showConfirmButton: false,
            timer: 1500
        })
    }
</script>

@endsection
