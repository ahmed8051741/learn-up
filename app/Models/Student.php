<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Student extends Authenticatable
{
    use HasFactory, SoftDeletes;

    public function getStatusKeywordAttribute()
    {
        return $this->Active ? "فعال" : "معطل";
    }

    public function subStudent()
    {
        // return $this->hasMany(Service::class);
        return $this->hasMany(Order::class, 'student_id', 'id');
    }

    public function subStudents()
    {
        // return $this->hasMany(Service::class);
        return $this->hasMany(Image::class, 'student_id', 'id');
    }

    public function subStud()
    {
        // return $this->hasMany(Service::class);
        return $this->hasMany(Rqserve::class, 'student_id', 'id');
    }


    public function subStuds()
    {
        // return $this->hasMany(Service::class);
        return $this->hasMany(Rqorder::class, 'student_id', 'id');
    }
}
