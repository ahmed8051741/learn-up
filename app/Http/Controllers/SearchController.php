<?php

namespace App\Http\Controllers;

use App\Models\category;
use App\Models\City;
use App\Models\Notice;
use App\Models\Order;
use App\Models\Rqorder;
use App\Models\Rqserve;
use App\Models\Service;
use App\Models\Star;
use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SearchController extends Controller
{

    public function ahmed_serve($id)
    {
        $serves = Service::withTrashed()->where('teacher_id', '=', $id)->get();
        return response()->view('admins.teacher.index_Service', ['servess' => $serves]);
    }

    public function Ahmed_Oeder($id)
    {
        $Order = Order::withTrashed()->where('student_id', '=', $id)->get();
        return response()->view('admins.std.index_order', ['Orders' => $Order]);
    }

    public function search_vs(Request $request)
    {
        $categoryx = NUll;
        $case  = NUll;
        $Cityx = NUll;
        $search = $request->search;

        // $data = Product::where('price', '>=', 100)->where('name', 'like', 'a%')->get();
        // $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')->get();
        // $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')->where('describe', 'like', "%$search%")->get();
        // $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')->whereLike(['title', 'describe'], $search)->get();

        $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')->whereLike('title', "%$search%")
            ->whereLike('describe', "%$search%")->get();

        $categorise = category::all();
        $cities = City::all();
        $Teacher = Teacher::all();
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
        $stars = Star::all();
        return response()->view('visitor.Home', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
    }





    public function search(Request $request)
    {
        $categoryx = NUll;
        $case  = NUll;
        $Cityx = NUll;
        $search = $request->search;

        // $data = Product::where('price', '>=', 100)->where('name', 'like', 'a%')->get();
        // $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')->get();
        // $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')->where('describe', 'like', "%$search%")->get();
        // $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')->whereLike(['title', 'describe'], $search)->get();

        $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')->whereLike('title', "%$search%")
            ->whereLike('describe', "%$search%")->get();

        $categorise = category::all();
        $cities = City::all();
        $Teacher = Teacher::all();
        $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
        $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
        $stars = Star::all();
        return response()->view('Teacher.Home', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
    }


    public function search_2(Request $request)
    {
        $request->validate(
            [

                'search' => 'sometimes',
                'category'  => 'sometimes',
                'case' => 'sometimes',
                'city' => 'sometimes',
            ]
        );


        $search = $request->get('search');
        $categoryx = $request->get('category');
        $case = $request->get('case');
        $Cityx = $request->get('city');
        $o = 0;
        $s = 50;
        if ($case  == $s) {
            $o = $s;
        } elseif ($case  == '100') {
            $o = 100;
        } elseif ($case  == '200') {
            $o = 200;
        } elseif ($case  == '300') {
            $o = 300;
        } elseif ($case  == '400') {
            $o = 400;
        } elseif ($case  == '500') {
            $o = 500;
        } elseif ($case  == "غير ذالك") {
            $o = 10000000;
        }



        if (($search != null) && ($categoryx != null) && ($case != null) && ($Cityx != null)) {

            $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')
                ->where('category_id', $categoryx)
                ->where('city_id', $Cityx)
                ->where('price', '<=', $o)
                ->where('describe', 'like', "%$search%")
                ->where('title', 'like', "%$search%")
                ->get();
            // ->whereLike('describe', "%$search%")->get();
            $categorise = category::all();
            $cities = City::all();
            $Teacher = Teacher::all();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
            $stars = Star::all();
            return response()->view('Teacher.Home', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search == null) && ($categoryx != null) && ($case != null) && ($Cityx != null)) {

            $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')
                ->where('category_id', $categoryx)
                ->where('city_id', $Cityx)
                ->where('price', '<=', $o)
                ->get();
            // ->whereLike('describe', "%$search%")->get();
            $categorise = category::all();
            $cities = City::all();
            $Teacher = Teacher::all();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
            $stars = Star::all();
            return response()->view('Teacher.Home', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search == null) && ($categoryx == null) && ($case != null) && ($Cityx != null)) {

            $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')
                ->where('city_id', $Cityx)
                ->where('price', '<=', $o)
                ->get();
            // ->whereLike('describe', "%$search%")->get();
            $categorise = category::all();
            $cities = City::all();
            $Teacher = Teacher::all();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
            $stars = Star::all();
            return response()->view('Teacher.Home', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search == null) && ($categoryx == null) && ($case == null) && ($Cityx != null)) {

            $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')
                ->where('city_id', $Cityx)
                ->get();
            // ->whereLike('describe', "%$search%")->get();
            $categorise = category::all();
            $cities = City::all();
            $Teacher = Teacher::all();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
            $stars = Star::all();
            return response()->view('Teacher.Home', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search == null) && ($categoryx == null) && ($case != null) && ($Cityx == null)) {

            $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')
                ->get();
            // ->whereLike('describe', "%$search%")->get();
            $categorise = category::all();
            $cities = City::all();
            $Teacher = Teacher::all();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
            $stars = Star::all();
            return response()->view('Teacher.Home', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search == null) && ($categoryx == null) && ($case == null) && ($Cityx == null)) {
            $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')->get();
            $categorise = category::all();
            $cities = City::all();
            $Teacher = Teacher::all();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
            $stars = Star::all();
            return response()->view('Teacher.Home', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search != null) && ($categoryx == null) && ($case == null) && ($Cityx == null)) {

            $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')->whereLike('title', "%$search%")
                ->whereLike('describe', "%$search%")->get();
            $categorise = category::all();
            $cities = City::all();
            $Teacher = Teacher::all();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
            $stars = Star::all();
            return response()->view('Teacher.Home', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search != null) && ($categoryx != null) && ($case == null) && ($Cityx == null)) {
            $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')
                ->where('category_id', $categoryx)
                ->where('describe', 'like', "%$search%")
                ->where('title', 'like', "%$search%")
                ->get();
            $categorise = category::all();
            $cities = City::all();
            $Teacher = Teacher::all();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
            $stars = Star::all();
            return response()->view('Teacher.Home', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search != null) && ($categoryx != null) && ($case != null) && ($Cityx == null)) {
            $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')
                ->where('category_id', $categoryx)
                ->where('price', '<=', $o)
                ->where('describe', 'like', "%$search%")
                ->where('title', 'like', "%$search%")
                ->get();
            $categorise = category::all();
            $cities = City::all();
            $Teacher = Teacher::all();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
            $stars = Star::all();
            return response()->view('Teacher.Home', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search != null) && ($categoryx != null) && ($case == null) && ($Cityx != null)) {
            $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')
                ->where('category_id', $categoryx)
                ->where('city_id', $Cityx)
                ->where('describe', 'like', "%$search%")
                ->where('title', 'like', "%$search%")
                ->get();
            $categorise = category::all();
            $cities = City::all();
            $Teacher = Teacher::all();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
            $stars = Star::all();
            return response()->view('Teacher.Home', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search != null) && ($categoryx == null) && ($case == null) && ($Cityx != null)) {
            $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')
                ->where('city_id', $Cityx)
                ->where('describe', 'like', "%$search%")
                ->where('title', 'like', "%$search%")
                ->get();
            $categorise = category::all();
            $cities = City::all();
            $Teacher = Teacher::all();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
            $stars = Star::all();
            return response()->view('Teacher.Home', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search != null) && ($categoryx == null) && ($case != null) && ($Cityx == null)) {
            $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')
                ->where('price', '<=', $o)
                ->where('describe', 'like', "%$search%")
                ->where('title', 'like', "%$search%")
                ->get();
            $categorise = category::all();
            $cities = City::all();
            $Teacher = Teacher::all();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
            $stars = Star::all();
            return response()->view('Teacher.Home', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search != null) && ($categoryx == null) && ($case != null) && ($Cityx != null)) {
            $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')
                ->where('price', '<=', $o)
                ->where('city_id', $Cityx)
                ->where('describe', 'like', "%$search%")
                ->where('title', 'like', "%$search%")
                ->get();
            $categorise = category::all();
            $cities = City::all();
            $Teacher = Teacher::all();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
            $stars = Star::all();
            return response()->view('Teacher.Home', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search == null) && ($categoryx != null) && ($case == null) && ($Cityx == null)) {
            $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')
                ->where('category_id', $categoryx)
                ->get();
            $categorise = category::all();
            $cities = City::all();
            $Teacher = Teacher::all();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
            $stars = Star::all();
            return response()->view('Teacher.Home', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search == null) && ($categoryx == null) && ($case != null) && ($Cityx == null)) {
            $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')
                ->where('price', '<=', $o)->get();
            $categorise = category::all();
            $cities = City::all();
            $Teacher = Teacher::all();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
            $stars = Star::all();
            return response()->view('Teacher.Home', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search == null) && ($categoryx != null) && ($case != null) && ($Cityx == null)) {
            $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')
                ->where('price', '<=', $o)
                ->where('category_id', $categoryx)
                ->get();
            $categorise = category::all();
            $cities = City::all();
            $Teacher = Teacher::all();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
            $stars = Star::all();
            return response()->view('Teacher.Home', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search == null) && ($categoryx != null) && ($case == null) && ($Cityx != null)) {
            $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')
                ->where('category_id', $categoryx)
                ->where('city_id', $Cityx)
                ->get();
            $categorise = category::all();
            $cities = City::all();
            $Teacher = Teacher::all();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Stud')->with('Service')->orderBy('id', 'DESC')->take(5)->get();
            $stars = Star::all();
            return response()->view('Teacher.Home', ['services' => $services, 'Teachers' => $Teacher, 'stars' => $stars, 'categorise' => $categorise, 'cities' => $cities, 'Teacher' => $Teacher, 'Notices' => $Notices, 'Reserves' => $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        }
    }


    public function search_std2(Request $request)
    {
        $request->validate(
            [

                'search' => 'sometimes',
                'category'  => 'sometimes',
                'case' => 'sometimes',
                'city' => 'sometimes',
            ]
        );


        $search = $request->get('search');
        $categoryx = $request->get('category');
        $case = $request->get('case');
        $Cityx = $request->get('city');
        $o = 0;
        $s = 50;
        if ($case  == $s) {
            $o = $s;
        } elseif ($case  == '100') {
            $o = 100;
        } elseif ($case  == '200') {
            $o = 200;
        } elseif ($case  == '300') {
            $o = 300;
        } elseif ($case  == '400') {
            $o = 400;
        } elseif ($case  == '500') {
            $o = 500;
        } elseif ($case  == "غير ذالك") {
            $o = 10000000;
        }



        if (($search != null) && ($categoryx != null) && ($case != null) && ($Cityx != null)) {
            $orders = Order::with('student')->with('link')->orderBy('id', 'DESC')
                ->where('category_id', $categoryx)
                ->where('city_id', $Cityx)
                ->where('price', '<=', $o)
                ->where('describe', 'like', "%$search%")
                ->where('title', 'like', "%$search%")
                ->get();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
            $categorise = category::all();
            $cities = City::all();
            $Student = Student::all();
            return response()->view('students.Home', ['orders' => $orders, 'categorise' => $categorise, 'cities' => $cities, 'Student' => $Student, 'Notices' => $Notices, 'Reserves' => $Reserves, $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search == null) && ($categoryx != null) && ($case != null) && ($Cityx != null)) {

            $orders = Order::with('student')->with('link')->orderBy('id', 'DESC')
                ->where('category_id', $categoryx)
                ->where('city_id', $Cityx)
                ->where('price', '<=', $o)
                ->get();
            // ->whereLike('describe', "%$search%")->get();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
            $categorise = category::all();
            $cities = City::all();
            $Student = Student::all();
            return response()->view('students.Home', ['orders' => $orders, 'categorise' => $categorise, 'cities' => $cities, 'Student' => $Student, 'Notices' => $Notices, 'Reserves' => $Reserves, $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search == null) && ($categoryx == null) && ($case != null) && ($Cityx != null)) {

            $orders = Order::with('student')->with('link')->orderBy('id', 'DESC')
                ->where('city_id', $Cityx)
                ->where('price', '<=', $o)
                ->get();
            // ->whereLike('describe', "%$search%")->get();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
            $categorise = category::all();
            $cities = City::all();
            $Student = Student::all();
            return response()->view('students.Home', ['orders' => $orders, 'categorise' => $categorise, 'cities' => $cities, 'Student' => $Student, 'Notices' => $Notices, 'Reserves' => $Reserves, $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search == null) && ($categoryx == null) && ($case == null) && ($Cityx != null)) {

            $orders = Order::with('student')->with('link')->orderBy('id', 'DESC')
                ->where('city_id', $Cityx)
                ->get();
            // ->whereLike('describe', "%$search%")->get();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
            $categorise = category::all();
            $cities = City::all();
            $Student = Student::all();
            return response()->view('students.Home', ['orders' => $orders, 'categorise' => $categorise, 'cities' => $cities, 'Student' => $Student, 'Notices' => $Notices, 'Reserves' => $Reserves, $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search == null) && ($categoryx == null) && ($case != null) && ($Cityx == null)) {

            $orders = Order::with('student')->with('link')->orderBy('id', 'DESC')
                ->get();
            // ->whereLike('describe', "%$search%")->get();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
            $categorise = category::all();
            $cities = City::all();
            $Student = Student::all();
            return response()->view('students.Home', ['orders' => $orders, 'categorise' => $categorise, 'cities' => $cities, 'Student' => $Student, 'Notices' => $Notices, 'Reserves' => $Reserves, $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search == null) && ($categoryx == null) && ($case == null) && ($Cityx == null)) {
            $orders = Order::with('student')->with('link')->orderBy('id', 'DESC')->get();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
            $categorise = category::all();
            $cities = City::all();
            $Student = Student::all();
            return response()->view('students.Home', ['orders' => $orders, 'categorise' => $categorise, 'cities' => $cities, 'Student' => $Student, 'Notices' => $Notices, 'Reserves' => $Reserves, $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search != null) && ($categoryx == null) && ($case == null) && ($Cityx == null)) {

            $orders = Order::with('student')->with('link')->orderBy('id', 'DESC')->whereLike('title', "%$search%")
                ->whereLike('describe', "%$search%")->get();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
            $categorise = category::all();
            $cities = City::all();
            $Student = Student::all();
            return response()->view('students.Home', ['orders' => $orders, 'categorise' => $categorise, 'cities' => $cities, 'Student' => $Student, 'Notices' => $Notices, 'Reserves' => $Reserves, $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search != null) && ($categoryx != null) && ($case == null) && ($Cityx == null)) {
            $orders = Order::with('student')->with('link')->orderBy('id', 'DESC')
                ->where('category_id', $categoryx)
                ->where('describe', 'like', "%$search%")
                ->where('title', 'like', "%$search%")
                ->get();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
            $categorise = category::all();
            $cities = City::all();
            $Student = Student::all();
            return response()->view('students.Home', ['orders' => $orders, 'categorise' => $categorise, 'cities' => $cities, 'Student' => $Student, 'Notices' => $Notices, 'Reserves' => $Reserves, $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search != null) && ($categoryx != null) && ($case != null) && ($Cityx == null)) {
            $orders = Order::with('student')->with('link')->orderBy('id', 'DESC')
                ->where('category_id', $categoryx)
                ->where('price', '<=', $o)
                ->where('describe', 'like', "%$search%")
                ->where('title', 'like', "%$search%")
                ->get();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
            $categorise = category::all();
            $cities = City::all();
            $Student = Student::all();
            return response()->view('students.Home', ['orders' => $orders, 'categorise' => $categorise, 'cities' => $cities, 'Student' => $Student, 'Notices' => $Notices, 'Reserves' => $Reserves, $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search != null) && ($categoryx != null) && ($case == null) && ($Cityx != null)) {
            $orders = Order::with('student')->with('link')->orderBy('id', 'DESC')
                ->where('category_id', $categoryx)
                ->where('city_id', $Cityx)
                ->where('describe', 'like', "%$search%")
                ->where('title', 'like', "%$search%")
                ->get();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
            $categorise = category::all();
            $cities = City::all();
            $Student = Student::all();
            return response()->view('students.Home', ['orders' => $orders, 'categorise' => $categorise, 'cities' => $cities, 'Student' => $Student, 'Notices' => $Notices, 'Reserves' => $Reserves, $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search != null) && ($categoryx == null) && ($case == null) && ($Cityx != null)) {
            $orders = Order::with('student')->with('link')->orderBy('id', 'DESC')
                ->where('city_id', $Cityx)
                ->where('describe', 'like', "%$search%")
                ->where('title', 'like', "%$search%")
                ->get();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
            $categorise = category::all();
            $cities = City::all();
            $Student = Student::all();
            return response()->view('students.Home', ['orders' => $orders, 'categorise' => $categorise, 'cities' => $cities, 'Student' => $Student, 'Notices' => $Notices, 'Reserves' => $Reserves, $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search != null) && ($categoryx == null) && ($case != null) && ($Cityx == null)) {
            $orders = Order::with('student')->with('link')->orderBy('id', 'DESC')
                ->where('price', '<=', $o)
                ->where('describe', 'like', "%$search%")
                ->where('title', 'like', "%$search%")
                ->get();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
            $categorise = category::all();
            $cities = City::all();
            $Student = Student::all();
            return response()->view('students.Home', ['orders' => $orders, 'categorise' => $categorise, 'cities' => $cities, 'Student' => $Student, 'Notices' => $Notices, 'Reserves' => $Reserves, $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search != null) && ($categoryx == null) && ($case != null) && ($Cityx != null)) {
            $orders = Order::with('student')->with('link')->orderBy('id', 'DESC')
                ->where('price', '<=', $o)
                ->where('city_id', $Cityx)
                ->where('describe', 'like', "%$search%")
                ->where('title', 'like', "%$search%")
                ->get();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
            $categorise = category::all();
            $cities = City::all();
            $Student = Student::all();
            return response()->view('students.Home', ['orders' => $orders, 'categorise' => $categorise, 'cities' => $cities, 'Student' => $Student, 'Notices' => $Notices, 'Reserves' => $Reserves, $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search == null) && ($categoryx != null) && ($case == null) && ($Cityx == null)) {
            $orders = Order::with('student')->with('link')->orderBy('id', 'DESC')
                ->where('category_id', $categoryx)
                ->get();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
            $categorise = category::all();
            $cities = City::all();
            $Student = Student::all();
            return response()->view('students.Home', ['orders' => $orders, 'categorise' => $categorise, 'cities' => $cities, 'Student' => $Student, 'Notices' => $Notices, 'Reserves' => $Reserves, $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search == null) && ($categoryx == null) && ($case != null) && ($Cityx == null)) {
            $orders = Order::with('student')->with('link')->orderBy('id', 'DESC')
                ->where('price', '<=', $o)->get();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
            $categorise = category::all();
            $cities = City::all();
            $Student = Student::all();
            return response()->view('students.Home', ['orders' => $orders, 'categorise' => $categorise, 'cities' => $cities, 'Student' => $Student, 'Notices' => $Notices, 'Reserves' => $Reserves, $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search == null) && ($categoryx != null) && ($case != null) && ($Cityx == null)) {
            $orders = Order::with('student')->with('link')->orderBy('id', 'DESC')
                ->where('price', '<=', $o)
                ->where('category_id', $categoryx)
                ->get();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
            $categorise = category::all();
            $cities = City::all();
            $Student = Student::all();
            return response()->view('students.Home', ['orders' => $orders, 'categorise' => $categorise, 'cities' => $cities, 'Student' => $Student, 'Notices' => $Notices, 'Reserves' => $Reserves, $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        } elseif (($search == null) && ($categoryx != null) && ($case == null) && ($Cityx != null)) {
            $orders = Order::with('student')->with('link')->orderBy('id', 'DESC')
                ->where('category_id', $categoryx)
                ->where('city_id', $Cityx)
                ->get();
            $Notices = Notice::with('Rqserve')->orderBy('id', 'DESC')->take(5)->get();
            $Reserves = Rqserve::with('subReserve')->with('Service')->with('Stud')->orderBy('id', 'DESC')->take(5)->get();
            $categorise = category::all();
            $cities = City::all();
            $Student = Student::all();
            return response()->view('students.Home', ['orders' => $orders, 'categorise' => $categorise, 'cities' => $cities, 'Student' => $Student, 'Notices' => $Notices, 'Reserves' => $Reserves, $Reserves, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case, 'search' => $search, 'categoryx' => $categoryx, 'Cityx' => $Cityx, 'case' => $case]);
        }
    }






    public function search_admin(Request $request)
    {
        $request->validate(
            [
                'search' => 'sometimes',
            ]
        );
        $search = $request->search;
        $teacher = Teacher::where('name', 'like', "%$search%")->get();
        return response()->view('admins.teacher.index', ['teachers' => $teacher, 'search' => $search]);
    }
}
// $services = Service::with('teacher')->with('category')->orderBy('id', 'DESC')
//     ->where('category_id', $categoryx)
//     ->where('city_id', $Cityx)
//     ->where('price', '<=', $o)
//     ->where('describe', 'like', "%$search%")
//     ->where('title', 'like', "%$search%")
//     ->get();
